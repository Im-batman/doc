<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

	    Schema::create('confirmations', function(Blueprint $table){
		    $table->increments('confid');
		    $table->string('phone')->nullable();
		    $table->string('email')->nullable();
		    $table->string('role')->nullable();
		    $table->string('code');
		    $table->timestamps();
	    });

	    Schema::create( 'password_resets', function ( Blueprint $table ) {
		    $table->increments('prid');
		    $table->string( 'email' ,191)->index();
		    $table->string( 'token' );
		    $table->timestamp( 'created_at' )->nullable();
	    } );

	    Schema::create('doctors', function(Blueprint $table){

	    	$table->increments( 'docid' );
		    $table->string( 'fname' );
		    $table->string( 'sname' );
		    $table->string( 'licenseid' );
		    $table->string( 'countryOfPractice' );
		    $table->string( 'nationality' );
		    $table->string( 'yearsOfExperience' );
		    $table->string( 'hospitalAffiliation' );
		    $table->string('bio',5000)->nullable();
		    $table->enum('paymentMethod',['paypal','bank','none']);
	        $table->string('paypal')->nullable();
	        $table->string('accountName')->nullable();
	        $table->string('accountNumber')->nullable();
	        $table->string('accountBank')->nullable();
	        $table->string('bankCode')->nullable();
		    $table->string( 'password', 60 );
		    $table->string( 'email',191 )->unique();
		    $table->string( 'image', 2000 );
		    $table->integer( 'earnings' )->default(0);
		    $table->integer('rating')->default(0);
		    $table->enum( 'gender', [ 'Male', 'Female' ] );
		    $table->string( 'phone',191 )->unique();
		    $table->boolean('isConfirmed')->default(0);
		    $table->boolean('isSuspended')->default(0);
		    $table->boolean('isPaymentRequested')->default(0);
		    $table->timestamps();

	    });

	    Schema::create('doctorlangs', function(Blueprint $table){
	        $table->increments('doclid');
	        $table->integer('docid');
	        $table->string('language');
	        $table->timestamps();
	    });

	    Schema::create('doctor_docs', function(Blueprint $table){
	    	$table->increments('dcid');
	    	$table->integer('docid');
	    	$table->string('title');
	    	$table->string('description',2000)->nullable();
	    	$table->string('url',1000);
	    	$table->timestamps();
	    });

	    Schema::create('specializations', function(Blueprint $table){
	    	$table->increments('spid');
	    	$table->string('name');
	    	$table->string('description');
	    	$table->timestamps();
	    });

	    Schema::create('doctor_specializations', function(Blueprint $table){
	    	$table->increments('dsid');
	    	$table->integer('docid');
	    	$table->integer('spid');
	    	$table->timestamps();
	    });

	    Schema::create( 'patients', function ( Blueprint $table ) {
		    $table->increments( 'patid' );
		    $table->string( 'fname' );
		    $table->string( 'sname' );
		    $table->string( 'email',191 )->unique();
		    $table->enum( 'gender', [ 'Male', 'Female' ] );
		    $table->string( 'dob' );
		    $table->decimal('height');
		    $table->decimal('weight');
		    $table->string( 'phone',191 )->unique();
	        $table->string( 'password', 60 );
		    $table->string( 'image', 2000 );
		    $table->string('authCode')->nullable();
		    $table->boolean('isConfirmed')->default(0);
		    $table->timestamps();
	    } );

	    Schema::create('medicalhistory', function(Blueprint $table){
		    $table->increments('mhid');
		    $table->integer('patid');
		    $table->integer('pdid')->nullable();
		    $table->enum('type',['patient','dependant'])->default('patient');
		    $table->string('history',2000);
		    $table->timestamps();
	    });


	    Schema::create('patient_dependants', function(Blueprint $table){
	        $table->increments('pdid');
	        $table->integer('patid');
		    $table->string( 'fname' );
		    $table->string( 'sname' );
		    $table->string( 'email',191 )->unique()->nullable();
		    $table->enum( 'gender', [ 'Male', 'Female' ] );
		    $table->string( 'dob' );
		    $table->decimal('height');
		    $table->decimal('weight');
		    $table->string( 'phone',191 )->unique()->nullable();
		    $table->string( 'image', 2000 );
		    $table->timestamps();
	    });

	    Schema::create('tests', function(Blueprint $table){
		    $table->increments('testid');
		    $table->string('title');
		    $table->string('description',2000)->nullable();
		    $table->string('image',2000)->nullable();
		    $table->timestamps();
	    });

	    Schema::create('testresults', function(Blueprint $table){
	        $table->increments('trid');
	        $table->integer('labid');
	        $table->integer('ctid');
	        $table->string('url',2000);
	        $table->string('description');
	        $table->timestamps();
	    });

	    Schema::create('prescriptions', function(Blueprint $table){
		    $table->increments('prid');
		    $table->integer('casid');
		    $table->integer('pharmid')->nullable();
		    $table->integer('docid');
		    $table->string('prescription',2000);
		    $table->string('code',255);
		    $table->timestamps();
	    });

	    Schema::create('cases', function(Blueprint $table){
	        $table->increments('casid');
	        $table->integer('docid')->nullable();
	        $table->integer('patid');
	        $table->integer('pdid')->nullable();
	        $table->integer('spid')->nullable();
	        $table->integer('pharmid')->nullable();
	        $table->string('prescriptionCode')->nulllable();
	        $table->string('prescription')->nullable();
	        $table->string('description',5000);
	        $table->string('diagnosis',5000)->nullable();
	        $table->enum('type',['patient','dependent'])->default('patient');
	        $table->enum('status',['Available','Pending','Completed','Cancelled']);
	        $table->enum('paymentStatus',['paid','unpaid'])->default('unpaid');
	        $table->enum('paymentMethod',['paypal','bank'])->default('bank');
	        $table->timestamps();
	    });

	    Schema::create('caseupdates', function(Blueprint $table){
		    $table->increments('cuid');
		    $table->integer('casid');
		    $table->integer('docid');
		    $table->boolean('patientCanView')->default(0);
		    $table->string('update');
		    $table->timestamps();
	    });


	    Schema::create('casedoctors', function(Blueprint $table){
	    	$table->increments('cdid');
	    	$table->integer('casid');
	    	$table->integer('docid');
	    	$table->timestamp('dateEnded')->nullable();
	    	$table->string('notes')->nullable();
	    	$table->timestamps();
	    });

	    Schema::create('casetests', function(Blueprint $table){
	        $table->increments('ctid');
	        $table->integer('casid');
		    $table->integer('docid');
	        $table->integer('labid')->nullable();
	        $table->integer('testid');

	        $table->timestamps();
	    });

	    Schema::create('casefiles', function(Blueprint $table){
	        $table->increments('cfid');
	        $table->integer('casid');
		    $table->string('name');
	        $table->string('url',2000);
	        $table->timestamps();
	    });

	    Schema::create('pharmacies', function(Blueprint $table){
	        $table->increments('pharmid');
	        $table->integer('uid');
		    $table->integer('createdBy');
	        $table->string('name');
	        $table->string('address',2000);
	        $table->string('city');
	        // add geolocation
	        $table->timestamps();
	    });

	    Schema::create('labs', function(Blueprint $table){
	        $table->increments('labid');
	        $table->integer('uid');
		    $table->integer('createdBy');
		    $table->string('name');
		    $table->string('address',2000);
		    $table->string('city');
		    $table->string('phone');
		    $table->string('email');
		    // add geolocation
		    $table->timestamps();
	    });
	    Schema::create('lab_tests', function(Blueprint $table){
	        $table->increments('ltid');
	        $table->integer('labid');
	        $table->integer('testid');
	        $table->timestamps();
	    });

	    Schema::create('hospitals', function(Blueprint $table){
		    $table->increments('hid');
		    $table->integer('uid');
		    $table->integer('createdBy');
		    $table->string('name');
		    $table->string('address',2000);
		    $table->string('country');
		    $table->string('city');
		    $table->string('phone');
		    $table->string('email');
		    $table->boolean('isPublished')->default('0');
		    // add geolocation
		    $table->timestamps();
	    });

	    Schema::create('hospital_specializations', function(Blueprint $table){
		    $table->increments('hsid');
		    $table->integer('hid');
		    $table->integer('spid');
		    $table->timestamps();
	    });


	    Schema::create('payments', function(Blueprint $table){
		    $table->increments('payid');
		    $table->string('amount');
		    $table->string('method');
		    $table->integer('cid');
		    $table->string('others');
		    $table->timestamps();
		    $table->softDeletes();
	    });

	    Schema::create('settings', function(Blueprint $table){
		    $table->increments('setid');
		    $table->string('name');
		    $table->string('value');
		    $table->timestamps();

	    });


	    Schema::create('healthprofile', function(Blueprint $table){
		    $table->increments('hpid');
		    $table->integer('patid');
		    $table->integer('pdid')->nullable();
		    $table->enum('type',['patient','dependent'])->default('patient');
		    $table->string('description',5000);
		    $table->timestamps();
	    });

	    Schema::create('terms', function(Blueprint $table){
		    $table->increments('tid');
		    $table->string('content',10000);
		    $table->timestamps();
		    $table->softDeletes();
	    });


	    Schema::create('privacy', function(Blueprint $table){
		    $table->increments('pid');
		    $table->string('content',10000);
		    $table->timestamps();
		    $table->softDeletes();
	    });

	    Schema::create('faqs', function(Blueprint $table){
		    $table->increments('fid');
		    $table->string('question');
		    $table->string('answer',5000);
		    $table->timestamps();
		    $table->softDeletes();
	    });


	    Schema::create( 'users', function ( Blueprint $table ) {
		    $table->increments( 'uid' );
		    $table->enum( 'role', [ 'Admin', 'Staff', 'Pharmacy', 'Lab', 'Hospital' ] );
		    $table->string( 'fname' );
		    $table->string( 'sname' );
		    $table->string( 'email',191 )->unique();
		    $table->string( 'password', 60 );
		    $table->string( 'phone',191 )->unique();
		    $table->rememberToken();
		    $table->string( 'image', 2000 );
		    $table->string( 'address', 2000 );
		    $table->timestamps();
	    } );

    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('users');
	    Schema::dropIfExists('confirmations');
	    Schema::dropIfExists('password_resets');
	    Schema::dropIfExists('doctors');
	    Schema::dropIfExists('doctor_docs');
	    Schema::dropIfExists('specializations');
	    Schema::dropIfExists('doctor_specializations');
	    Schema::dropIfExists('patients');
	    Schema::dropIfExists('tests');
	    Schema::dropIfExists('testresults');
	    Schema::dropIfExists('cases');
	    Schema::dropIfExists('casesfiles');
	    Schema::dropIfExists('casetests');
	    Schema::dropIfExists('files');
	    Schema::dropIfExists('pharmacies');
	    Schema::dropIfExists('labs');
	    Schema::dropIfExists('lab_tests');
	    Schema::dropIfExists('patients');
	    Schema::dropIfExists('settings');

    }
}
