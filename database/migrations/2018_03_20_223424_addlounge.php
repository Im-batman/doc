<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addlounge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lounge', function(Blueprint $table){
            $table->increments('lid');
            $table->integer('uid');
            $table->string('title');
            $table->string('content',5000);
            $table->string('link')->nullable();
            $table->string('image',1000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lounge');
    }
}
