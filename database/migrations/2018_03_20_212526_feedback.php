<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Feedback extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback', function(Blueprint $table){
        	$table->increments('fdid');
        	$table->integer('casid');
        	$table->integer('patid');
        	$table->integer('uid')->nullable();
        	$table->enum('feeling',['1','2','3','4','5']);
        	$table->string('content',5000);
        	$table->boolean('isViewed')->default(0);
        	$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
