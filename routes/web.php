<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('manage-customers','HomeController@manageCustomers');


Route::get('profile','HomeController@profile');
Route::get('logout','HomeController@logout');

//redirects

//tests
Route::get('add-test','HomeController@redirect');
Route::get('edit-test','HomeController@redirect');
//specializations
Route::get('add-specialization','HomeController@redirect');
Route::get('assign-specialization','HomeController@redirect');
//hospitals
Route::get('add-hospital-specialization','HomeController@redirect');
//labs
Route::get('add-lab-test','HomeController@redirect');
//patients
Route::get('update-patient/{patid}','HomeController@redirect');
//dependants
Route::get('update-dependent/{pdid}','HomeController@redirect');
//terms-faq-privacy
Route::get('terms','HomeController@redirect');
 
Route::get('faqs','HomeController@redirect');

Route::get('edit-faq/{fid}','HomeController@redirect');
Route::get('privacy','HomeController@redirect');



//Admin Routes

Route::get('admin','AdminController@index');
Route::get('manage-specializations','AdminController@manageSpecializations');
Route::get('feedback','AdminController@feedback');
Route::get('feedback/{fdid}','AdminController@feedbackDetails');
Route::get('feedback-viewed/{fdid}','AdminController@feedbackViewed');


Route::get('manage-lounge','AdminController@manageLounge');

Route::get('add-lounge-item','AdminController@addLoungeItem');
Route::get('lounge/{lid}','AdminController@loungeDetails');
Route::post('add-lounge-item','AdminController@postAddLoungeItem');

Route::post('add-specialization','AdminController@postAddSpecialization');
Route::post('assign-specialization','AdminController@postAssignSpecialization');


Route::get('remove-doctor-specialization/{dsid}','AdminController@removeDoctorSpecialization');

Route::get('suspend-patient/{patid}','AdminController@suspendPatient');
Route::get('unsuspend-patient/{patid}','AdminController@unsuspendPatient');

//tests
Route::get('manage-tests','HomeController@manageTests');
Route::get('edit-test/{testid}','HomeController@editTest');
Route::post('add-test','HomeController@postAddTest');
Route::post('edit-test','HomeController@postEditTest');

//settings
Route::get('manage-settings','HomeController@manageSettings');
Route::post('edit-setting','HomeController@editSetting');

//hospitals
Route::get('manage-hospitals','HomeController@manageHospitals');
Route::get('hospital/{hid}','HomeController@hospitalDetails');
Route::get('add-hospital','HomeController@addHospital');
Route::get('publish-hospital/{hid}','HomeController@publishHospital');
Route::get('unpublish-hospital/{hid}','HomeController@unpublishHospital');
Route::get('remove-hospital-specialization/{hid}/{spid}','HomeController@removeHospitalSpecialization');

Route::post('add-hospital','HomeController@postAddHospital');
Route::post('add-hospital-specialization','HomeController@postAddHospitalSpecialization');

//doctors
Route::get('manage-doctors','HomeController@manageDoctors');
Route::get('doctor/{docid}','HomeController@doctor');
Route::get('confirm-doctor/{docid}','HomeController@confirmDoctor');
Route::get('suspend-doctor/{docid}','HomeController@suspendDoctor');
Route::get('unsuspend-doctor/{docid}','HomeController@unsuspendDoctor');


//cases

Route::get('manage-cases','AdminController@manageCases');
Route::get('case/{casid}','HomeController@caseDetails');


//labs
Route::get('manage-labs','HomeController@manageLabs');
Route::get('add-lab','HomeController@addLab');
Route::get('lab/{labid}','HomeController@labDetails');
Route::get('remove-test/{labid}/{testid}','HomeController@removeTest');

Route::post('add-lab','HomeController@postAddLab');
Route::post('add-lab-test','HomeController@postAddLabTest');


//pharmacies
Route::get('manage-pharmacies','HomeController@managePharmacies');
Route::get('add-pharmacy','HomeController@addPharmacy');
Route::get('pharmacy/{pharmid}','HomeController@pharmacyDetails');

Route::post('add-pharmacy','HomeController@postAddPharmacy');

//patients
Route::get('manage-patients','HomeController@getPatients');
Route::get('patient/{patid}','HomeController@patientDetails');

Route::get('edit-patient/{patid}','HomeController@editPatient');
Route::post('update-patient/{patid}','HomeController@updatePatient');


//labview
Route::get('dashboard/lab','LabController@dashboard');
Route::get('dashboard/lab/cases','LabController@cases');
Route::get('dashboard/lab/patients','LabController@patients');


//dependants
Route::get('dependant/{pdid}','HomeController@dependantDetails');
Route::get('edit-dependant/{pdid}','HomeController@editdependant');
Route::post('update-dependant/{pdid}','HomeController@updatedependant');


//terms
Route::get('manage-terms','AdminController@terms');

Route::post('terms','AdminController@postTerms');

//faqs
Route::get('add-faq','AdminController@addFAQ');
Route::get('manage-faqs','AdminController@faqs');
Route::get('edit-faq/{fid}','AdminController@editFaq');
Route::get('delete-faq/{fid}','AdminController@deleteFaq');

Route::post('faqs','AdminController@postFaq');
Route::post('edit-faq/{fid}','AdminController@postEditFaq');

//privacy policy
Route::get('manage-privacy','AdminController@privacyPolicy');
Route::post('privacy','AdminController@postPrivacyPolicy');


//payments

Route::get('manage-payments','AdminController@managePayments');
Route::get('export-payments','AdminController@exportPayments');

//income

Route::get('manage-income','AdminController@manageIncome');


Route::get('pay-doctors','AdminController@payDoctors');



Route::get('datatable','HomeController@getData');