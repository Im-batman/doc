<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//general routes
Route::post('specializations','ApiController@specializations');

Route::post('live-update','ApiController@liveUpdate');

Route::post('lounge-articles','ApiController@loungeArticles');

Route::post('consultation-fee','ApiController@consultationFee');
Route::post('subscription-fee','ApiController@subscriptionFee');
Route::post('case-details','ApiController@caseDetails');
Route::post('complete-case','ApiController@completeCase');
Route::post('cancel-case','ApiController@cancelCase');

Route::post('hospitals','ApiController@hospitals');
Route::post('pharmacies','ApiController@pharmacies');
Route::post('labs','ApiController@labs');

//patient routes
Route::post('sign-up', 'ApiController@signUp');
Route::post('add-dependant', 'ApiController@addDependant');
Route::post('update-dependant','ApiController@updateDependant');
Route::post('patient-details','ApiController@patientDetails');
Route::post('dependants','ApiController@dependants');
Route::post('add-feedback','ApiController@addFeedback');

Route::post('select-lab','ApiController@selectLab');

Route::post('patient-edit-profile','ApiController@patientEditProfile');


Route::post('new-case','ApiController@newCase');


Route::post('add-medical-history','ApiController@addMedicalHistory');
Route::post('social-sign-up', 'ApiController@socialSignUp');

Route::post('sign-in', 'ApiController@signIn' );
Route::post('social-sign-in','ApiController@socialSignIn');


Route::post('send-sms-verification-code','ApiController@sendSMSVerification');
Route::post('confirm-sms-verification-code','ApiController@confirmSMSVerification');


Route::post('send-email-verification-code','ApiController@sendEmailVerification');
Route::post('confirm-email-verification-code','ApiController@confirmEmailVerification');

Route::post('patient-upload-test-result','ApiController@patientUploadTestResult');

//doctor routes

Route::post('doctor-sign-in', 'ApiController@doctorSignIn' );
Route::post('doctor-sign-up','ApiController@doctorSignUp');
Route::post('doctor-edit-profile','ApiController@doctorEditProfile');

Route::post('doctor-upload-file','ApiController@doctorUploadFile');
Route::post('doctor-remove-file','ApiController@doctorRemoveFile');

Route::post('doctor-add-specialization','ApiController@doctorAddSpecialization');
Route::post('doctor-remove-specialization','ApiController@doctorRemoveSpecialization');

Route::post('doctor-add-language','ApiController@doctorAddLanguage');
Route::post('doctor-remove-language','ApiController@doctorRemoveLanguage');

Route::post('doctor-request-test','ApiController@doctorRequestTest');
Route::post('doctor-remove-test-request','ApiController@doctorRemoveTestRequest');

Route::post('doctor-update-bank-details','ApiController@doctorUpdateBankDetails');

Route::post('accept-case','ApiController@acceptCase');
Route::post('unaccept-case','ApiController@unacceptCase');
Route::post('available-cases','ApiController@availableCases');
Route::post('doctor-details','ApiController@doctorDetails');
Route::post('doctor-add-prescription','ApiController@doctorAddPrescription');
Route::post('doctor-add-diagnosis','ApiController@doctorAddDiagnosis');

Route::post('doctor-add-case-update','ApiController@doctorAddCaseUpdate');
Route::post('handle-card-payment','ApiController@handleCardPayment');
