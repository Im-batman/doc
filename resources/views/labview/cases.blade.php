@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="content sm-gutter">
            <div class="page-title">
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple ">
                        <div class="grid-title">
                            <h4>Cases <span class="semi-bold"> <label class="label label-success">All</label> </span></h4>
                        </div>
                        <div class="grid-body ">
                            <table class="table table-striped" id="example2">
                                <thead>
                                <tr>
                                    <th>Case ID</th>
                                    <th>Status</th>
                                    <th>Patient</th>
                                    <th>Doctor</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cases as $case)
                                    <tr>
                                        <td>{{$case->casid}}</td>
                                        <td>{{$case->status}}</td>
                                        <td>{{$case->Patient->fname}} {{$case->Patient->sname}}</td>
                                        <td>{{$case->Doctor->fname}} {{$case->Doctor->sname}}</td>
                                        <td>
                                            <a href="{{url('case/' . $case->casid)}}">View</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>




@endsection















