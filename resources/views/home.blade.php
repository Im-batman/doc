
@extends('layouts.admin')

@section('content')

    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div id="portlet-config" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"></button>
                <h3>Widget Settings</h3>
            </div>
            <div class="modal-body"> Widget settings form goes here </div>
        </div>
        <div class="clearfix"></div>
        <div class="content sm-gutter">
            <div class="page-title">
            </div>



            <script>
                $(document).ready(function () {
                    $('#cases').click(function () {
                        window.location = '{{url('manage-cases')}}';
                    });
                    $('#patients').click(function () {
                        window.location = '{{url('manage-patients')}}';
                    });
                    $('#doctors').click(function () {
                        window.location = '{{url('manage-doctors')}}';
                    });
                    $('#revenue').click(function () {
                        window.location = '{{url('manage-payments')}}';
                    });
                    $('#hospitals').click(function () {
                        window.location = '{{url('manage-hospitals')}}';
                    });
                    $('#pharmacies').click(function () {
                        window.location = '{{url('manage-pharmacies')}}';
                    });
                    $('#labs').click(function () {
                        window.location = '{{url('manage-labs')}}';
                    });
                })
            </script>



            <style>
                .dashboard .tiles:hover{
                    cursor: pointer;
                }
            </style>


            @if(Auth::user()->role == "Admin")
                <div class="row 2col dashboard">
                    <div class="col-md-3 col-sm-6 m-b-10">
                        <div id="cases" class="tiles blue ">
                            <div class="tiles-body">
                                <div class="controller">
                                    <a href="javascript:;" class="reload"></a>
                                </div>
                                <div class="heading"> Cases </div>
                                <div class="heading"> <span id="availableCases" class="animate-number" data-value="{{count($cases)}}" data-animation-duration="1200">0</span> </div>
                                {{--<div class="progress transparent progress-small no-radius">--}}
                                    {{--<div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="26.8%"></div>--}}
                                {{--</div>--}}
                                <div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp; {{count($pendingCases)}} <span class="blend">pending</span></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 m-b-10">
                        <div id="patients" class="tiles green ">
                            <div class="tiles-body">
                                <div class="controller">
                                    <a href="javascript:;" class="reload"></a>
                                </div>
                                <div class="heading"> Patients </div>
                                <div class="heading"> <span class="animate-number" data-value="{{count($patients)}}" data-animation-duration="1000">0</span> </div>
                                <div class="progress transparent progress-small no-radius">
                                    <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="79%"></div>
                                </div>
                                <div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp; {{count($patientsToday)}} <span class="blend">signed up today</span></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 m-b-10">
                        <div id="doctors" class="tiles red ">
                            <div class="tiles-body">
                                <div class="controller">
                                    <a href="javascript:;" class="reload"></a>
                                </div>
                                <div class="heading"> Doctors</div>
                                <div class="heading"> <span class="animate-number" data-value="{{count($doctors)}}" data-animation-duration="1200">0</span> </div>
                                <div class="progress transparent progress-white progress-small no-radius">
                                    <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="45%"></div>
                                </div>
                                <div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp;{{count($doctorsToday)}} <span class="blend">signed up today</span></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 m-b-10">
                    <div id="revenue" class="tiles purple  ">
                        <div class="tiles-body">
                            <div class="controller">
                                <a href="javascript:;" class="reload"></a>
                            </div>
                            <div class="heading"> Revenue </div>
                            <div class="row-fluid">
                                <div class="heading"> $ <span class="animate-number" data-value="{{count($payments)}}" data-animation-duration="700">0</span> </div>
                                <div class="progress transparent progress-white progress-small no-radius">
                                    <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="12%"></div>
                                </div>
                            </div>
                            <div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp; ${{count($paymentsToday)}} <span class="blend">generated today</span></span>
                            </div>
                        </div>
                    </div>
                </div>
                </div>

                <div class="row 2col">
                    <div class="col-md-3 col-sm-6 m-b-10">
                        <div id="hospitals" class="tiles purple  ">
                            <div class="tiles-body">
                                <div class="controller">
                                    <a href="javascript:;" class="reload"></a>
                                </div>
                                <div class="heading"> Hospitals </div>
                                <div class="row-fluid">
                                    <div class="heading"> <span class="animate-number" data-value="{{count($hospitals)}}" data-animation-duration="700">0</span> </div>
                                    <div class="progress transparent progress-white progress-small no-radius">
                                        <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="12%"></div>
                                    </div>
                                </div>
                                <div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp;{{count($hospitalsToday)}} <span class="blend">were added today</span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 m-b-10">
                        <div id="labs" class="tiles red ">
                            <div class="tiles-body">
                                <div class="controller">
                                    <a href="javascript:;" class="reload"></a>
                                </div>
                                <div class="heading"> Labs </div>
                                <div class="heading"> <span class="animate-number" data-value="{{count($labs)}}" data-animation-duration="1200">0</span> </div>
                                <div class="progress transparent progress-white progress-small no-radius">
                                    <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="45%"></div>
                                </div>
                                <div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp; {{count($labsToday)}} <span class="blend">were added today</span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 m-b-10">
                        <div id="pharmacies" class="tiles green ">
                            <div class="tiles-body">
                                <div class="controller">
                                    <a href="javascript:;" class="reload"></a>
                                </div>
                                <div class="heading"> Pharmacies </div>
                                <div class="heading"> <span class="animate-number" data-value="{{count($pharmacies)}}" data-animation-duration="1000">0</span> </div>
                                <div class="progress transparent progress-small no-radius">
                                    <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="79%"></div>
                                </div>
                                <div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp; {{count($pharmaciesToday)}} <span class="blend">were added today</span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 m-b-10">
                        <div id="tickets" class="tiles blue ">
                            <div class="tiles-body">
                                <div class="controller">
                                    <a href="javascript:;" class="reload"></a>
                                </div>
                                {{--<div class="tiles-title"> Cases </div>--}}
                                <div class="heading"> Tickets </div>
                                <div class="heading"> <span class="animate-number" data-value="0" data-animation-duration="1200">0</span> </div>
                                <div class="progress transparent progress-small no-radius">
                                    <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="26.8%"></div>
                                </div>
                                <div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp; 74% <span class="blend">pending</span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @else

                <!-- dashboard for non-admins -->
                
                <div class="col-md-3 col-sm-6 m-b-10">
                    <div id="cases" class="tiles blue ">
                        <div class="tiles-body">
                            <div class="controller">
                                <a href="javascript:;" class="reload"></a>
                            </div>
                            <div class="heading"> Cases </div>
                            <div class="heading"> <span class="animate-number" data-value="{{count($cases)}}" data-animation-duration="1200">0</span> </div>
                            {{--<div class="progress transparent progress-small no-radius">--}}
                                {{--<div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="26.8%"></div>--}}
                            {{--</div>--}}
                            <div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp; {{count($pendingCases)}} <span class="blend">pending</span></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 m-b-10">
                    <div id="patients" class="tiles green ">
                        <div class="tiles-body">
                            <div class="controller">
                                <a href="javascript:;" class="reload"></a>
                            </div>
                            <div class="heading"> Patients </div>
                            <div class="heading"> <span class="animate-number" data-value="{{count($patients)}}" data-animation-duration="1000">0</span> </div>
                            {{--<div class="progress transparent progress-small no-radius">--}}
                                {{--<div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="79%"></div>--}}
                            {{--</div>--}}
                            <div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp; {{count($patientsToday)}} <span class="blend">signed up today</span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 m-b-10">
                    <div id="revenue" class="tiles purple  ">
                        <div class="tiles-body">
                            <div class="controller">
                                <a href="javascript:;" class="reload"></a>
                            </div>
                            <div class="heading"> Revenue </div>
                            <div class="row-fluid">
                                <div class="heading"> $ <span class="animate-number" data-value="{{count($payments)}}" data-animation-duration="700">0</span> </div>
                                <div class="progress transparent progress-white progress-small no-radius">
                                    <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="12%"></div>
                                </div>
                            </div>
                            <div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp; ${{count($paymentsToday)}} <span class="blend">generated today</span></span>
                            </div>
                        </div>
                    </div>
                </div>

            @endif




        </div>
    </div>






@endsection












