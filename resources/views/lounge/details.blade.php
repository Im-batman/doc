@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title">
                <a href="{{url('manage-lounge')}}">
                    <i class="icon-custom-left"></i>
                </a>
                <h3>Lounge Article <span>#{{$loungeItem->lid}}</span></h3>

                <div class="pull-right">
                    {{--@if($feedback->isViewed == 0)--}}
                        {{--<a href="{{url('feedback-viewed/' . $feedback->fdid)}}" class="btn btn-primary">Mark as Viewed</a>--}}
                    {{--@endif--}}

                </div>
            </div>
        </div>


        <div class="row-fluid">
            <div class="grid simple col-md-10 col-md-offset-1">
                <div class="grid-title no-border">

                    {{$loungeItem->title}}

                </div>
                <div class="grid-body no-border">
                    <br>
                    <div class="row">
                        <p>
                            {{$loungeItem->content}}
                        </p>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4">
                            <img src="{{$loungeItem->image}}" class="img img-thumbnail"/>
                        </div>
                        <div class="form-group col-md-4 col-sm-12">
                            <br>
                            <label class="form-label">Created By</label>
                            <span class="help">
                                <a href="{{url('user/' . $loungeItem->uid)}}">
                                    {{$loungeItem->User->fname}} {{$loungeItem->User->sname}}
                                </a>
                            </span>

                            @if(isset($loungeItem->link))
                            <br>
                            <label class="form-label">Link</label>
                            <span class="help">
                                <a target="_blank" href="{{$loungeItem->link}}">
                                    {{$loungeItem->link}}
                                </a>
                            </span>
                            @endif

                        </div>

                    </div>

                </div>
            </div>

        </div>



    </div>
@endsection