@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="row-fluid">
                <div align="center">

                    <form>
                        <select name="term" class="form-control inline" style="height: 40px !important;width:150px;">
                            <option value="fname">Firstname</option>
                            <option value="sname">Surname</option>
                            <option value="email">Email</option>
                            <option value="phone">Phone</option>
                        </select>
                        <input name="value" type="text" class="no-boarder " placeholder="Search" style="width:250px;">
                        <button class="btn btn-primary"> <i class="material-icons">search</i></button>

                    </form>
                </div>

                <div class="span12">
                    <div class="grid simple ">
                        <div class="grid-title">
                            <h4>Add <span class="semi-bold">Article</span></h4>
                            <div class="tools">
                                <a href="javascript:;" class="expand"></a>
                                <a href="#grid-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                                <a href="javascript:;" class="remove"></a>
                            </div>
                        </div>
                        <div class="grid-body ">

                            <form method="post" enctype="multipart/form-data" action="{{url('add-lounge-item')}}">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title">
                                </div>

                                <div class="form-group">
                                    <label>Link</label>
                                    <input type="text" class="form-control" name="link">
                                </div>


                                <div class="form-group">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="image">
                                </div>

                                <div class="form-group">
                                    <label>Content</label>
                                    <textarea name="content" class="form-control" required></textarea>
                                </div>

                                <button class="btn btn-primary">Add</button>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>




@endsection















