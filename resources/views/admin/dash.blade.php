
@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="content sm-gutter">
            <div class="page-title">
            </div>

            <script>
                $(document).ready(function () {
                    $('.casesDiv').click(function () {
                        window.location = '{{url('manage-cases')}}';
                    });
                    $('#patientsDiv').click(function () {
                        window.location = '{{url('manage-patients')}}';
                    });
                    $('#doctorsDiv').click(function () {
                        window.location = '{{url('manage-doctors')}}';
                    });
                    $('#revenueDiv').click(function () {
                        window.location = '{{url('manage-payments')}}';
                    });
                    $('#hospitalsDiv').click(function () {
                        window.location = '{{url('manage-hospitals')}}';
                    });
                    $('#pharmaciesDiv').click(function () {
                        window.location = '{{url('manage-pharmacies')}}';
                    });
                    $('#labsDiv').click(function () {
                        window.location = '{{url('manage-labs')}}';
                    });
                })
            </script>

            <style>
                .dashboard .tiles:hover{
                    cursor: pointer;
                }
            </style>

            <div class="row 2col dashboard">
                <div class="col-md-4 col-sm-6 m-b-10">
                    <div class="tiles blue casesDiv">
                        <div class="tiles-body">
                            <div class="controller">
                                <a href="javascript:;" class="reload"></a>
                            </div>
                            <div class="tiles-title"> Cases </div>
                            <div class="heading"> Available </div>
                            <div class="heading"> <span id="availableCases" class="animate-number" data-value="{{count($availableCases)}}" data-animation-duration="1200">0</span> </div>
                            {{--<div class="progress transparent progress-small no-radius">--}}
                                {{--<div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="26.8%"></div>--}}
                            {{--</div>--}}
                            {{--<div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp; {{count($availableCases)}} <span class="blend">today</span></span>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 m-b-10">
                    <div class="tiles casesDiv" style="background-color: darkorange">
                        <div class="tiles-body">
                            <div class="controller">
                                <a href="javascript:;" class="reload"></a>
                            </div>
                            <div class="tiles-title"> Cases </div>
                            <div class="heading"> Pending </div>
                            <div class="heading"> <span id="pendingCases" class="animate-number" data-value="{{count($pendingCases)}}" data-animation-duration="1200">0</span> </div>
                            {{--<div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp; {{count($pendingCases)}} <span class="blend">today</span></span>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 m-b-10">
                    <div class="tiles casesDiv" style="background-color: green">
                        <div class="tiles-body">
                            <div class="controller">
                                <a href="javascript:;" class="reload"></a>
                            </div>
                            <div class="tiles-title"> Cases </div>
                            <div class="heading"> Complete </div>
                            <div class="heading"> <span id="completedCases" class="animate-number" data-value="{{count($completedCases)}}" data-animation-duration="1200">0</span> </div>
                            {{--<div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp; {{count($pendingCases)}} <span class="blend">today</span></span>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>


                <!-- end cases  -->

                <div class="col-md-6 col-sm-6 m-b-10">
                    <div id="patientsDiv" class="tiles green ">
                        <div class="tiles-body">
                            <div class="controller">
                                <a href="javascript:;" class="reload"></a>
                            </div>
                            <div class="heading"> Patients </div>
                            <div class="heading"> <span id="patients" class="animate-number" data-value="{{count($patients)}}" data-animation-duration="1000">0</span> </div>
                            <div class="description"><i class="icon-custom-right"></i><span id="patientsToday" class="text-white mini-description ">&nbsp; {{count($patientsToday)}} <span class="blend">signed up today</span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 m-b-10">
                    <div id="doctorsDiv" class="tiles red ">
                        <div class="tiles-body">
                            <div class="controller">
                                <a href="javascript:;" class="reload"></a>
                            </div>
                            <div class="heading"> Doctors</div>
                            <div class="heading"> <span id="doctors" class="animate-number" data-value="{{count($doctors)}}" data-animation-duration="1200">0</span> </div>
                            <div class="description"><i class="icon-custom-right"></i><span id="doctorsToday" class="text-white mini-description ">&nbsp;{{count($doctorsToday)}} <span class="blend">signed up today</span></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row 2col">
                <div class="col-md-3 col-sm-6 m-b-10">
                    <div id="hospitalsDiv" class="tiles purple  ">
                        <div class="tiles-body">
                            <div class="controller">
                                <a href="javascript:;" class="reload"></a>
                            </div>
                            <div class="heading"> Hospitals </div>
                            <div class="row-fluid">
                                <div class="heading"> <span id="hospitals" class="animate-number" data-value="{{count($hospitals)}}" data-animation-duration="700">0</span> </div>
                            </div>
                            <div class="description"><i class="icon-custom-right"></i><span id="hospitalsToday" class="text-white mini-description ">&nbsp;{{count($hospitalsToday)}} <span class="blend">were added today</span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 m-b-10">
                    <div id="labsDiv" class="tiles red ">
                        <div class="tiles-body">
                            <div class="controller">
                                <a href="javascript:;" class="reload"></a>
                            </div>
                            <div class="heading"> Labs </div>
                            <div class="heading"> <span id="labs" class="animate-number" data-value="{{count($labs)}}" data-animation-duration="1200">0</span> </div>
                            <div class="description"><i class="icon-custom-right"></i><span id="labsToday" class="text-white mini-description ">&nbsp; {{count($labsToday)}} <span class="blend">were added today</span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 m-b-10">
                    <div id="pharmaciesDiv" class="tiles green ">
                        <div class="tiles-body">
                            <div class="controller">
                                <a href="javascript:;" class="reload"></a>
                            </div>
                            <div class="heading"> Pharmacies </div>
                            <div class="heading"> <span id="pharmacies" class="animate-number" data-value="{{count($pharmacies)}}" data-animation-duration="1000">0</span> </div>
                            <div class="description"><i class="icon-custom-right"></i><span id="pharmaciesToday" class="text-white mini-description ">&nbsp; {{count($pharmaciesToday)}} <span class="blend">were added today</span></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 m-b-10">
                    <div id="revenueDiv" class="tiles blue  ">
                        <div class="tiles-body">
                            <div class="controller">
                                <a href="javascript:;" class="reload"></a>
                            </div>
                            <div class="heading"> Revenue </div>
                            <div class="row-fluid">
                                <div class="heading"> $ <span id="revenue" class="animate-number" data-value="{{count($payments)}}" data-animation-duration="700">0</span> </div>
                            </div>
                            <div class="description"><i class="icon-custom-right"></i><span id="revenueToday" class="text-white mini-description ">&nbsp; ${{count($paymentsToday)}} <span class="blend">generated today</span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function(){

            var previousComplete;
            var previousPending;
            var previousAvailable;


            setInterval(function () {
                $.ajax({
                    url:"{{url('api/live-update')}}",
                    method: "post",
                    data:{
                        _token : "{{csrf_token()}}"
                    },
                    success: function (response) {

                        if(
                        response.availableCases > previousAvailable
                        ){
                        var audioElement = document.createElement('audio');
                        audioElement.setAttribute('src', "{{url('notification.mp3')}}");
                        audioElement.play();
                        //
                        }

                        $('#availableCases').text(response.availableCases);
                        $('#pendingCases').text(response.pendingCases);
                        $('#completedCases').text(response.completedCases);

                        $('#patients').text(response.patients );

                        $('#patientsToday').html(" " + response.patientsToday + " <span class=\"blend\">signed up today</span>");
                        $('#doctors').text(response.doctors);
                        $('#doctorsToday').html(" " + response.doctorsToday + " <span class=\"blend\">signed up today</span>");

                        $('#hospitals').text(response.hospitals);
                        $('#hospitalsToday').text(response.hospitalsToday);

                        $('#labs').text(response.labs);
                        $('#labsToday').text(response.labsToday);

                        $('#pharmacies').text(response.pharmacies);
                        $('#pharmaciesToday').text(response.pharmaciesToday);


                        previousAvailable  = response.availableCases;
                        previousPending   = response.pendingCases;
                        previousComplete = response.completedCases;

                    },
                    error: function (error) {

                    }
                });

            },5000);
        });
    </script>


@endsection












