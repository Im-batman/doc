@extends('layouts.admin')

@section('content')




    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div class="clearfix"></div>
        <div class="content sm-gutter">

            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple ">
                        <div class="grid-title">
                            <h4>Hospitals</h4>
                        </div>
                        <div class="grid-body ">
                            <table class="table table-striped" id="example2">
                                <thead>

                                @if(count($hospitals) > 0)
                                <tr>
                                    <th>#ID </th>
                                    <th>Name</th>
                                    <th>City</th>
                                    <th>Address</th>
                                    <th>Registered</th>
                                </tr>

                                </thead>
                                <tbody>

                                @foreach($hospitals as $h)
                                <tr class="odd gradeX">
                                    <td>#{{$h->hid}}</td>
                                    <td>{{$h->name}}</td>
                                    <td>{{$h->city}}</td>
                                    <td>{{$h->address}}</td>
                                    <td class="center"> {{$h->created_at}}</td>
                                </tr>
                                    @endforeach

                                    @else
                                        <h2 style=" text-align: center"> No hospitals have been added</h2>
                                    @endif

                                </tbody>
                            </table>
                            {{$hospitals->links()}}
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>




@endsection















