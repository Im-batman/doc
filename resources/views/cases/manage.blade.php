<?php use Carbon\Carbon; use Illuminate\Support\Facades\Input; ?>
@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            <div class="page-title">
            </div>


            <div class="row-fluid">

                <div class="span12">
                    <div class="grid simple ">
                        <div class="grid-title">
                            <h4 style="margin: 10px 0 0 0;">View <span class="semi-bold">Cases</span></h4>
                            <div class="tools">

                                <div class="page btn-group" role="group" aria-label="Basic example">
                                    <a href="{{url('manage-cases?pages=10')}}" type="button"
                                       @if(session()->get('pages') == 10)
                                       style="color:white"
                                       class="btn btn-primary"
                                       @else
                                       class="btn"
                                            @endif>10</a>

                                    <a href="{{url('manage-cases?pages=25')}}" type="button"
                                       @if(session()->get('pages') == 25)
                                       style="color:white"
                                       class="btn btn-primary"
                                       @else
                                       class="btn"
                                        @endif>25</a>

                                    <a href="{{url('manage-cases?pages=50')}}" type="button"
                                       @if(session()->get('pages') == 50)
                                       style="color:white"
                                       class="btn btn-primary"
                                        @else
                                           class="btn"
                                        @endif>50</a>
                                </div>

                                <div class="filtericons" align="right" >

                                    <div class="btn-group btn-group-xs" role="group" aria-label="...">

                                        <script>
                                            $(document).ready(function () {
                                                $('.dropdown-menu li').on('click',function(e){
                                                    e.preventDefault();
                                                    var filterlabel = $("#filterlabel");
                                                    var value =  $(this).data("key");
                                                    var selected = $(this).text();
                                                    filterlabel.html(selected + '<span class="caret"></span>');
                                                    $('#by').val(value);
                                                    console.log(value);


                                                });

                                            });
                                        </script>

                                        <form class="form-inline" action="{{url('manage-cases')}}" method="get">
                                            <div class="input-group">

                                                <div class="input-group-btn">
                                                    <button type="button" id="filterlabel" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Select <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li data-key="fname"><a href="#">Firstname</a></li>
                                                        <li><a href="#">Surname</a></li>
                                                        <li><a href="#">Email</a></li>
                                                        <li><a href="#">Phone</a></li>
                                                    </ul>
                                                    <input name="by" id="by" type="hidden">
                                                    <input name="pages" value="{{$input::get('pages')}}" type="hidden">
                                                </div><!-- /btn-group -->

                                                <input style="border-top-right-radius: 5px;border-bottom-right-radius: 5px;" class="form-control" value="" placeholder="Search" name="term" type="text">
                                            </div>

                                        </form>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="grid-body ">
                            <table class="table table-striped">
                                @if(count($cases) > 0)
                                    <thead>
                                    <tr>
                                        <th>
                                            <a href="{{url('/')}}" style="color:grey">
                                                CASE ID
                                                <i style="float:left;margin-right:5px;" class="fa fa-sort-down"></i>

                                            </a>
                                        </th>
                                        <th>Status</th>
                                        <th>Patient</th>
                                        <th>Gender</th>
                                        <th>Specialization</th>
                                        <th>Doctor</th>
                                        <th></th>


                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($cases as $case )

                                        <tr class="odd gradeX">
                                            <td>#{{str_pad($case->casid,6,"0",STR_PAD_LEFT)}}
                                            </td>
                                            <td>
                                                @if($case->status == 'Available')
                                                    <label class="label label-primary">Available</label>
                                                @endif
                                                @if($case->status == 'Pending')
                                                    <label class="label label-warning">Pending</label>
                                                @endif
                                                @if($case->status == 'Completed')
                                                    <label class="label label-success">Completed</label>
                                                @endif
                                                @if($case->status == 'Cancelled')
                                                    <label class="label label-danger">Cancelled</label>
                                                @endif

                                            </td>
                                            <td>
                                                @if($case->type == 'patient')
                                                    <a href="{{url('patient/' . $case->Patient->patid)}}">
                                                        {{$case->Patient->fname}} {{$case->Patient->sname}}
                                                    </a>
                                                @endif

                                                @if($case->type == 'dependant')
                                                    <a href="{{url('dependant/' . $case->Dependant->pdid)}}">
                                                        {{$case->Dependant->fname}} {{$case->Dependant->sname}}
                                                    </a>
                                                @endif

                                            </td>
                                            <td>{{$case->Patient->gender}}</td>
                                            <td>
                                                @if(isset($case->Specialization))
                                                {{$case->Specialization->name}}
                                                    @else
                                                    Not Assigned
                                                @endif
                                            </td>
                                            <td>
                                                @if(isset($case->Doctor))
                                                    <a href="{{url('doctor/' . $case->Doctor->docid)}}">
                                                        {{$case->Doctor->fname}} {{$case->Doctor->sname}}
                                                    </a>
                                                @else
                                                    Not Accepted Yet
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{url('case/' . $case->casid)}}" class="label label-primary">View</a>
                                            </td>
                                        </tr>

                                    @endforeach
                                    @else

                                        <h3 style="text-align: center"> There are no cases on the system @if($input::has("filter")) that match the filter @endif </h3>

                                    @endif


                                    </tbody>

                            </table>
                            {{$cases->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection















