<?php use Carbon\Carbon; ?>
@extends('layouts.admin')

@section('content')

    @if($case->type == "patient")
        <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title" style=" margin-left: 60px;margin-bottom: 50px;margin-right: 60px;">
                <a href="{{url('manage-cases')}}">
                    <i class="icon-custom-left"></i>
                </a>

                <div class="pull-right">
                    @if(!isset($case->spid))
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Add Specialization
                        </button>
                    @else
                        <button class="btn btn-warning" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Modify Specialization
                        </button>
                    @endif

                </div>
            </div>
        </div>


        <div class="row-fluid">

            <div class="grid simple col-md-10 col-md-offset-1" style="margin-bottom: 5px !important;">
                <div class="collapse" id="collapseExample">

                    <div class="grid-body no-border" align="center" style="padding: 10px;">

                        <form method="post" action="{{url('assign-specialization')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="casid" value="{{$case->casid}}">

                            <label class="inline">Add Specialization</label>
                            <select name="spid">
                                @foreach($specializations as $specialization)
                                    <option value="{{$specialization->spid}}">{{$specialization->name}}</option>
                                @endforeach

                            </select>
                            @if(!isset($case->spid))
                                <button class="btn btn-primary">Add</button>
                            @else
                                <button class="btn btn-warning">Modify</button>
                            @endif
                        </form>

                    </div>

                </div>
            </div>

            <div class="grid simple col-md-10 col-md-offset-1">
                <div class="grid-body no-border">
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <img src="{{$case->Patient->image}}" class="img img-thumbnail" style="width: 100%"/>

                            @if($case->status == 'Available')
                                <label class="label label-primary">Available</label>
                            @endif
                            @if($case->status == 'Pending')
                                <label class="label label-warning">Pending</label>
                            @endif
                            @if($case->status == 'Completed')
                                <label class="label label-success">Completed</label>
                            @endif
                            @if($case->status == 'Cancelled')
                                <label class="label label-danger">Cancelled</label>
                            @endif



                            @if($case->Patient->isSuspended == 1)
                                <label class="label label-danger">Suspended</label>
                            @endif

                        </div>

                        <div class="col-md-8">
                            <h3 style="margin: 0 0 10px 0;">
                                <span class="semi-bold">
                                    #{{str_pad($case->casid,6,"0",STR_PAD_LEFT)}}
                                </span>
                            </h3>
                        </div>

                        <div class="form-group col-md-4 col-sm-12">
                            <label class="form-label">Name</label>
                            <span class="help">
                                <a style="text-decoration: #0a6ea0;" href="{{url('patient/'.$case->Patient->patid)}}">
                                    {{$case->Patient->fname}} {{$case->Patient->sname}}
                                </a>
                            </span>
                            <br>
                            <label class="form-label">Email</label>
                            <span class="help">{{$case->Patient->email}}</span>
                            <br>
                            <label class="form-label">Phone</label>
                            <span class="help">{{$case->Patient->phone}}</span>
                            <br>
                            <label class="form-label">Gender</label>
                            <span class="help">{{$case->Patient->gender}}</span>
                            <br>
                            @if(isset($case->Doctor))
                                <label class="form-label">Doctor</label>
                                <span class="help">
                                    <a href="{{url('doctor/' . $case->Doctor->docid)}}">
                                        {{$case->Doctor->fname}} {{$case->Doctor->sname}}
                                    </a>
                                </span>
                            @else
                                <label class="form-label">Doctor</label>
                                <span class="help">Not accepted yet</span>
                            @endif

                        </div>

                        <div class="form-group col-md-4 col-sm-12">

                            <label class="form-label">DOB</label>
                            <span class="help">{{$case->Patient->dob}}</span>
                            <br>
                            <label class="form-label">Height</label>
                            <span class="help">{{$case->Patient->height}}cm</span>
                            <br>
                            <label class="form-label">Weight</label>
                            <span class="help">{{$case->Patient->weight}}kg</span>
                            <br>
                            @if(isset($case->Specialization))
                                <label class="form-label">Specialization</label>
                                <span class="help">{{$case->Specialization->name}}</span>
                            @endif

                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div style="padding: 0 15px;">
                            <h4 class="title">Description</h4>
                            <p style="text-align: justify">
                                {!! $case->description !!}
                            </p>

                        </div>
                    </div>

                </div>
            </div>
        </div>

            <div class="row-fluid">
                <div class="grid simple col-md-10 col-md-offset-1">
                    <div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                                    Requested Tests
                                </a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Case Doctors</a></li>
                            {{--<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>--}}
                            {{--<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>--}}
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <div class="grid-body no-border">

                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Test Title</th>
                                            <th>Test Description</th>
                                            <th>Date Requested</th>
                                            <th>Requested By</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($case->Tests as $test)
                                            <tr>
                                                <td>{{$test->Test->title}}</td>
                                                <td>{{$test->Test->description}}</td>
                                                <td>{{$test->created_at->toDayDateTimeString()}}</td>
                                                <td>
                                                    <a href="{{url('doctor/' . $test->docid )}}">
                                                        {{$test->Doctor->fname}} {{$test->Doctor->sname}}
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>


                                </div>


                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile">
                                <div class="grid-body no-border">

                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Doctor</th>
                                            <th>Date Started</th>
                                            <th>Date Ended</th>
                                            <th>Notes</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($case->Doctors as $doctor)
                                            <tr>
                                                <td>
                                                    <a href="{{url('doctor/' . $doctor->docid )}}">
                                                        {{$doctor->Doctor->fname}} {{$doctor->Doctor->sname}}
                                                    </a>
                                                </td>

                                                <td>{{$doctor->created_at->toDayDateTimeString()}}</td>
                                                <td>
                                                    @if(isset($doctor->dateEnded))
                                                        {{Carbon::createFromFormat("Y-m-d H:i:s",$doctor->dateEnded)->toDayDateTimeString()}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(isset($doctor->notes))
                                                        {{$doctor->notes}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>


                                </div>

                            </div>
                            {{--<div role="tabpanel" class="tab-pane" id="messages">...</div>--}}
                            {{--<div role="tabpanel" class="tab-pane" id="settings">...</div>--}}
                        </div>
                    </div>
                </div>
            </div>


        @if(isset($case->Feedback))

            <div class="row-fluid white-box">
                <div class="grid simple col-md-10 col-md-offset-1">

                    <div class="grid-title no-border">
                        <h4>
                            Feedback
                        </h4>
                    </div>

                    <div class="grid-body no-border">
                        <p>
                            {{$case->Feedback->content}}
                        </p>

                    </div>
                </div>
            </div>

        @endif

    </div>
    @else
        <div class="page-content">

            <div class="clearfix"></div>
            <div class="content sm-gutter">
                @include('notification')
                <div class="page-title">
                    <a href="{{url('manage-cases')}}">
                        <i class="icon-custom-left"></i>
                    </a>

                    <div class="pull-right">
                        @if(!isset($case->spid))
                            <a class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                Add Specialization
                            </a>
                        @else
                            <a class="btn btn-warning" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                Modify Specialization
                            </a>
                        @endif

                        <a style="margin-right: 0 !important;" class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample1" aria-expanded="false" aria-controls="collapseExample">
                           View Sponsor Details
                        </a>

                    </div>
                </div>
            </div>


            <div class="row-fluid">

                <div class="grid simple col-md-10 col-md-offset-1" style="margin-bottom: 5px !important;">
                    <div class="collapse" id="collapseExample">

                        <div class="grid-body no-border" align="center" style="padding: 10px;">


                            <form method="post" action="{{url('assign-specialization')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="casid" value="{{$case->casid}}">

                                <label class="inline">Add Specialization</label>
                                <select name="spid" style="border-radius: 5px;height: 37px !important;">
                                    @foreach($specializations as $specialization)
                                        <option value="{{$specialization->spid}}">{{$specialization->name}}</option>
                                    @endforeach

                                </select>
                                @if(!isset($case->spid))
                                    <button style="height: 37px !important;margin-top: -5px !important;"
                                            class="btn btn-primary">Add</button>
                                @else
                                    <button style="height: 37px !important;margin-top: -5px !important;"
                                            class="btn btn-warning">Modify</button>
                                @endif
                            </form>

                        </div>

                    </div>
                </div>


                <div class="grid simple col-md-10 col-md-offset-1" style="margin-bottom: 5px !important;">
                    <div class="collapse" id="collapseExample1">
                        <div class="grid-title no-border">
                            <h4>
                                Parent Details
                            </h4>
                        </div>
                        <div class="grid-body no-border">
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{$case->Patient->image}}" class="img img-thumbnail" style="width: 100%"/>


                                @if($case->Patient->isSuspended == 1)
                                    <label class="label label-danger">Suspended</label>
                                @endif

                            </div>

                            <div class="form-group col-md-4 col-sm-12">

                                <label class="form-label">Name</label>
                                <span class="help">
                                    <a style="text-decoration: #0a6ea0;" href="{{url('patient/'.$case->Patient->patid)}}">
                                        {{$case->Patient->fname}} {{$case->Patient->sname}}
                                    </a>
                                </span>
                                <br>
                                <label class="form-label">Email</label>
                                <span class="help">{{$case->Patient->email}}</span>
                                <br>
                                <label class="form-label">Phone</label>
                                <span class="help">{{$case->Patient->phone}}</span>
                                <br>
                                <label class="form-label">Gender</label>
                                <span class="help">{{$case->Patient->gender}}</span>

                            </div>

                            <div class="form-group col-md-4 col-sm-12">
                                <label class="form-label">DOB</label>
                                <span class="help">{{$case->Patient->dob}}</span>
                                <br>
                                <label class="form-label">Height</label>
                                <span class="help">{{$case->Patient->height}}cm</span>
                                <br>
                                <label class="form-label">Weight</label>
                                <span class="help">{{$case->Patient->weight}}kg</span>
                                <br>

                                @if(isset($case->Doctor))
                                    <label class="form-label">Doctor</label>
                                    <span class="help">
                                <a href="{{url('doctor/' . $case->Doctor->docid)}}">
                                    {{$case->Doctor->fname}} {{$case->Doctor->sname}}
                                </a>
                            </span>
                                    <br>
                                @else
                                    <label class="form-label">Doctor</label>
                                    <span class="help">Not accepted yet</span>
                                    <br>
                                @endif

                                @if(isset($case->Specialization))
                                    <label class="form-label">Specialization</label>
                                    <span class="help">{{$case->Specialization->name}}</span>
                                    <br>
                                @endif

                            </div>
                        </div>

                    </div>
                    </div>
                </div>

                <div class="grid simple col-md-10 col-md-offset-1">

                    <div class="grid-body no-border">
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{$case->Dependant->image}}" style="width: 100%;" class="img img-thumbnail"/>
                                @if($case->status == 'Available')
                                    <label class="label label-primary">Available</label>
                                @endif
                                @if($case->status == 'Pending')
                                    <label class="label label-warning">Pending</label>
                                @endif
                                @if($case->status == 'Completed')
                                    <label class="label label-success">Completed</label>
                                @endif
                                @if($case->status == 'Cancelled')
                                    <label class="label label-danger">Cancelled</label>
                                @endif

                            </div>


                            <div class="col-md-8">
                                <h3 style="margin: 0 0 10px 0;">
                                    <span class="semi-bold">
                                        #{{str_pad($case->casid,6,"0",STR_PAD_LEFT)}}
                                    </span>
                                </h3>
                            </div>



                            <div class="form-group col-md-4 col-sm-12">
                                <br>
                                <label class="form-label">Name</label>
                                <span class="help">
                                    <a style="text-decoration: #0a6ea0;" href="{{url('dependant/'.$case->Dependant->pdid)}}">
                                        {{$case->Dependant->fname}} {{$case->Dependant->sname}}
                                    </a>
                                </span>
                                <br>
                                <label class="form-label">Parent</label>
                                <span class="help">
                                    <a style="text-decoration: #0a6ea0;" href="{{url('patient/'.$case->Patient->patid)}}">
                                        {{$case->Patient->fname}} {{$case->Patient->sname}}
                                    </a>
                                </span>
                                <br>
                                <label class="form-label">Email</label>
                                <span class="help">{{$case->Dependant->email}}</span>
                                <br>
                                <label class="form-label">Phone</label>
                                <span class="help">{{$case->Dependant->phone}}</span>
                                <br>
                                <label class="form-label">Gender</label>
                                <span class="help">{{$case->Dependant->gender}}</span>

                            </div>




                            <div class="form-group col-md-4 col-sm-12">

                                <br>
                                <label class="form-label">DOB</label>
                                <span class="help">{{$case->Dependant->dob}}</span>
                                <br>
                                <label class="form-label">Height</label>
                                <span class="help">{{$case->Dependant->height}}cm</span>
                                <br>
                                <label class="form-label">Weight</label>
                                <span class="help">{{$case->Dependant->weight}}kg</span>
                                <br>

                                @if(isset($case->Doctor))
                                    <label class="form-label">Doctor</label>
                                    <span class="help">
                                    <a href="{{url('doctor/' . $case->Doctor->docid)}}">
                                        {{$case->Doctor->fname}} {{$case->Doctor->sname}}
                                    </a>
                                </span>
                                    <br>
                                @else
                                    <label class="form-label">Doctor</label>
                                    <span class="help">Not accepted yet</span>
                                    <br>
                                @endif

                                @if(isset($case->Specialization))
                                    <label class="form-label">Specialization</label>
                                    <span class="help">{{$case->Specialization->name}}</span>
                                    <br>
                                @endif

                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div style="padding: 0 15px;">
                                <h4 class="title">Description</h4>
                                <p style="text-align: justify">
                                    {!! $case->description !!}
                                </p>

                            </div>
                        </div>

                    </div>

                </div>
            </div>





            <div class="row-fluid">
                <div class="grid simple col-md-10 col-md-offset-1">
                    <div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Prescriptions</a></li>
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                                    Requested Tests
                                </a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Case Doctors</a></li>
                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Feedback</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane" id="settings">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Code</th>
                                        <th>Doctor</th>
                                        <th>Date Added</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($case->Prescriptions as $prescription)
                                        <tr>
                                            <td>{{$prescription->prid}}</td>
                                            <td>{{$prescription->code}}</td>
                                            <td>
                                                <a href="{{url('doctor/' . $prescription->docid )}}">
                                                    {{$prescription->Doctor->fname}} {{$prescription->Doctor->sname}}
                                                </a>
                                            </td>
                                            <td>{{$prescription->created_at->toDayDateTimeString()}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <div class="grid-body no-border">

                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Test Title</th>
                                            <th>Test Description</th>
                                            <th>Date Requested</th>
                                            <th>Requested By</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($case->Tests as $test)
                                            <tr>
                                                <td>{{$test->Test->title}}</td>
                                                <td>{{$test->Test->description}}</td>
                                                <td>{{$test->created_at->toDayDateTimeString()}}</td>
                                                <td>
                                                    <a href="{{url('doctor/' . $test->docid )}}">
                                                        {{$test->Doctor->fname}} {{$test->Doctor->sname}}
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile">
                                <div class="grid-body no-border">

                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Doctor</th>
                                            <th>Date Started</th>
                                            <th>Date Ended</th>
                                            <th>Notes</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($case->Doctors as $doctor)
                                            <tr>
                                                <td>
                                                    <a href="{{url('doctor/' . $doctor->docid )}}">
                                                        {{$doctor->Doctor->fname}} {{$doctor->Doctor->sname}}
                                                    </a>
                                                </td>

                                                <td>{{$doctor->created_at->toDayDateTimeString()}}</td>
                                                <td>
                                                    @if(isset($doctor->dateEnded))
                                                        {{Carbon::createFromFormat("Y-m-d H:i:s",$doctor->dateEnded)->toDayDateTimeString()}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(isset($doctor->notes))
                                                        {{$doctor->notes}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="messages">
                                <div class="grid-body no-border">

                                    @if(isset($case->Feedback))

                                        <div class="row-fluid white-box">
                                            <div class="grid simple col-md-10 col-md-offset-1">

                                                <div class="grid-title no-border">
                                                </div>

                                                <div class="grid-body no-border">
                                                    <p>
                                                        Rating :
                                                        @for( $i = 0; $i < number_format($case->Feedback->feeling,0); $i++)
                                                            <i class="fa fa-star" style="color: gold;"></i>
                                                        @endfor
                                                        <br>

                                                        {{$case->Feedback->content}}
                                                    </p>

                                                </div>
                                            </div>
                                        </div>

                                    @endif

                                </div>
                                @endif

                                </div>
                            </div>


                            {{--<div role="tabpanel" class="tab-pane" id="messages">...</div>--}}

                        </div>
                    </div>
                </div>
            </div>
@endsection