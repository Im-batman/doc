@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title" style="margin-left: 60px;margin-bottom: 50px;margin-right: 60px;">
                <a href="{{url('patient/' . $dependant->patid)}}">
                    <i class="icon-custom-left"></i>
                </a>

                <div class="pull-right">


                </div>
            </div>
        </div>


        <div class="row-fluid">
            <div class="grid simple col-md-10 col-md-offset-1">
                <div class="grid-title no-border">
                </div>
                <div class="grid-body no-border">
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <img src="{{$dependant->image}}" class="img img-thumbnail" width="100%"/>
                            <label class="label label-success">Dependant Patient</label>
                        </div>
                        <div class="col-md-8">
                            <h3 style="margin: 0 0 10px 0;"><span class="semi-bold">{{$dependant->fname}} {{$dependant->sname}}</span></h3>
                        </div>
                        <div class="form-group col-md-4 col-sm-12">
                            <label class="form-label">Email</label>
                            <span class="help">{{$dependant->email}}</span>
                            <br>
                            <label class="form-label">Phone</label>
                            <span class="help">{{$dependant->phone}}</span>
                            <br>
                            <label class="form-label">Gender</label>
                            <span class="help">{{$dependant->gender}}</span>
                            <br>
                            <label class="form-label">Total Cases</label>
                            <span class="help">{{count($dependant->Cases)}}</span>

                        </div>

                        <div class="form-group col-md-4 col-sm-12">

                            <label class="form-label">DOB</label>
                            <span class="help">{{$dependant->dob}}</span>
                            <br>
                            <label class="form-label">Height</label>
                            <span class="help">{{$dependant->height}}cm</span>
                            <br>
                            <label class="form-label">Weight</label>
                            <span class="help">{{$dependant->weight}}kg</span>

                        </div>
                    </div>


                </div>
            </div>

        </div>

        <div class="row-fluid">
            <div class="grid simple col-md-10 col-md-offset-1">
                <div class="grid-title no-border">
                    <h4>Cases</h4>
                </div>
                <div class="grid-body no-border">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Case ID</th>
                            <th>Date Started</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($dependant->Cases as $case)
                            <tr>
                                <td>{{$case->casid}}</td>
                                <td>{{$case->created_at->toDayDateTimeString()}}</td>
                                <td>
                                    @if($case->status == 'Available')
                                        <label class="label label-primary">Available</label>
                                    @endif

                                    @if($case->status == 'Pending')
                                        <label class="label label-warning">Pending</label>
                                    @endif
                                    @if($case->status == 'Complete')
                                        <label class="label label-success">Complete</label>
                                    @endif
                                    @if($case->status == 'Cancelled')
                                        <label class="label label-danger">Cancelled</label>
                                    @endif

                                </td>
                                <td>
                                    <a class="label label-primary" href="{{url('case/' . $case->casid)}}">View</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>


    </div>
@endsection