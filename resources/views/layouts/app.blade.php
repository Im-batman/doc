<!DOCTYPE html>
<html>


<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Doctap| Home</title>

    <link href="img/favicon.144x144.html" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="img/favicon.114x114.html" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="img/favicon.72x72.html" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="img/favicon.57x57.html" rel="apple-touch-icon" type="image/png">
    <link href="img/favicon.html" rel="icon" type="image/png">
    <link href="img/favicon-2.html" rel="shortcut icon">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>

    <![endif]-->
    <link rel="stylesheet" href="{{url('css/separate/vendor/slick.min.css')}}">
    <link rel="stylesheet" href="{{url('css/separate/pages/profile.min.css')}}">

    <link rel="stylesheet" href="{{url('css/lib/lobipanel/lobipanel.min.css')}}">
    <link rel="stylesheet" href="{{url('css/separate/vendor/lobipanel.min.css')}}">
    <link rel="stylesheet" href="{{url('css/lib/jqueryui/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{url('css/separate/pages/widgets.min.css')}}">
    <link rel="stylesheet" href="{{url('css/lib/font-awesome/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{url('css/lib/bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('css/main.css')}}">
    <link rel="stylesheet" href="{{url('css/paginate.css')}}">
    <link rel="stylesheet" href="{{url('css/jquery-ui.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/selectize/selectize.css')}}" />


    <script src="{{url('js/lib/jquery/jquery.min.js')}}"></script>

    <script type="text/javascript" src="{{url('js/jquery-ui.min.js')}}"></script>

    <script type="text/javascript" src="{{url('js/selectize.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('select.searchable').selectize();
        });
    </script>



</head>
<body class="with-side-menu control-panel control-panel-compact">

<header class="site-header">
    <div class="container-fluid">



        <button id="show-hide-sidebar-toggle" style="float: left;" class="show-hide-sidebar">
            <span>toggle menu</span>
        </button>

        <button class="hamburger hamburger--htla">
            <span>toggle menu</span>
        </button>

        <a style="color:white; margin-left: 20px" href="{{url('/')}}" class="site-logo">
            {{--<img  class="hidden-md-down" src="{{url('img/logo.png')}}" alt="">--}}
            Doctap
        </a>

        <div class="site-header-content">
            <div class="site-header-content-in">
                <div class="site-header-shown">
                    <div class="dropdown dropdown-notification notif">
                        <a href="#"
                           class="header-alarm dropdown-toggle active"
                           id="dd-notification"
                           data-toggle="dropdown"
                           aria-haspopup="true"
                           aria-expanded="false">
                            <i class="font-icon-alarm"></i>
                        </a>

                        {{-- notifications --}}
                        {{--<div class="dropdown-menu dropdown-menu-right dropdown-menu-notif" aria-labelledby="dd-notification">--}}
                            {{--<div class="dropdown-menu-notif-header">--}}
                                {{--Notifications--}}
                                {{--<span class="label label-pill label-danger">4</span>--}}
                            {{--</div>--}}
                            {{--<div class="dropdown-menu-notif-list">--}}
                                {{--<div class="dropdown-menu-notif-item">--}}
                                    {{--<div class="photo">--}}
                                        {{--<img src="img/photo-64-1.jpg" alt="">--}}
                                    {{--</div>--}}
                                    {{--<div class="dot"></div>--}}
                                    {{--<a href="#">Morgan</a> was bothering about something--}}
                                    {{--<div class="color-blue-grey-lighter">7 hours ago</div>--}}
                                {{--</div>--}}
                                {{--<div class="dropdown-menu-notif-item">--}}
                                    {{--<div class="photo">--}}
                                        {{--<img src="img/photo-64-2.jpg" alt="">--}}
                                    {{--</div>--}}
                                    {{--<div class="dot"></div>--}}
                                    {{--<a href="#">Lioneli</a> had commented on this <a href="#">Super Important Thing</a>--}}
                                    {{--<div class="color-blue-grey-lighter">7 hours ago</div>--}}
                                {{--</div>--}}
                                {{--<div class="dropdown-menu-notif-item">--}}
                                    {{--<div class="photo">--}}
                                        {{--<img src="img/photo-64-3.jpg" alt="">--}}
                                    {{--</div>--}}
                                    {{--<div class="dot"></div>--}}
                                    {{--<a href="#">Xavier</a> had commented on the <a href="#">Movie title</a>--}}
                                    {{--<div class="color-blue-grey-lighter">7 hours ago</div>--}}
                                {{--</div>--}}
                                {{--<div class="dropdown-menu-notif-item">--}}
                                    {{--<div class="photo">--}}
                                        {{--<img src="img/photo-64-4.jpg" alt="">--}}
                                    {{--</div>--}}
                                    {{--<a href="#">Lionely</a> wants to go to <a href="#">Cinema</a> with you to see <a href="#">This Movie</a>--}}
                                    {{--<div class="color-blue-grey-lighter">7 hours ago</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="dropdown-menu-notif-more">--}}
                                {{--<a href="#">See more</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>





                    @if(Auth::guest())

                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @else
                    <div class="dropdown user-menu">
                        <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{Auth::user()->image}}" alt="">
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                            <a class="dropdown-item" href="{{url('update-staff/' . Auth::user()->uid)}}"><span class="font-icon glyphicon glyphicon-user"></span>Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item"  href="{{ route('logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();"><span class="font-icon glyphicon glyphicon-log-out"></span>Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>

                        </div>
                    </div>
                    @endif

                    <button type="button" class="burger-right">
                        <i class="font-icon-menu-addl"></i>
                    </button>
                </div><!--.site-header-shown-->

                <div class="mobile-menu-right-overlay"></div>
                <div class="site-header-collapsed">
                    <div class="site-header-collapsed-in">
                        <div class="dropdown dropdown-typical">
                            <div class="dropdown-menu" aria-labelledby="dd-header-sales">
                                <a class="dropdown-item" href="#"><span class="font-icon font-icon-home"></span>Skillset</a>
                                <a class="dropdown-item" href="#"><span class="font-icon font-icon-cart"></span>Real Gmat Test</a>
                                <a class="dropdown-item" href="#"><span class="font-icon font-icon-speed"></span>Prep Official App</a>
                                <a class="dropdown-item" href="#"><span class="font-icon font-icon-users"></span>CATprer Test</a>
                                <a class="dropdown-item" href="#"><span class="font-icon font-icon-comments"></span>Third Party Test</a>
                            </div>
                        </div>
                        <div class="dropdown dropdown-typical">

                            {{--@if(Session::get('settings')->topMenu == 1)--}}

                            {{--<div class="dropdown dropdown-typical">--}}
                                {{--<a class="dropdown-toggle" style="color:white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                    {{--Request Management--}}
                                {{--</a>--}}

                                {{--<div class="dropdown-menu" >--}}
                                    {{--<a class="dropdown-item"  href=" {{url('requests')}} ">View Requests</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}



                            {{--<div class="dropdown dropdown-typical">--}}
                                {{--<a class="dropdown-toggle" style="color:white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                    {{--Tailor Management--}}

                                {{--</a>--}}

                                {{--<div class="dropdown-menu" >--}}
                                    {{--<a class="dropdown-item"  href=" {{url('view-tailors')}} ">View Tailors</a>--}}

                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="dropdown dropdown-typical">--}}
                                {{--<a class="dropdown-toggle" style="color:white"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                    {{--Staff Management--}}
                                {{--</a>--}}

                                {{--<div class="dropdown-menu" >--}}
                                    {{--<a class="dropdown-item"  href=" {{url('register')}} ">Add Staff</a>--}}
                                    {{--<a class="dropdown-item"  href=" {{url('manage-staff')}} ">Manage Staff</a>--}}

                                {{--</div>--}}
                            {{--</div>--}}


                            {{--<div class="dropdown dropdown-typical">--}}
                                {{--<a class="dropdown-toggle" style="color:white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}

                                    {{--Skillset Management--}}
                                {{--</a>--}}

                                {{--<div class="dropdown-menu" aria-labelledby="dd-header-social">--}}
                                    {{--<a class="dropdown-item"  href=" {{url('add-skillset')}} ">Add Skillset</a>--}}
                                    {{--<a class="dropdown-item"  href=" {{url('manage-skillsets')}} ">Manage Skillsets</a>--}}

                                {{--</div>--}}
                            {{--</div>--}}


                            {{--@endif--}}


                        </div><!--.site-header-collapsed-in-->
                    </div><!--.site-header-collapsed-->
                </div><!--site-header-content-in-->
            </div><!--.site-header-content-->
        </div><!--.container-fluid-->
    </div>
</header><!--.site-header-->

<div class="mobile-menu-left-overlay"></div>


{{-- Side Menu --}}

<nav class="side-menu">
    <ul class="side-menu-list">
        <li class="grey">
            <a href="{{url('/')}}">
	            <span>
	                <i class="font-icon font-icon-dashboard"></i>
	                <span class="lbl">Dashboard</span>
	            </span>
            </a>
        </li>

        <li class="gold with-sub">
	            <span>
	                <i class="font-icon font-icon-edit"></i>
	                <span class="lbl">Cases</span>
	            </span>
            <ul>
                <li><a href="{{url('fashion-houses')}}"><span class="lbl">All</span></a></li>
                <li><a href="{{url('fashion-houses?criteria=employed')}}"><span class="lbl">Completed</span></a></li>
                <li><a href="{{url('fashion-houses?criteria=employed')}}"><span class="lbl">Pending</span></a></li>
            </ul>
        </li>


        <li class="gold">
            <a href="{{url('/requests')}}">
	            <span>
	                <i class="font-icon font-icon-edit"></i>
	                <span class="lbl">Doctors</span>
	            </span>
            </a>

        </li>
        <li class="gold">
            <a href="{{url('/view-tailors')}}">
	            <span>
	                <i class="font-icon font-icon-edit"></i>
	                <span class="lbl">Patients</span>
	            </span>
            </a>
        </li>

        <li class="gold with-sub">
	            <span>
	                <i class="font-icon font-icon-edit"></i>
	                <span class="lbl">Hospitals</span>
	            </span>
            <ul>
                <li><a href="{{url('fashion-houses')}}"><span class="lbl">Add</span></a></li>
                <li><a href="{{url('fashion-houses?criteria=employed')}}"><span class="lbl">Manage</span></a></li>
            </ul>
        </li>


        <li class="gold with-sub">
	            <span>
	                <i class="font-icon font-icon-edit"></i>
	                <span class="lbl">Labs</span>
	            </span>
            <ul>
                <li><a href="{{url('add-skillset')}}"><span class="lbl">Add</span></a></li>
                <li><a href="{{url('manage-skillsets')}}"><span class="lbl">Manage</span></a></li>
            </ul>
        </li>

        <li class="gold with-sub">
	            <span>
	                <i class="font-icon font-icon-edit"></i>
	                <span class="lbl">Pharmacies</span>
	            </span>
            <ul>
                <li><a href="{{url('tailor-appointments')}}"><span class="lbl">Add</span></a></li>
                <li><a href="{{url('fashion-house-appointments')}}"><span class="lbl">Manage</span></a></li>
            </ul>
        </li>

        {{--@if(Auth::user()->role == 'Admin')--}}

        {{--<li class="gold with-sub">--}}
            {{--<span>--}}
                {{--<i class="font-icon font-icon-edit"></i>--}}
                {{--<span class="lbl">Staff</span>--}}
            {{--</span>--}}
            {{--<ul>--}}
                {{--<li><a href="{{url('register')}}"><span class="lbl">Add</span></a></li>--}}
                {{--<li><a href="{{url('manage-staff')}}"><span class="lbl">Manage</span></a></li>--}}
            {{--</ul>--}}
        {{--</li>--}}


        {{--<li class="gold with-sub">--}}
	            {{--<span>--}}
	                {{--<i class="font-icon font-icon-edit"></i>--}}
	                {{--<span class="lbl">Admin</span>--}}
	            {{--</span>--}}
            {{--<ul>--}}
                {{--<li><a href="{{url('reports')}}"><span class="lbl">Reports</span></a></li>--}}
                {{--<li><a href="{{url('mobile-settings')}}"><span class="lbl">Un-delete</span></a></li>--}}
                {{--<li><a href="{{url('mobile-settings')}}"><span class="lbl">Mobile Settings</span></a></li>--}}
                {{--<li><a href="{{url('web-settings')}}"><span class="lbl">Web Settings</span></a></li>--}}
            {{--</ul>--}}
        {{--</li>--}}

        {{--@endif--}}

    </ul>


</nav><!--.side-menu-->

<div class="page-content">
    <div class="container-fluid">


        @yield('content')

    </div><!--.page-content-->




    <script src="{{url('js/lib/tether/tether.min.js')}}"></script>
    <script src="{{url('js/lib/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{url('js/footable.min.js')}}"></script>
    <script src="{{url('js/plugins.js')}}"></script>

    <script type="text/javascript" src="{{url('js/lib/lobipanel/lobipanel.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/lib/match-height/jquery.matchHeight.min.js')}}"></script>

    <script>
        $(document).ready(function() {


            $('.table').footable();

            $('.panel').lobiPanel({
                sortable: true
            });
            $('.panel').on('dragged.lobiPanel', function(ev, lobiPanel){
                $('.dahsboard-column').matchHeight();
            });


            $(window).resize(function(){
                drawChart();
                setTimeout(function(){
                }, 1000);
            });
        });
    </script>



    <script src="{{url('js/app.js')}}"></script>
</div>
</body>

</html>


