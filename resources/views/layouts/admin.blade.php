<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Doctap Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="Gurudeveloper Inc." name="author" />
    <!-- BEGIN PLUGIN CSS -->
    {{--<link href="{{url('assets/plugins/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />--}}
    <link href="{{url('assets/plugins/jquery-metrojs/MetroJs.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{url('assets/plugins/shape-hover/css/demo.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('assets/plugins/shape-hover/css/component.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('assets/plugins/owl-carousel/owl.carousel.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('assets/plugins/owl-carousel/owl.theme.css')}}" />
    <link href="{{url('assets/plugins/pace/pace-theme-flash.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{url('assets/plugins/jquery-slider/css/jquery.sidr.light.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{url('assets/plugins/jquery-ricksaw-chart/css/rickshaw.css')}}" type="text/css" media="screen">
{{--    <link rel="stylesheet" href="{{url('assets/plugins/Mapplic/mapplic/mapplic.css')}}" type="text/css" media="screen">--}}
    <!-- END PLUGIN CSS -->
    <!-- BEGIN PLUGIN CSS -->
    <link href="{{url('assets/plugins/pace/pace-theme-flash.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{url('assets/plugins/bootstrapv3/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/plugins/bootstrapv3/css/bootstrap-theme.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('css/fonts/font.css')}}" rel="stylesheet">
    <link href="{{url('assets/plugins/animate.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/plugins/jquery-scrollbar/jquery.scrollbar.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PLUGIN CSS -->
    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="{{url('webarch/css/webarch.css')}}" rel="stylesheet" type="text/css" />
    <!-- END CORE CSS FRAMEWORK -->

    <script src="{{url('assets/plugins/jquery/jquery-1.11.3.min.js')}}" type="text/javascript"></script>

    {{--<script src="{{url('js/jquery.min.js')}}"></script>--}}

    <link rel="stylesheet" href="{{url('css/jquery-ui.min.css')}}">

    <script type="text/javascript" src="{{url('js/jquery-ui.min.js')}}"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="">

<!-- css styles from meeting -->
<style>
    div.page-content div.pull-right a {
        margin-right:10px !important;
    }

    div.page-content div.pull-right a:last-child {
        margin-right:0 !important;
    }

    .btn-primary, .label-primary {
        background-color: #009EC5 !important;
    }

    .btn-danger, .label-danger {
        background-color: #C1272D !important;
    }

    .btn-warning, .label-warning {
        background-color: #F5A623 !important;
    }
    .btn-secondary, .label-secondary {
        background-color: #008399 !important;
    }

    .btn-success, .label-success {
        background-color: #098A54 !important;
    }

    .page-title{
        margin-left: 60px;margin-bottom: 50px;margin-right: 60px;
    }

    table thead > tr{
        border-bottom: 1px solid #AFB6BE !important;
    }

    .grid.simple .tab-pane .grid-body{
        padding: 0 !important;
    }

    .grid.simple .grid-body{
        border:none !important;
    }

    .grid.simple .grid-body .full{
        padding: 0 !important;
    }

    .page-title.full{
        margin-left:0 !important;
        margin-right:0 !important;
    }
</style>

<style>
    .filtericons{
        float: right;
    }

    .btn {
        height: 37px;
        padding:7px 10px;
        border: 1px solid #E5E9EC;
        background: none;
        border-radius: 5px;
    }

    .grid-title div.page{
        margin: 0 10px;
    }

    .filtericons img{
        width:24px;
        padding:5px;
    }

    .input-group-btn{
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;

    }

    .page-sidebar-wrapper li a svg{
        margin:0 5px 0 0 !important;
    }

    .header-seperation{
        background-color: #009EC5 !important;
    }
</style>

<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse ">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="navbar-inner">
        <div align="center" class="header-seperation">
            <ul class="nav pull-left notifcation-center visible-xs visible-sm">
                <li class="dropdown">
                    <a href="#main-menu" data-webarch="toggle-left-side">
                        <i class="material-icons">menu</i>
                    </a>
                </li>
            </ul>
            <!-- BEGIN LOGO -->
            <a href="{{url('/')}}">
                <img src="{{url('assets/img/logo.png')}}" class="logo" alt="" width="126" style="margin-top: 14px;" />
            </a>
            <!-- END LOGO -->
            <ul class="nav pull-right notifcation-center">
                {{--<li class="dropdown hidden-xs hidden-sm">--}}
                    {{--<a href="{{url('/')}}" class="dropdown-toggle active" data-toggle="">--}}
                        {{--<i class="material-icons">home</i>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="dropdown visible-xs visible-sm">--}}
                    {{--<a href="#" data-webarch="toggle-right-side">--}}
                        {{--<i class="material-icons">chat</i>--}}
                    {{--</a>--}}
                {{--</li>--}}
            </ul>
        </div>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <div class="header-quick-nav">
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="pull-left">
                <ul class="nav quick-section">
                    <li class="quicklinks">
                        <a href="#" class="" id="layout-condensed-toggle">
                            <i class="material-icons">menu</i>
                        </a>
                    </li>
                </ul>
                <ul class="nav quick-section">
                    {{--<li class="quicklinks"> <span class="h-seperate"></span></li>--}}

                    {{--<li class="quicklinks">--}}
                        {{--<a href="#" class="" id="my-task-list" data-placement="bottom" data-content='' data-toggle="dropdown" data-original-title="Notifications">--}}
                            {{--<i class="material-icons">notifications_none</i>--}}
                            {{--<span class="badge badge-important bubble-only"></span>--}}
                        {{--</a>--}}
                    {{--</li>--}}


                    <li class="m-r-10 input-prepend inside search-form no-boarder">

                        <span class="add-on"> <i class="material-icons">search</i></span>
                        <input id="caseidSearch" type="text" class="no-boarder " placeholder="Go to case" style="width:250px;">

                        <script>
                            $(document).ready(function () {
                                var caseid = $('#caseidSearch');
                                caseid.keypress(function(e) {
                                    if(e.which === 13) {
                                        window.location = "{{url('case')}}/" + caseid.val() + "?role=" + "{{Auth::user()->role}}";
                                    }
                                });


                            })
                        </script>
                    </li>


                </ul>
            </div>


            <!-- notifications for the users  -->
            @include("appnotifications")

            <!-- END TOP NAVIGATION MENU -->

            <!-- BEGIN CHAT TOGGLER -->
            <div class="pull-right">
                {{--<div class="chat-toggler sm">--}}
                    {{--<div class="profile-pic">--}}
                        {{--<img src="{{Auth::user()->image}}" alt="" width="35" height="35" />--}}
                        {{--<div class="availability-bubble online"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <ul class="nav quick-section ">

                    <li class="quicklinks">
                        <a data-toggle="dropdown" class="dropdown-toggle  pull-right " href="#" id="user-options">

                            <div class="profile-pic">
                                <span style="color:#6f7b8a;font-size: 19px;">
                                {{Auth::user()->fname}} {{Auth::user()->sname}}
                                </span>
                                <img style="margin-top: -7px;" src="{{Auth::user()->image}}" alt="" width="35" height="35" />
                                <div class="availability-bubble online"></div>
                            </div>

                        </a>
                        <ul class="dropdown-menu  pull-right" role="menu" aria-labelledby="user-options">
                            <li>
                                <a href="{{url('profile')}}"> My Account</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{url('logout')}}"><i class="material-icons">power_settings_new</i>&nbsp;&nbsp;Log Out</a>
                            </li>
                        </ul>
                    </li>
                    {{--<li class="quicklinks"> <span class="h-seperate"></span></li>--}}
                    {{--<li class="quicklinks">--}}
                        {{--<a href="#" class="chat-menu-toggle" data-webarch="toggle-right-side"><i class="material-icons">chat</i><span class="badge badge-important hide">1</span>--}}
                        {{--</a>--}}
                        {{--<div class="simple-chat-popup chat-menu-toggle hide">--}}
                            {{--<div class="simple-chat-popup-arrow"></div>--}}
                            {{--<div class="simple-chat-popup-inner">--}}
                                {{--<div style="width:100px">--}}
                                    {{--<div class="semi-bold">David Nester</div>--}}
                                    {{--<div class="message">Hey you there </div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                </ul>
            </div>
            <!-- END CHAT TOGGLER -->
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
    <!-- BEGIN SIDEBAR -->
    @include('sidebars.left')
    <a href="#" class="scrollup">Scroll</a>
    <div class="footer-widget">
        <div class="progress transparent progress-small no-radius no-margin">
            <div class="progress-bar progress-bar-success animate-progress-bar" data-percentage="79%" style="width: 79%;"></div>
        </div>
        <div class="pull-right">
            {{--<div class="details-status"> <span class="animate-number" data-value="86" data-animation-duration="560">86</span>% </div>--}}
            <a href="{{url('logout')}}"><i class="material-icons">power_settings_new</i></a></div>
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE CONTAINER-->



@yield('content')



{{--@include('chat')--}}
</div>


<!-- END CONTAINER -->
<!-- BEGIN CORE JS FRAMEWORK-->
<script src="{{url('assets/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
<!-- BEGIN JS DEPENDECENCIES-->
<script src="{{url('assets/plugins/bootstrapv3/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/plugins/jquery-block-ui/jqueryblockui.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/plugins/jquery-unveil/jquery.unveil.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js')}}" type="text/javascript"></script>
<script src="{{url('assets/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
<!-- END CORE JS DEPENDECENCIES-->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{url('webarch/js/webarch.js')}}" type="text/javascript"></script>
<script src="{{url('assets/js/chat.js')}}" type="text/javascript"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
{{--<script src="{{url('assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js')}}" type="text/javascript"></script>--}}
<script src="{{url('assets/plugins/jquery-ricksaw-chart/js/raphael-min.js')}}"></script>
<script src="{{url('assets/plugins/jquery-ricksaw-chart/js/d3.v2.js')}}"></script>
<script src="{{url('assets/plugins/jquery-ricksaw-chart/js/rickshaw.min.js')}}"></script>
<script src="{{url('assets/plugins/jquery-ricksaw-chart/js/rickshaw.min.js')}}"></script>
<script src="{{url('assets/plugins/jquery-ricksaw-chart/js/rickshaw.min.js')}}"></script>
<script src="{{url('assets/plugins/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script>
{{--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>--}}
{{--<script src="{{url('assets/plugins/jquery-gmap/gmaps.js')}}" type="text/javascript"></script>--}}
<script src="{{url('assets/plugins/Mapplic/js/jquery.easing.js')}}" type="text/javascript"></script>
<script src="{{url('assets/plugins/Mapplic/js/jquery.mousewheel.js')}}" type="text/javascript"></script>
<script src="{{url('assets/plugins/Mapplic/js/hammer.js')}}" type="text/javascript"></script>
<script src="{{url('assets/plugins/Mapplic/mapplic/mapplic.js')}}" type="text/javascript"></script>
<script src="{{url('assets/plugins/jquery-flot/jquery.flot.js')}}" type="text/javascript"></script>
<script src="{{url('assets/plugins/jquery-metrojs/MetroJs.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{url('assets/js/dashboard_v2.js')}}" type="text/javascript"></script>
</body>
</html>