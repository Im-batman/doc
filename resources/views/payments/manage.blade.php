<?php use Carbon\Carbon; use Illuminate\Support\Facades\Input; ?>
@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title">
                <div class="pull-right">

                    <a href="{{url('pay-doctors')}}" class="btn btn-success" type="button" >
                        Pay Doctors
                    </a>

                    <a href="{{url('export-payments')}}" class="btn btn-success" type="button" >
                        Export Payments
                    </a>

                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Filter
                    </button>

                </div>

            </div>



            <div class="row-fluid">

                <div class="grid simple col-md-10 col-md-offset-1" style="margin-bottom: 5px !important;">
                    <div class="collapse" id="collapseExample">

                        <div class="grid-body no-border" align="center" style="padding: 10px;">

                            <form class="form-inline" method="post" action="{{url('manage-payments')}}">
                                {{csrf_field()}}

                                <div class="form-group">
                                    <span style="float: left;line-height: 30px;margin-right: 10px;">Requested between</span>
                                    <input id="fromDate" class="dates form-control" value="{{Input::get('fromDate')}}" type="text" name="fromDate" required>
                                    <span>And</span>
                                    <input id="toDate" class="dates form-control"  value="{{Input::get('toDate')}}" type="text" name="toDate" required>
                                    <span>And Status</span>
                                    <select name="by" class="form-control">
                                        <option value=""> All </option>
                                        <option value="Available"
                                                {{--@if( Input::get('by') == "Paid")--}}
                                                {{--selected--}}
                                                {{--@endif--}}
                                        >Paid</option>

                                        <option value="Scheduled"
                                                {{--@if( Input::get('by') == "Unpaid")--}}
                                                {{--selected--}}
                                                {{--@endif--}}
                                        >Unpaid</option>
                                    </select>
                                    <button class="btn btn-primary">Search</button>

                                </div>

                            </form>

                        </div>

                    </div>
                </div>

                <script>
                    $(document).ready( function() {

                        $('.dates').datetimepicker();

                    } );
                </script>


                <div class="span12">
                    <div class="grid simple ">
                        <div class="grid-title">
                            <h4 style="margin: 10px 0 0 0;">Payments <span class="semi-bold">Due</span></h4>
                            <div class="tools">

                                <div class="page btn-group" role="group" aria-label="Search form">
                                    <a href="{{url('manage-payments?pages=10')}}" type="button"
                                       @if(session()->get('pages') == 10)
                                       style="color:white"
                                       class="btn btn-primary"
                                       @else
                                       class="btn"
                                            @endif>10</a>

                                    <a href="{{url('manage-payments?pages=25')}}" type="button"
                                       @if(session()->get('pages') == 25)
                                       style="color:white"
                                       class="btn btn-primary"
                                       @else
                                       class="btn"
                                            @endif>25</a>

                                    <a href="{{url('manage-payments?pages=50')}}" type="button"
                                       @if(session()->get('pages') == 50)
                                       style="color:white"
                                       class="btn btn-primary"
                                       @else
                                       class="btn"
                                            @endif>50</a>
                                </div>

                                <div class="filtericons" align="right" >

                                    <div class="btn-group btn-group-xs" role="group" aria-label="...">

                                        <script>
                                            $(document).ready(function () {
                                                $('.dropdown-menu li').on('click',function(e){
                                                    e.preventDefault();
                                                    var filterlabel = $("#filterlabel");
                                                    var value =  $(this).data("key");
                                                    var selected = $(this).text();
                                                    filterlabel.html(selected + '<span class="caret"></span>');
                                                    $('#by').val(value);
                                                    console.log(value);


                                                });

                                            });
                                        </script>

                                        <form class="form-inline" action="{{url('manage-payments')}}" method="get">
                                            <div class="input-group">

                                                <div class="input-group-btn">
                                                    <button type="button" id="filterlabel" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Select <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li data-key="fname"><a href="#">Firstname</a></li>
                                                        <li><a href="#">Surname</a></li>
                                                        <li><a href="#">Email</a></li>
                                                        <li><a href="#">Phone</a></li>
                                                    </ul>
                                                    <input name="by" id="by" type="hidden">
                                                    <input name="pages" value="{{$input::get('pages')}}" type="hidden">
                                                </div><!-- /btn-group -->

                                                <input style="border-top-right-radius: 5px;border-bottom-right-radius: 5px;" class="form-control" value="" placeholder="Search" name="term" type="text">
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-body ">
                            <table class="table table-striped">
                                @if(count($payments) > 0)
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Date Ended</th>
                                        <th title="Case ID">CID</th>
                                        <th>Doctor</th>
                                        <th style="width: 80px;">C Fee</th>
                                        <th>Doctors Fee</th>
                                        <th>Access Fee</th>
                                        <th>Tx ID</th>
                                        <th>Due Date</th>
                                        <th>Service Status</th>
                                        <th>Payment Status</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($payments as $payment)

                                        <tr class="odd gradeX">
                                            <td class="center"> {{$payment['payid']}} </td>
                                            <td class="center">
                                                {{Carbon::createFromFormat("Y-m-d H:i:s",$payment['case']['ended_at'])->toDayDateTimeString()}}

                                            </td>
                                            <td class="center"> {{$payment['casid']}} </td>
                                            <td class="center"> {{$payment['case']['doctor']['fname']}} {{$payment['case']['doctor']['sname']}} </td>
                                            <td class="center"> {{$payment['amount']}} </td>
                                            <td class="center">{{$payment['doctorsFee']}}</td>
                                            <td class="center">{{$payment['accessFee']}}</td>
                                            <td class="center">{{$payment['others']}}</td>
                                            <td class="center">{{$payment['maturityDate']}}</td>
                                            <td class="center">{{$payment['case']['status']}}</td>
                                            <td class="center">{{$payment['status']}}</td>
                                            <td>
                                                @if($payment['status'] == "Unpaid")
                                                    <label class="label label-danger">Unpaid</label>
                                                @else
                                                    <label class="label label-success">Paid</label>
                                                @endif
                                            </td>
                                        </tr>

                                    @endforeach
                                    @else
                                        <h3 style="text-align: center"> There are no payments available </h3>
                                    @endif

                                    </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>




@endsection















