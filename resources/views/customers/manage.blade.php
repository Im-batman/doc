<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

<div class="container">
    <div class="panel">
        <div class="panel-heading">Registered Users</div>
        <div class="panel-heading">
            <table>
                <tr>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>isConfirmed</th>
                    <th>Date Registered</th>
                    <th></th>
                </tr>

                @foreach($customers as $customer)
                    <tr>
                        <td>{{$customer->name}}</td>
                        <td>{{$customer->phone}}</td>
                        <td>{{$customer->email}}</td>
                        <td>
                            @if($cutomer->isConfirmed == 1)
                               <label class="label label-success">Confirmed</label>
                                @else
                                <label class="label label-danger">Un-confirmed</label>
                            @endif
                        </td>
                        <td>
                            {{Carbon::createFromFormat("Y-m-d H:i:s",$customer->created_at)->toDayDateTimeString()}}
                        </td>
                        <td>
                            <a href="{{url('customer/' . $customer->cid)}}" class="btn btn-primary">View</a>
                        </td>
                    </tr>
                @endforeach

            </table>
            {{$customers->links()}}
        </div>
    </div>
</div>

@endsection