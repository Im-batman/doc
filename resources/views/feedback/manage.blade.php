@extends('layouts.admin')

@section('content')


    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title">
            </div>

            <div class="row-fluid">

                <div class="span12">
                    <div class="grid simple ">
                        <div class="grid-title">
                            <h4 style="margin: 10px 0 0 0;">Submitted <span class="semi-bold">Feedback</span></h4>
                            <div class="tools">

                                <div class="page btn-group" role="group" aria-label="Search form">
                                    <a href="{{url('feedback?pages=10')}}" type="button"
                                       @if(session()->get('pages') == 10)
                                       style="color:white"
                                       class="btn btn-primary"
                                       @else
                                       class="btn"
                                            @endif>10</a>

                                    <a href="{{url('feedback?pages=25')}}" type="button"
                                       @if(session()->get('pages') == 25)
                                       style="color:white"
                                       class="btn btn-primary"
                                       @else
                                       class="btn"
                                            @endif>25</a>

                                    <a href="{{url('feedback?pages=50')}}" type="button"
                                       @if(session()->get('pages') == 50)
                                       style="color:white"
                                       class="btn btn-primary"
                                       @else
                                       class="btn"
                                            @endif>50</a>
                                </div>

                                <div class="filtericons" align="right" >

                                    <div class="btn-group btn-group-xs" role="group" aria-label="...">

                                        <script>
                                            $(document).ready(function () {
                                                $('.dropdown-menu li').on('click',function(e){
                                                    e.preventDefault();
                                                    var filterlabel = $("#filterlabel");
                                                    var value =  $(this).data("key");
                                                    var selected = $(this).text();
                                                    filterlabel.html(selected + '<span class="caret"></span>');
                                                    $('#by').val(value);
                                                    console.log(value);


                                                });

                                            });
                                        </script>

                                        <form class="form-inline" action="{{url('manage-cases')}}" method="get">
                                            <div class="input-group">

                                                <div class="input-group-btn">
                                                    <button type="button" id="filterlabel" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Select <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li data-key="fname"><a href="#">Firstname</a></li>
                                                        <li><a href="#">Surname</a></li>
                                                        <li><a href="#">Email</a></li>
                                                        <li><a href="#">Phone</a></li>
                                                    </ul>
                                                    <input name="by" id="by" type="hidden">
                                                    <input name="pages" value="{{$input::get('pages')}}" type="hidden">
                                                </div><!-- /btn-group -->

                                                <input style="border-top-right-radius: 5px;border-bottom-right-radius: 5px;" class="form-control" value="" placeholder="Search" name="term" type="text">
                                            </div>

                                        </form>

                                    </div>
                                </div>

                        </div>
                        <div class="grid-body ">
                            <table class="table table-striped">
                                @if(count($feedbacks) > 0)
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Patient</th>
                                        <th>Case ID</th>
                                        <th>Status</th>
                                        <th>Rating</th>
                                        <th>Created At</th>
                                        <th></th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($feedbacks as $feedback)

                                        <tr class="odd gradeX">
                                            <td class="center"> {{$feedback->fdid}} </td>
                                            <td class="center">
                                                <a href="{{url('patient/' . $feedback->Patient->patid )}}">
                                                    {{$feedback->Patient->fname}} {{$feedback->Patient->sname}}
                                                </a>
                                            </td>
                                            <td class="center">
                                                <a href="{{url('case/' . $feedback->Case->casid)}}">
                                                    {{$feedback->Case->casid}}
                                                </a>
                                            </td>
                                            <td class="center">
                                                @if($feedback->isViewed == 1)
                                                    <label class="label label-success">Viewed</label>
                                                    @else
                                                    <label class="label label-primary">Not Viewed</label>
                                                @endif
                                            </td>
                                            <td>

                                                @for( $i = 0; $i < number_format($feedback->feeling,0); $i++)
                                                    <i class="fa fa-star" style="color: gold;"></i>
                                                @endfor

                                            </td>
                                            <td class="center"> {{$feedback->created_at->toDayDateTimeString()}} </td>
                                            <td>
                                                <a href="{{url('feedback/' . $feedback->fdid)}}" class="label label-primary">View</a>
                                            </td>
                                        </tr>

                                    @endforeach
                                    @else
                                        <h3 style="text-align: center"> There's no feedback submitted yet</h3>
                                    @endif


                                    </tbody>
                            </table>
                            {{$feedbacks->links()}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection















