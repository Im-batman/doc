@extends('layouts.auth')

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif


    <form class="sign-box form-horizontal" method="POST" action="{{ route('password.request') }}">
        {{ csrf_field() }}

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="sign-avatar">
            <img src="{{url('img/avatar-sign.png')}}" alt="">
        </div>

        <header class="sign-title">Tailors Hub - Reset Password</header>


        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

            <input id="email" type="email" class="form-control" placeholder="E-Mail Address" name="email" value="{{ old('email') }}" required autofocus>

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

            <input id="password" type="password" class="form-control" placeholder="Password" name="password" required>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif

        </div>


        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

            <input id="password" type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" required>
            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif

        </div>




        <div class="form-group">
            <div class="checkbox float-left">

                <input id="signed-in" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                <label for="signed-in">Keep me signed in</label>
            </div>
        </div>
        <button type="Submit" class="btn btn-primary">Reset Password</button>

    </form>


@endsection
