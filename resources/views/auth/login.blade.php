@extends('layouts.auth')

@section('content')


    <form class="sign-box form-horizontal" method="POST" action="{{ route('login') }}">
        @include('notification')
        {{ csrf_field() }}

        <div align="center" style="padding: 10px;margin:10px 10px 20px 10px;">
            <img style="width:60%;" src="{{url('assets/img/logodark.png')}}" alt="">
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

            <input id="email" type="email" class="form-control" placeholder="E-Mail Address" name="email" value="{{ old('email') }}" required autofocus>

            @if ($errors->has('email'))
                <p align="center" class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </p>
            @endif

        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

            <input id="password" type="password" class="form-control" placeholder="Password" name="password" required>
            @if ($errors->has('password'))
                <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
            @endif

        </div>

        <div class="form-group">
            <div class="checkbox float-left">

                <input id="signed-in" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                <label for="signed-in" style="color: #38899E;">Remember me</label>
            </div>
        </div>
        <button style="font-size: small; width: 100%;" type="submit" class="btn btn-rounded btn-primary">LOGIN</button>

        <div align="center">
            <a style="font-size:small;color: #38899E;" href="{{ route('password.request') }}"><b>Forgot Password?</b></a>
        </div>

    </form>



@endsection








{{--<!DOCTYPE html>--}}
{{--<html>--}}
{{--<head>--}}
    {{--<meta http-equiv="content-type" content="text/html;charset=UTF-8" />--}}
    {{--<meta charset="utf-8" />--}}
    {{--<title>Webarch - Responsive Admin Dashboard</title>--}}
    {{--<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />--}}
    {{--<meta content="" name="description" />--}}
    {{--<meta content="" name="author" />--}}
    {{--<!-- BEGIN PLUGIN CSS -->--}}
    {{--<link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />--}}
    {{--<link href="assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="assets/plugins/bootstrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">--}}
    {{--<link href="assets/plugins/animate.min.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" />--}}
    {{--<!-- END PLUGIN CSS -->--}}
    {{--<!-- BEGIN CORE CSS FRAMEWORK -->--}}
    {{--<link href="webarch/css/webarch.css" rel="stylesheet" type="text/css" />--}}
    {{--<!-- END CORE CSS FRAMEWORK -->--}}
{{--</head>--}}
{{--<!-- END HEAD -->--}}
{{--<!-- BEGIN BODY -->--}}
{{--<body class="error-body no-top lazy" data-original="assets/img/work.jpg" style="background-image: url('assets/img/work.jpg')">--}}
{{--<div class="container">--}}
    {{--<div class="row login-container animated fadeInUp">--}}
        {{--<div class="col-md-7 col-md-offset-2 tiles white no-padding">--}}
            {{--<div class="p-t-30 p-l-40 p-b-20 xs-p-t-10 xs-p-l-10 xs-p-b-10">--}}
                {{--<h2 class="normal">--}}
                    {{--Sign in to DocTap--}}
                {{--</h2>--}}
                {{--<p>--}}
                    {{--Use Facebook, Twitter or your email to sign in.--}}
                    {{--<br>--}}
                {{--</p>--}}
                {{--<p class="p-b-20">--}}
                    {{--Sign up Now! for DocTap accounts, it's free and always will be..--}}
                {{--</p>--}}
                {{--<div role="tablist">--}}
                    {{--<a href="#tab_login" class="btn btn-primary btn-cons" role="tab" data-toggle="tab">Login</a> or&nbsp;&nbsp;--}}
                    {{--<a href="#tab_register" class="btn btn-info btn-cons" role="tab" data-toggle="tab">Create an account</a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="tiles grey p-t-20 p-b-20 no-margin text-black tab-content">--}}
                {{--<div role="tabpanel" class="tab-pane active" id="tab_login">--}}
                    {{--<form class="animated fadeIn validate" id="" name="" method="POST" action="{{ route('login') }}">--}}

                        {{--{{csrf_field()}}--}}
                        {{--<div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">--}}
                            {{--<div class="col-md-6 col-sm-6">--}}
                                {{--<input class="form-control" id="login_username" name="email" placeholder="Email" type="email" required>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-6 col-sm-6">--}}
                                {{--<input class="form-control" id="login_pass" name="password" placeholder="Password" type="password" required>--}}
                            {{--</div>--}}


                        {{--</div>--}}



                        {{--<div class="row p-t-10 m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">--}}
                            {{--<div class="control-group col-md-10">--}}
                                {{--<div class="checkbox checkbox check-success">--}}
                                    {{--<a href="{{ route('password.request') }}">Trouble login in?</a>&nbsp;&nbsp;--}}
                                    {{--<input id="checkbox1" type="checkbox" value="1">--}}
                                    {{--<label for="checkbox1">Keep me reminded</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="row p-t-10 m-l-20 m-r-20 xs-m-l-10 xs-m-r-10 pull-right">--}}
                            {{--<button class="btn btn-primary btn-cons" >Login</button>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
                {{--<div role="tabpanel" class="tab-pane" id="tab_register">--}}


                    {{--<form class="animated fadeIn validate" id="" name="">--}}
                        {{--<div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">--}}
                            {{--<div class="col-md-6 col-sm-6">--}}
                                {{--<input class="form-control" id="fname" name="fname" placeholder="First Name" type="text" required>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-6 col-sm-6">--}}
                                {{--<input class="form-control" id="sname" name="sname" placeholder="Last Name" type="password" required>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">--}}
                            {{--<div class="col-md-6 col-sm-6">--}}
                                {{--<input class="form-control" id="reg_first_Name" name="email" placeholder=" Email Address" type="email" required>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-6 col-sm-6">--}}
                                {{--<input class="form-control" id="phone" name="phone" placeholder="Phone Number" type="text" required>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<textarea class="form-control" id="address" name="address" placeholder="Address" type="text" required></textarea>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">--}}
                            {{--<div class="col-md-12">--}}

                                {{--<select id="role" onchange="roleChanged()" name="role" class="form-control">--}}
                                    {{--<option> Role</option>--}}
                                    {{--<option> Hospital</option>--}}
                                    {{--<option> Lab</option>--}}
                                    {{--<option> Pharmacy</option>--}}
                                    {{--<option> Staff</option>--}}
                                    {{--<option> Admin</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<input id="image" type="file" class="form-control" name="image">--}}
                            {{--</div>--}}
                        {{--</div>--}}


                        {{--<div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">--}}
                            {{--<div class="col-md-6 col-sm-6">--}}
                                {{--<input id="password" type="password" class="form-control" placeholder="Password" name="password" minlength="6" required>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-6 col-sm-6">--}}
                                {{--<input id="password-confirm" type="password" minlength="6" placeholder="Comfirm password" class="form-control" name="password_confirmation" required>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<hr>--}}
                        {{--<h3> Institutional Details </h3>--}}

                        {{--<div id="place">--}}

                        {{--<div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<textarea class="form-control" id="placename" name="placename" placeholder="Name" type="text" required></textarea>--}}
                            {{--</div>--}}
                        {{--</div>--}}


                        {{--<div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<textarea class="form-control" id="placeaddress" name="placeaddress" placeholder="Address" type="text" required></textarea>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<select id="placecity" onchange="roleChanged()" name="placecity" class="form-control">--}}
                                    {{--<option> City</option>--}}
                                    {{--<option> Abuja</option>--}}
                                    {{--<option> Lagos</option>--}}
                                    {{--<option> Port-Harcourt</option>--}}
                                    {{--<option> Jos</option>--}}
                                    {{--<option> Admin</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}


                    {{--</form>--}}





                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<!-- END CONTAINER -->--}}
{{--<script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>--}}
{{--<!-- BEGIN JS DEPENDECENCIES-->--}}
{{--<script src="assets/plugins/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>--}}
{{--<script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>--}}
{{--<script src="assets/plugins/jquery-block-ui/jqueryblockui.min.js" type="text/javascript"></script>--}}
{{--<script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>--}}
{{--<script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>--}}
{{--<script src="assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>--}}
{{--<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>--}}
{{--<script src="assets/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>--}}
{{--<!-- END CORE JS DEPENDECENCIES-->--}}
{{--<!-- BEGIN CORE TEMPLATE JS -->--}}
{{--<script src="webarch/js/webarch.js" type="text/javascript"></script>--}}
{{--<script src="assets/js/chat.js" type="text/javascript"></script>--}}
{{--<!-- END CORE TEMPLATE JS -->--}}



{{--</body>--}}




{{--<script>--}}

    {{--function roleChanged(){--}}
        {{--var place = $('#place');--}}
        {{--var role = $('#role');--}}

        {{--if(role.val() == 'Admin' || role.val() == 'Staff'){--}}
            {{--place.addClass('hidden');--}}
        {{--}--}}

        {{--if(role.val() == 'Pharmacy'){--}}
            {{--place.removeClass('hidden');--}}
        {{--}--}}
        {{--if(role.val() == 'Lab'){--}}
            {{--place.removeClass('hidden');--}}
        {{--}--}}
        {{--if(role.val() == 'Hospital'){--}}
            {{--place.removeClass('hidden');--}}
        {{--}--}}

    {{--}--}}

    {{--$(document).ready(function () {--}}


    {{--});--}}
{{--</script>--}}


{{--</html>--}}






































