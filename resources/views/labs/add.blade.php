@extends('layouts.admin')

@section('content')


    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title">
            </div>

            <div class="row-fluid">
                <div align="center">

                    <form>
                        <select name="term" class="form-control inline" style="height: 40px !important;width:150px;">
                            <option value="name">Name</option>
                            <option value="city">City</option>
                            <option value="address">Address</option>
                            <option value="email">Email</option>
                            <option value="phone">Phone</option>
                        </select>
                        <input name="value" type="text" class="no-boarder " placeholder="Search" style="width:250px;">
                        <button class="btn btn-primary"> <i class="material-icons">search</i></button>

                    </form>
                </div>

                <div class="span12">
                    <div class="grid simple ">
                        <div class="grid-title">
                            <h4>Add <span class="semi-bold">Lab</span></h4>
                            <div class="tools">
                                <a href="javascript:;" class="expand"></a>
                                <a href="#grid-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                                <a href="javascript:;" class="remove"></a>
                            </div>
                        </div>
                        <div class="grid-body ">

                            <form method="post" action="{{url('add-lab')}}">
                                {{csrf_field()}}

                                <input type="hidden" name="role" value="Lab">
                                <div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" id="fname" name="fname" value="{{old('fname')}}" placeholder="First Name" type="text" required>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" id="sname" name="sname" value="{{old('sname')}}" placeholder="Last Name" type="text" required>
                                    </div>
                                </div>

                                <div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" id="reg_first_Name" name="useremail" value="{{old('useremail')}}" placeholder=" Email Address" type="email" required>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" id="phone" name="userphone" value="{{old('userphone')}}" placeholder="Phone Number" type="tel" required>
                                    </div>
                                </div>

                                <div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
                                    <div class="col-md-12">
                                        <textarea class="form-control" id="address" name="useraddress" placeholder="Address" type="text" required>
                                            {{trim(old('useraddress'))}}
                                        </textarea>
                                    </div>
                                </div>

                                <div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
                                    <div class="col-md-12">
                                        <input id="image" type="file" class="form-control" name="image">
                                    </div>
                                </div>


                                <div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
                                    <div class="col-md-6 col-sm-6">
                                        <input onkeyup="verifyPassword()" id="password" type="password" class="form-control" placeholder="Password" name="password" minlength="6" required>
                                        <span id="success" class="help hidden" style="color:green">Passwords match</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <input onkeyup="verifyPassword()" id="password-confirm" type="password" minlength="6" placeholder="Confirm password" class="form-control" name="password_confirmation" required>

                                        <span id="error" class="help hidden" style="color:darkred">Passwords don't match</span>
                                    </div>
                                </div>

                                <hr>
                                <h3> Institutional Details </h3>


                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" value="{{old('name')}}" name="name" required>
                                </div>

                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" class="form-control" value="{{old('phone')}}" name="phone" required>
                                </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" value="{{old('email')}}" name="email" required>
                                </div>

                                <div class="form-group">
                                    <label>City</label>

                                    <select class="form-control" name="city" required>

                                        <option >Abuja</option>
                                        <option >Abia</option>
                                        <option >Adamawa</option>
                                        <option >Anambra</option>
                                        <option >Akwa Ibom</option>
                                        <option >Bauchi</option>
                                        <option >Bayelsa</option>
                                        <option >Benue</option>
                                        <option >Borno</option>
                                        <option >Cross River</option>
                                        <option >Delta</option>
                                        <option >Ebonyi</option>
                                        <option >Enugu</option>
                                        <option >Edo</option>
                                        <option >Ekiti</option>
                                        <option >Gombe</option>
                                        <option >Imo</option>
                                        <option >Jigawa</option>
                                        <option >Kaduna</option>
                                        <option >Kano</option>
                                        <option >Katsina</option>
                                        <option >Kebbi</option>
                                        <option >Kogi</option>
                                        <option >Kwara</option>
                                        <option >Lagos</option>
                                        <option >Nasarawa</option>
                                        <option >Niger</option>
                                        <option >Ogun</option>
                                        <option >Ondo</option>
                                        <option >Osun</option>
                                        <option >Oyo</option>
                                        <option >Plateau</option>
                                        <option >Rivers</option>
                                        <option >Sokoto</option>
                                        <option >Taraba</option>
                                        <option >Yobe</option>
                                        <option >Zamfara</option>

                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea name="address" class="form-control" required>
                                        {{ trim(old('address'))}}
                                    </textarea>
                                </div>

                                <button id="addButton" class="btn btn-primary">Add</button>

                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


    <script>
         function verifyPassword(){
                var password = $('#password');
                var passwordConfirm = $('#password-confirm');
                var addButton = $('#addButton');
                var error = $('#error');
                var success = $('#success');

                if(password.val() !== passwordConfirm.val()){
                    addButton.addClass('disabled');
                    error.removeClass('hidden');
                    success.addClass('hidden');
                } else {
                    addButton.removeClass('disabled');
                    error.addClass('hidden');
                    success.removeClass('hidden');
                }
            }

    </script>

@endsection















