@extends('layouts.admin')

@section('content')

    <style>

        hr{
            margin-top: 0;
            margin-bottom: 10px;
        }
    </style>

    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title">
                <a href="{{url('manage-labs')}}">
                    <i class="icon-custom-left"></i>
                </a>

                <div class="pull-right">

                    @if($lab->isBanned == 0)
                        <a href="{{url('suspend-lab/' . $lab->docid)}}" class="btn btn-danger">Suspend Lab</a>
                    @else
                        <a href="{{url('unsuspend-lab/' . $lab->docid)}}" class="btn btn-warning">Readmit Lab</a>
                    @endif
                    </div>
                </div>
            </div>


            <div class="row-fluid">
                <div class="grid simple col-md-10 col-md-offset-1">
                    <div class="grid-title no-border">
                    </div>
                    <div class="grid-body no-border">
                        <br>
                        <div class="row">

                            <div class="col-md-12" style="margin-top: -30px;margin-bottom: 10px;">
                                <div class="col-md-1" style="padding:0;">
                                    <img style="float: left; min-height: 52px; min-width: 52px" src="{{$lab->User->image}}" alt="" class="img img-thumbnail"/>
                                </div>

                                <div class="col-md-11">
                                    <h3 style="padding: 10px"><span class="semi-bold">{{$lab->name}} </span></h3>
                                </div>

                            </div>

                            <div class="form-group col-md-6 col-sm-12">

                                <h4>Manager Details</h4>
                                <hr>
                                <label class="form-label">Name</label>
                                <span class="help">{{$lab->User->fname}}</span>
                                <span class="help">{{$lab->User->sname}}</span>
                                <br>
                                <label class="form-label">Email</label>
                                <span class="help">{{$lab->User->email}}</span>
                                <br>
                                <label class="form-label">Phone</label>
                                <span class="help">{{$lab->User->phone}}</span>
                                <br>
                                <label class="form-label">Address</label>
                                <span class="help">{{$lab->User->address}}</span>
                                <br>
                                <label class="form-label">Created By</label>
                                <span class="help">{{$lab->CreatedBy->fname}} {{$lab->CreatedBy->sname}}</span>

                            </div>

                            <div class="form-group col-md-6 col-sm-12">
                                <h4>Lab Details</h4>
                                <hr>
                                <label class="form-label">Lab Name</label>
                                <span class="help">{{$lab->name}}</span>

                                <br>
                                <label class="form-label">Lab Phone</label>
                                <span class="help">{{$lab->phone}}</span>

                                <br>
                                <label class="form-label">Lab Email</label>
                                <span class="help">{{$lab->email}}</span>

                                <br>
                                <label class="form-label">City</label>
                                <span class="help">{{$lab->city}}</span>

                                <br>
                                <label class="form-label">Address</label>
                                <span class="help">{{$lab->address}}</span>
                            </div>
                        </div>


                    </div>
                </div>

            </div>


            <div class="row-fluid">
                <div class="grid simple col-md-10 col-md-offset-1">
                    <div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                                    Supported Tests
                                </a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Add Supported Tests</a></li>
                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Cases</a></li>
                            {{--<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>--}}
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <div class="grid-body no-border">

                                    <table class="table table-striped">
                                        <thead>
                                        <tr>

                                            <th>Test Title</th>
                                            <th>Test Description</th>
                                            <th>Date Started</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($lab->LabTests as $test)
                                            <tr>
                                                <td>{{$test->Test->title}}</td>
                                                <td>{{$test->Test->description}}</td>
                                                <td>{{$test->Test->created_at->toDayDateTimeString()}}</td>
                                                <td>
                                                    <a class="label label-danger" href="{{url('remove-test/' . $lab->labid .'/' . $test->Test->testid )}}">Remove</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>


                                </div>


                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile">
                                <div class="grid-body no-border">

                                    <div class="grid-body no-border">

                                        @if(count($tests) <= 0)
                                            <p align="center">
                                                You already support all available tests on the system
                                            </p>

                                        @else
                                            <form method="post" action="{{url('add-lab-test')}}">
                                                {{csrf_field()}}
                                                <input type="hidden" name="labid" value="{{$lab->labid}}">

                                                @foreach($tests as $test)
                                                    <div class="form-group col-md-4" style="padding: 0; margin: 0;">
                                                        <input class="col-md-1" type="checkbox" name="testid[]" value="{{$test->testid}}" >
                                                        <label class="col-md-11">{{$test->title}}</label>
                                                    </div>
                                                @endforeach

                                                <div class="col-md-1 col-md-offset-11">
                                                    <button class="btn btn-primary">Add</button>
                                                </div>
                                            </form>

                                        @endif


                                    </div>

                                </div>
                            </div>


                            <div role="tabpanel" class="tab-pane" id="messages">
                                <div class="grid-body no-border">

                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Case ID</th>
                                            <th>Date Started</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php $labCases = $lab->Cases()->paginate(1); ?>
                                        @foreach($labCases as $case)
                                            <tr>
                                                <td>{{$case->casid}}</td>
                                                <td>{{$case->created_at->toDayDateTimeString()}}</td>
                                                <td>
                                                    @if($case->status == 'Pending')
                                                        <label class="label label-warning">Pending</label>
                                                    @endif
                                                    @if($case->status == 'Complete')
                                                        <label class="label label-success">Complete</label>
                                                    @endif
                                                    @if($case->status == 'Cancelled')
                                                        <label class="label label-danger">Cancelled</label>
                                                    @endif

                                                </td>
                                                <td>
                                                    <a href="{{url('case/' . $case->casid)}}">View</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{$labCases->render()}}

                                </div>
                            </div>


                            {{--<div role="tabpanel" class="tab-pane" id="messages">...</div>--}}
                            {{--<div role="tabpanel" class="tab-pane" id="settings">...</div>--}}
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection