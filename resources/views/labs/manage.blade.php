@extends('layouts.admin')

@section('content')


    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title">
            </div>

            <div class="row-fluid">

                <div class="span12">
                    <div class="grid simple ">
                        <div class="grid-title">
                            <h4 style="margin: 10px 0 0 0;">Registered <span class="semi-bold">Labs</span></h4>
                            <div class="tools">

                                <div class="page btn-group" role="group" aria-label="Search form">
                                    <a href="{{url('manage-labs?pages=10')}}" type="button"
                                       @if(session()->get('pages') == 10)
                                       style="color:white"
                                       class="btn btn-primary"
                                       @else
                                       class="btn"
                                            @endif>10</a>

                                    <a href="{{url('manage-labs?pages=25')}}" type="button"
                                       @if(session()->get('pages') == 25)
                                       style="color:white"
                                       class="btn btn-primary"
                                       @else
                                       class="btn"
                                            @endif>25</a>

                                    <a href="{{url('manage-labs?pages=50')}}" type="button"
                                       @if(session()->get('pages') == 50)
                                       style="color:white"
                                       class="btn btn-primary"
                                       @else
                                       class="btn"
                                            @endif>50</a>
                                </div>

                                <div class="filtericons" align="right" >

                                    <div class="btn-group btn-group-xs" role="group" aria-label="...">

                                        <script>
                                            $(document).ready(function () {
                                                $('.dropdown-menu li').on('click',function(e){
                                                    e.preventDefault();
                                                    var filterlabel = $("#filterlabel");
                                                    var value =  $(this).data("key");
                                                    var selected = $(this).text();
                                                    filterlabel.html(selected + '<span class="caret"></span>');
                                                    $('#by').val(value);
                                                    console.log(value);


                                                });

                                            });
                                        </script>

                                        <form class="form-inline" action="{{url('manage-cases')}}" method="get">
                                            <div class="input-group">

                                                <div class="input-group-btn">
                                                    <button type="button" id="filterlabel" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Select <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li data-key="fname"><a href="#">Firstname</a></li>
                                                        <li><a href="#">Surname</a></li>
                                                        <li><a href="#">Email</a></li>
                                                        <li><a href="#">Phone</a></li>
                                                    </ul>
                                                    <input name="by" id="by" type="hidden">
                                                    <input name="pages" value="{{$input::get('pages')}}" type="hidden">
                                                </div><!-- /btn-group -->

                                                <input style="border-top-right-radius: 5px;border-bottom-right-radius: 5px;" class="form-control" value="" placeholder="Search" name="term" type="text">
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="grid-body ">
                            <table class="table table-striped">
                                @if(count($labs) > 0)
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>City</th>
                                        <th>Phone</th>
                                        <th>Registered</th>
                                        <th></th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($labs as $lab)

                                        <tr class="odd gradeX">
                                            <td class="center"> {{$lab->labid}} </td>
                                            <td class="center"> {{$lab->name}} </td>
                                            <td class="center"> {{$lab->city}} </td>
                                            <td class="center"> {{$lab->phone}} </td>
                                            <td class="center"> {{$lab->created_at->toDayDateTimeString()}} </td>
                                            <td>
                                                <a href="{{url('lab/' . $lab->labid)}}" class="label label-primary">View</a>
                                            </td>
                                        </tr>

                                    @endforeach
                                    @else
                                        <h3 style="text-align: center"> There are no labs on the system </h3>
                                    @endif


                                    </tbody>
                            </table>

                            {{$labs->links()}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection















