@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title full">

                <div class="pull-right">
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Edit Setting
                    </button>
                </div>

            </div>

            <div class="row-fluid">
                <div class="collapse span12" id="collapseExample">
                    <div class="grid simple ">
                        <div class="grid-title">
                            <h4>Edit <span class="semi-bold">Setting</span></h4>
                        </div>
                        <div class="grid-body ">
                            <form method="post" class="form-inline" action="{{url('edit-setting')}}">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label>Settings</label>
                                    <select name="name" class="form-control">
                                        <option value="minimumNairaPayoutThreshold">Minimum Naira Payout Threshold
                                        </option>
                                        <option value="minimumDollarPayoutThreshold">Minimum Dollar Payout Threshold
                                        </option>
                                        <option value="consultationFee">Consultation Fee</option>
                                        <option value="subscriptionFee">Subscription Fee</option>
                                        <option value="accessFee">Access Fee</option>
                                        <option value="paymentSchedule">Payment Schedule (In days)</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Value</label>
                                    <input type="text" class="form-control" name="value">
                                </div>

                                <div class="form-group">
                                    <button style="margin-top: 25px" class="btn btn-primary">Save</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

                <div class="span12">
                    <div class="grid simple collapsed ">
                        <div class="grid-title">
                            <h4>Current <span class="semi-bold">Settings</span></h4>
                            <div class="tools">

                            </div>
                        </div>
                        <div class="grid-body ">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Value</th>
                                    <th>Date Set</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="odd gradeX">
                                    <td>Minimum Naira Payment Threshold(&#x20A6;)</td>
                                    <td>{{$mnpt->value}}</td>
                                    <td>
                                        {{$mnpt->created_at->toDayDateTimeString()}}
                                    </td>
                                </tr>

                                <tr class="odd gradeX">
                                    <td>Minimum Dollar Payment Threshold ($)</td>
                                    <td>{{$mdpt->value}}</td>
                                    <td>
                                        {{$mdpt->created_at->toDayDateTimeString()}}
                                    </td>
                                </tr>

                                <tr class="odd gradeX">
                                    <td>Consultation Fee(&#x20A6;)</td>
                                    <td>{{$consultationFee->value}}</td>
                                    <td>
                                        {{$consultationFee->created_at->toDayDateTimeString()}}
                                    </td>
                                </tr>

                                <tr class="odd gradeX">
                                    <td>Subscription Fee (&#x20A6;)</td>
                                    <td>{{$subscriptionFee->value}}</td>
                                    <td>
                                        {{$subscriptionFee->created_at->toDayDateTimeString()}}
                                    </td>
                                </tr>

                                <tr class="odd gradeX">
                                    <td>Access Fee (In Percentage)</td>
                                    <td>{{$accessFee->value}}</td>
                                    <td>
                                        {{$accessFee->created_at->toDayDateTimeString()}}
                                    </td>
                                </tr>

                                <tr class="odd gradeX">
                                    <td>Payment Schedule (In Days)</td>
                                    <td>{{$paymentSchedule->value}}</td>
                                    <td>
                                        {{$paymentSchedule->created_at->toDayDateTimeString()}}
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="span12">
                        <div class="grid simple collapse ">
                            <div class="grid-title">
                                <h4>Settings <span class="semi-bold">History</span></h4>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                    <a href="#grid-config" data-toggle="modal" class="config"></a>
                                    <a href="javascript:;" class="reload"></a>
                                    <a href="javascript:;" class="remove"></a>
                                </div>
                            </div>
                            <div class="grid-body ">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Value</th>
                                        <th>Date Set</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($settings as $setting)

                                        <tr class="odd gradeX">
                                            <td>{{$setting->name}}</td>
                                            <td>{{$setting->value}}</td>
                                            <td>
                                                {{$setting->created_at->toDayDateTimeString()}}
                                            </td>
                                        </tr>

                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection















