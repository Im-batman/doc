<?php use Carbon\Carbon; ?>
@extends('layouts.admin')

@section('content')
    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title" style=" margin-left: 60px;margin-bottom: 50px;margin-right: 60px;">

                <a href="{{url('manage-patients')}}">
                    <i class="icon-custom-left"></i>
                </a>


                <div class="pull-right">
                    <a href="{{url('edit-patient/' . $patient->patid)}}" class="btn btn-primary">Edit details</a>

                    @if($patient->isConfirmed == 1 )
                        @if($patient->isSuspended == 0)
                            <a style="margin-right:0 !important;" href="{{url('suspend-patient/' . $patient->patid)}}" class="btn btn-danger">Suspend patient</a>
                        @else
                                <a style="margin-right:0 !important;" href="{{url('unsuspend-patient/' . $patient->patid)}}" class="btn btn-warning">Readmit patient</a>
                        @endif
                    @endif
                </div>
            </div>
        </div>


        <div class="row-fluid">
            <div class="grid simple col-md-10 col-md-offset-1">
                <div class="grid-title no-border">
                </div>
                <div class="grid-body no-border">
                    <br>
                    <div class="row">
                        <div class="col-md-4">

                            <img src="{{$patient->image}}" class="img img-thumbnail" style="width: 100%"/>

                            @if($patient->isConfirmed == 1)
                                <label class="label label-success">Confirmed</label>
                            @else
                                <label class="label label-danger">Un-Confirmed</label>
                            @endif

                            @if($patient->isSuspended == 1)
                                <label class="label label-danger">Suspended</label>
                            @endif
                        </div>
                        <div class="col-md-8">
                            <h3 style="margin: 0 0 10px 0;"><span class="semi-bold">{{$patient->fname}} {{$patient->sname}}</span></h3>
                        </div>

                        <div class="form-group col-md-4 col-sm-12">

                            <label class="form-label">Email</label>
                            <span class="help">{{$patient->email}}</span>
                            <br>
                            <label class="form-label">Phone</label>
                            <span class="help">{{$patient->phone}}</span>
                            <br>
                            <label class="form-label">Gender</label>
                            <span class="help">{{$patient->gender}}</span>
                            <br>
                            <label class="form-label">Total Cases</label>
                            <span class="help">{{count($patient->Cases)}}</span>

                        </div>

                        <div class="form-group col-md-4 col-sm-12">

                            <label class="form-label">DOB</label>
                            <span class="help">{{$patient->dob}}</span>
                            <br>
                            <label class="form-label">Height</label>
                            <span class="help">{{$patient->height}}cm</span>
                            <br>
                            <label class="form-label">Weight</label>
                            <span class="help">{{$patient->weight}}kg</span>
                            <br>
                            <label class="form-label">Total Dependants</label>
                            <span class="help">{{count($patient->Dependants)}}</span>

                        </div>
                    </div>


                </div>
            </div>

        </div>



        <div class="row-fluid">
            <div class="grid simple col-md-10 col-md-offset-1">
                <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                                Dependant
                            </a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Cases</a></li>
                        {{--<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Cases</a></li>--}}
                        {{--<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>--}}
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="grid-body no-border">

                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th title="Case Count">Cases</th>
                                        <th>Phone</th>
                                        <th>Gender</th>
                                        <th>Date Created</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($patient->Dependants as $dependant)
                                        <tr>
                                            <td>{{$dependant->fname}} {{$dependant->sname}}</td>
                                            <td>{{count($dependant->Cases)}}</td>
                                            <td>{{$dependant->phone}}</td>
                                            <td>{{$dependant->gender}}</td>
                                            <td>{{$dependant->created_at->toDayDateTimeString()}}</td>
                                            <td>
                                                <a class="label label-primary" href="{{url('dependant/' . $dependant->pdid) }}">View</a>
                                                <a class="label label-warning" href="{{url('edit-dependant/' . $dependant->pdid) }}">Edit</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>


                            </div>


                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="grid-body no-border">

                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Case ID</th>
                                        <th>For</th>
                                        <th>Date Started</th>
                                        <th>Date Ended</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    <?php $patientCases = $patient->Cases()->paginate(10);  ?>

                                    @foreach($patientCases as $case)
                                        <tr>
                                            <td>{{$case->casid}}</td>
                                            <td>
                                                @if($case->type == "patient")
                                                    This Patient
                                                @else
                                                    <a href="{{url('dependant/' . $case->dependant->pdid) }}">
                                                        {{$case->Dependant->fname}} {{$case->Dependant->sname}}
                                                    </a>
                                                @endif
                                            </td>
                                            <td>{{$case->created_at->toDayDateTimeString()}}</td>
                                            <td>
                                                @if(isset($case->ended_at))
                                                {{Carbon::createFromFormat("Y-m-d H:i:s",$case->ended_at)->toDayDateTimeString()}}
                                                @endif
                                            </td>
                                            <td>
                                                @if($case->status == 'Available')
                                                    <label class="label label-primary">Available</label>
                                                @endif

                                                @if($case->status == 'Pending')
                                                    <label class="label label-warning">Pending</label>
                                                @endif
                                                @if($case->status == 'Completed')
                                                    <label class="label label-success">Complete</label>
                                                @endif
                                                @if($case->status == 'Cancelled')
                                                    <label class="label label-danger">Cancelled</label>
                                                @endif

                                            </td>
                                            <td>
                                                <a class="label label-primary" href="{{url('case/' . $case->casid)}}">View</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{$patientCases->render()}}

                            </div>
                        </div>


                        {{--<div role="tabpanel" class="tab-pane" id="messages">...</div>--}}
                        {{--<div role="tabpanel" class="tab-pane" id="settings">...</div>--}}
                    </div>
                </div>
            </div>
        </div>






























        {{--<div class="row-fluid">--}}
            {{--<div class="grid simple col-md-10 col-md-offset-1">--}}
                {{--<div class="grid-title no-border">--}}
                    {{--<h4>Dependants</h4>--}}
                {{--</div>--}}
                {{--<div class="tools">--}}
                    {{--<a href="javascript:;" class="collapse"></a>--}}
                    {{--<a href="#grid-config" data-toggle="modal" class="config"></a>--}}
                    {{--<a href="javascript:;" class="reload"></a>--}}
                    {{--<a href="javascript:;" class="remove"></a>--}}
                {{--</div>--}}

                {{--<div class="grid-body no-border">--}}
                    {{--<table class="table table-striped">--}}
                            {{--<thead>--}}
                            {{--<tr>--}}
                                {{--<th>Name</th>--}}
                                {{--<th title="Case Count">Cases</th>--}}
                                {{--<th>Phone</th>--}}
                                {{--<th>Gender</th>--}}
                                {{--<th>Date Created</th>--}}
                                {{--<th></th>--}}
                            {{--</tr>--}}
                            {{--</thead>--}}
                            {{--<tbody>--}}

                            {{--@foreach($patient->Dependants as $dependant)--}}
                                {{--<tr>--}}
                                    {{--<td>{{$dependant->fname}} {{$dependant->sname}}</td>--}}
                                    {{--<td>{{count($dependant->Cases)}}</td>--}}
                                    {{--<td>{{$dependant->phone}}</td>--}}
                                    {{--<td>{{$dependant->gender}}</td>--}}
                                    {{--<td>{{$dependant->created_at->toDayDateTimeString()}}</td>--}}
                                    {{--<td>--}}
                                        {{--<a class="label label-primary" href="{{url('dependant/' . $dependant->pdid) }}">View</a>--}}
                                        {{--<a class="label label-warning" href="{{url('edit-dependant/' . $dependant->pdid) }}">Edit</a>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                            {{--@endforeach--}}
                            {{--</tbody>--}}
                    {{--</table>--}}

                {{--</div>--}}

            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="row-fluid">--}}
            {{--<div class="grid simple col-md-10 col-md-offset-1">--}}
                {{--<div class="grid-title no-border">--}}
                    {{--<h4>Cases</h4>--}}
                {{--</div>--}}
                {{--<div class="grid-body no-border">--}}
                    {{--<table class="table table-striped">--}}
                        {{--<thead>--}}
                        {{--<tr>--}}
                            {{--<th>Case ID</th>--}}
                            {{--<th>For</th>--}}
                            {{--<th>Date Started</th>--}}
                            {{--<th>Status</th>--}}
                            {{--<th></th>--}}
                        {{--</tr>--}}
                        {{--</thead>--}}
                        {{--<tbody>--}}

                        {{--@foreach($patient->Cases as $case)--}}
                            {{--<tr>--}}
                                {{--<td>{{$case->casid}}</td>--}}
                                {{--<td>--}}
                                    {{--@if($case->type == "patient")--}}
                                        {{--This Patient--}}
                                        {{--@else--}}
                                        {{--<a href="{{url('dependant/' . $case->dependant->pdid) }}">--}}
                                            {{--{{$case->Dependant->fname}} {{$case->Dependant->sname}}--}}
                                        {{--</a>--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                                {{--<td>{{$case->created_at->toDayDateTimeString()}}</td>--}}
                                {{--<td>--}}
                                    {{--@if($case->status == 'Available')--}}
                                        {{--<label class="label label-primary">Available</label>--}}
                                    {{--@endif--}}

                                {{--@if($case->status == 'Pending')--}}
                                        {{--<label class="label label-warning">Pending</label>--}}
                                    {{--@endif--}}
                                    {{--@if($case->status == 'Complete')--}}
                                        {{--<label class="label label-success">Complete</label>--}}
                                    {{--@endif--}}
                                    {{--@if($case->status == 'Cancelled')--}}
                                        {{--<label class="label label-danger">Cancelled</label>--}}
                                    {{--@endif--}}

                                {{--</td>--}}
                                {{--<td>--}}
                                    {{--<a class="label label-primary" href="{{url('case/' . $case->casid)}}">View</a>--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                        {{--@endforeach--}}
                        {{--</tbody>--}}
                    {{--</table>--}}
                {{--</div>--}}

            {{--</div>--}}
        {{--</div>--}}


</div>
@endsection