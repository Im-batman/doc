@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple col-md-6 col-md-offset-3 ">
                        <div class="grid-title">
                            <h4> <span class="semi-bold">Update Patient</span></h4>
                            <div class="tools">
                                <a href="{{url('patient/' . $patient->patid)}}" >
                                    <i style="font-size: 16px" class="fa fa-close"></i>
                                </a>
                            </div>
                        </div>
                        <div class="grid-body ">

                            <form method="post" action="{{url('update-patient/'.$patient->patid)}}">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" name="fname" value="{{$patient->fname}}">
                                </div>

                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" name="sname" value="{{$patient->sname}}">
                                </div>
                                <div class="form-group">
                                    <label>Gender</label>
                                    <select name="gender">
                                        <option @if($patient->gender == 'male')
                                            selected
                                        @endif>Male</option>
                                        <option @if($patient->gender == 'female')
                                            selected
                                        @endif>Female</option>
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email" value="{{$patient->email}}">
                                </div>

                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" class="form-control" name="phone" value="{{$patient->phone}}">
                                </div>

                                <div class="form-group">
                                    <label>Height</label>
                                    <input type="text" class="form-control" name="height" value="{{$patient->height}}">
                                </div>

                                <div class="form-group">
                                    <label>Weight</label>
                                    <input type="text" class="form-control" name="weight" value="{{$patient->weight}}">
                                </div>

                                <button type="submit" class="btn btn-primary">Update</button>

                            </form>
                        </div>
                    </div>
                </div>

                {{--<div class="span12">--}}
                    {{--<div class="grid simple ">--}}
                        {{--<div class="grid-title">--}}
                            {{--<h4>Registered <span class="semi-bold">Specializations</span></h4>--}}
                            {{--<div class="tools">--}}
                                {{--<a href="javascript:;" class="collapse"></a>--}}
                                {{--<a href="#grid-config" data-toggle="modal" class="config"></a>--}}
                                {{--<a href="javascript:;" class="reload"></a>--}}
                                {{--<a href="javascript:;" class="remove"></a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="grid-body ">--}}
                            {{--<table class="table table-striped">--}}
                                {{--@if(count($patient) > 0)--}}
                                    {{--<thead>--}}
                                    {{--<tr>--}}
                                        {{--<th>ID</th>--}}
                                        {{--<th>Name</th>--}}
                                        {{--<th>Description</th>--}}
                                        {{--<th></th>--}}

                                    {{--</tr>--}}
                                    {{--</thead>--}}
                                    {{--<tbody>--}}


                                        {{--<tr class="odd gradeX">--}}
                                            {{--<td>{{$patient->name}}</td>--}}

                                            {{--<td>--}}
                                                {{--<a href="{{url('edit-test/' . $specialization->spid)}}" class="label label-primary">Edit</a>--}}
                                            {{--</td>--}}
                                        {{--</tr>--}}


                                    {{--@else--}}

                                        {{--<h3 style="text-align: center"> There are no specializations registered on the system </h3>--}}

                                    {{--@endif--}}


                                    {{--</tbody>--}}
                            {{--</table>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>




@endsection















