<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cases extends Model
{
    protected $primaryKey = 'casid';
    protected $table = 'cases';

	public function Patient() {
		return $this->belongsTo(patient::class,'patid','patid');
    }

	public function Dependant() {
		return $this->belongsTo(patientDependant::class,'pdid','pdid');
    }

	public function Doctor() {
		return $this->belongsTo(doctor::class,'docid','docid');
    }

	public function Pharmacy() {
		return $this->hasOne(pharmacy::class,'pharmid','pharmid');
    }

	public function Tests() {
		return $this->hasMany(caseTest::class,'casid','casid');
    }

	public function Specialization() {
		return $this->belongsTo(specialization::class,'spid','spid');
    }

	public function Feedback() {
		return $this->hasOne(feedback::class,'casid','casid');
    }

	public function Doctors() {
		return $this->hasMany(caseDoctor::class,'casid','casid');
    }

	public function Updates() {
		return $this->hasMany(caseUpdate::class,'casid','casid');
    }

	public function Prescriptions() {
		return $this->hasMany(prescription::class,'casid','casid');
    }

	public function Payment() {
		return $this->hasOne(payment::class,'casid','casid');
    }

}
