<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class doctorlang extends Model
{
    protected $primaryKey = 'doclid';
    protected $table      = 'doctorlangs';

	public function Doctor() {
		return $this->belongsTo(doctor::class,'docid','docid');
    }
}
