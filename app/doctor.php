<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class doctor extends Model
{
    protected $primaryKey = 'docid';
    protected $table = 'doctors';
    protected $guarded = [];

	public function Files() {
		return $this->hasMany(doctorsDoc::class,'docid','docid');
    }

	public function Specialization() {
		return $this->hasMany(doctorsSpec::class,'docid','docid');
    }

	public function Cases() {
		return $this->hasMany(cases::class,'docid','docid');
    }

	public function Payments() {
		return $this->hasMany(doctorPayment::class,'docid','docid');
    }

}
