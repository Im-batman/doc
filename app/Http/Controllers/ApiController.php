<?php

namespace App\Http\Controllers;

use App\caseDoctor;
use App\cases;
use App\caseTest;
use App\caseUpdate;
use App\confirmation;
use App\doctor;
use App\doctorlang;
use App\doctorsDoc;
use App\doctorsSpec;
use App\feedback;
use App\hospital;
use App\lab;
use App\loungeItem;
use App\medicalHistory;
use App\patient;
use App\Mail\confirmEmail;
use App\passwordReset;
use App\patientDependant;
use App\payment;
use App\pharmacy;
use App\prescription;
use App\setting;
use App\specialization;
use App\test;
use App\testResult;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ApiController extends Controller
{

	public function signUp(Request $request) {


		$patientEmailCount = patient::where("email",$request->input('email'))->count();
		if($patientEmailCount > 0) return array('message' => "Email already exists");

		$phone = $request->input('phone');
		if($phone != null) {

			$patientPhoneCount = patient::where( "phone", $phone)->count();
			if ( $patientPhoneCount > 0 ) {
				return array( 'message' => "Phone number already exists" );
			}
		}

//		try{
			$response = DB::transaction( function () {
				global $request;

				$signup = new patient();
				$signup->fname = $request->input('fname');
				$signup->sname = $request->input('sname');
				$signup->dob = $request->input('dob');
				$signup->weight = $request->input('weight');
				$signup->height = $request->input('height');
				$signup->password = bcrypt($request->input('password'));
				$signup->email = $request->input('email');
				$signup->gender = $request->input('gender');
				$signup->phone = $request->input('phone');
				$signup->image = url('/profileImages/default-image.png');
				$signup->isConfirmed = 0;
				$status = $signup->save();

				if($status) return array('message' => 1); else return array('message' => 0);

			},2);

			return $response;

//		}catch (\Exception $exception){
//			Bugsnag::notifyException($exception);
//			return array('message' => 0);
//		}


	}

	public function doctorSignUp(Request $request) {


		$doctorEmailCount = doctor::where("email",$request->input('email'))->count();
		if($doctorEmailCount > 0) return array('message' => "Email already exists");

		$phone = $request->input('phone');
		if($phone != null) {

			$doctorPhoneCount = patient::where( "phone", $phone)->count();
			if ( $doctorPhoneCount > 0 ) {
				return array( 'message' => "Phone number already exists" );
			}
		}

//		try{
		$response = DB::transaction( function () {
			global $request;

			$doctor = new doctor();
			$doctor->fname = $request->input('fname');
			$doctor->sname = $request->input('sname');
			$doctor->licenseid = $request->input('licenseid');
			$doctor->countryOfPractice = $request->input('countryOfPractice');
			$doctor->nationality = $request->input('nationality');
			$doctor->yearsOfExperience = $request->input('yearsOfExperience');
			$doctor->hospitalAffiliation = $request->input('hospitalAffiliation');
			$doctor->paymentMethod = $request->input('paymentMethod');
			$doctor->paypal = $request->input('paypal');
			$doctor->accountName = $request->input('accountName');
			$doctor->accountNumber = $request->input('accountNumber');
			$doctor->accountBank = $request->input('accountBank');
			$doctor->bankCode = $request->input('bankCode');
			$doctor->password = bcrypt($request->input('password'));
			$doctor->email = $request->input('email');
			$doctor->image = url('/profileImages/default-image.png');
			$doctor->gender = $request->input('gender');
			$doctor->phone = $request->input('phone');
			$doctor->isConfirmed = 0;
			$status = $doctor->save();

			if($status) return array('message' => 1); else return array('message' => 0);

		},2);

		return $response;

//		}catch (\Exception $exception){
//			Bugsnag::notifyException($exception);
//			return array('message' => 0);
//		}

	}

	public function doctorAddSpecialization( Request $request ) {

		$docid = $request->input('docid');
		$spid  = $request->input('spid');

		$specilization = new doctorsSpec();
		$specilization->docid = $docid;
		$specilization->spid  = $spid;
		$specilization->save();

		return array("message" => 1);

	}

	public function doctorRemoveSpecialization( Request $request ) {

		$dsid = $request->input('dsid');

		doctorsSpec::destroy($dsid);

		return array("message" => 1);

	}


	public function doctorAddLanguage( Request $request ) {

		$docid = $request->input('docid');

		$language = new doctorlang();
		$language->docid = $docid;
		$language->language = $request->input('language');
		$language->save();


		return array("message" => 1);

	}

	public function doctorRemoveLanguage( Request $request ) {

		$doclid = $request->input('doclid');

		doctorlang::destroy($doclid);

		return array("message" => 1);

	}

	public function availableCases( Request $request ) {
		$docid = $request->input('docid');

		$doctor = doctor::find($docid);

		$doctorSpecsIDs = array();


		foreach($doctor->Specialization as $specialization){
			array_push($doctorSpecsIDs,$specialization->spid);
		}


		if(count($doctorSpecsIDs) <= 0)
			//return general cases when the doctor has no specialization
			$availableCases = cases::where('status','Available')->where("spid",null)->get();
		else {

			//return general and specialized cases when doctor has a specialization

			$availableGeneralCases = cases::where('status','Available')->where("spid",null)->get();
			$availableSpecialistCases = cases::where('status','Available')->whereIn("spid",$doctorSpecsIDs)->get();


			$availableCases = collect($availableGeneralCases)->merge($availableSpecialistCases);
		}

		//get relationship data for the cases
		foreach($availableCases as $item){

			try{$item->Patient;}catch (\Exception $exception){}
			try{$item->Doctor;}catch (\Exception $exception){}
			try{$item->Specialization;}catch (\Exception $exception){}
			try{$item->Dependant;}catch (\Exception $exception){}
		}

		return $availableCases;


	}

	public function doctorAddCaseUpdate( Request $request ) {
		$docid = $request->input('docid');
		$casid = $request->input('casid');
		$patientCanView = $request->input('patientCanView');
		$case = cases::find($casid);

		if($case->docid != $docid)return response(array("message" => "Un-authorized. You are not the doctor handling this case"),500);

		$caseUpdate = new caseUpdate();
		$caseUpdate->docid = $docid;
		$caseUpdate->casid = $casid;
		if(!empty($patientCanView))
		$caseUpdate->patientCanView = $patientCanView;
		$caseUpdate->update = $request->input('update');
		$caseUpdate->save();

		return array("message" => 1);
	}

	public function caseDetails( Request $request ) {
		$casid = $request->input('casid');
		$docid = $request->input('docid');
		$patid = $request->input('patid');
		$case = cases::find($casid);

		if(empty($docid) && empty($patid) ) return response(array("message" => "Bad request."),500);
		if(!empty($docid) && $case->docid != $docid) return response(array("message" => "Un-authorized. You are not the doctor handling this case."),500);
		if(!empty($patid) && $case->patid != $patid) return response(array("message" => "You are not authorized to view this case."),500);

		try{$case->Patient;}catch (\Exception $exception){}
		try{$case->Patient->History;}catch (\Exception $exception){}
		try{$case->Dependant;}catch (\Exception $exception){}
		try{$case->Dependant->History;}catch (\Exception $exception){}
		try{$case->Doctor;}catch (\Exception $exception){}
		try{$case->Pharmacy;}catch (\Exception $exception){}
		try{$case->Tests;}catch (\Exception $exception){}
		try{

			$case->Specialization;

			foreach($case->Specialization as $specialization){
				$specialization->Specialization;
			}

		}catch (\Exception $exception){}
		try{$case->Feedback;}catch (\Exception $exception){}
		try{$case->Doctors;}catch (\Exception $exception){}
		try{

			$case->Updates;

			if(isset($patid)){
				$updatesForPatient = collect();
				foreach ($case->Updates as $update ){
					if($update->patientCanView == 1){
						$updatesForPatient->push($update);
					}
				}

				$case = collect($case)->put('patientupdates',$updatesForPatient);

			}
		}catch (\Exception $exception){}

		return $case;
	}

	public function doctorDetails( Request $request ) {
		$docid = $request->input('docid');
		$doctor = doctor::find($docid);

		try{$doctor->Specialization;}catch (\Exception $exception){}
		try{$doctor->Cases;}catch (\Exception $exception){}

		$totalPendingMaturity = 0;

		foreach($doctor->Cases as $case){
			$paymentSchedule = setting::where('name','paymentSchedule')->get()->last()->value;
			$accessFeePercent = setting::where('name','accessFee')->get()->last()->value;
			$maturityDate = Carbon::createFromFormat("Y-m-d H:i:s",$case->created_at)->addDays($paymentSchedule);

			$consultationFee = $case->Payment->amount;
			$accessFee       = $consultationFee * ( $accessFeePercent / 100 );
			$doctorsFee      = $consultationFee - $accessFee;

			// check if case is completed, mature
			if($case->status == "Completed" &&
			   Carbon::now()->lessThan($maturityDate) &&
			   $case->paymentStatus == "paid" ) {
				$totalPendingMaturity += $doctorsFee;
			}

		}

		$doctor = collect($doctor)->put('totalPendingMaturity',$totalPendingMaturity);

		return $doctor;

	}

	public function unacceptCase( Request $request ) {

		$response = DB::transaction(function(){
			global $request;

			$docid = $request->input('docid');
			$casid = $request->input('casid');



			$case = cases::find($casid);
			if($case->docid == $docid){


				$case = cases::find($casid);
				$case->docid = null;
				$case->status = "Available";
				$case->save();

				$caseDoctor = caseDoctor::where('casid',$casid)->where('docid',$docid)->get()->last();

				if(!isset($caseDoctor->dateEnded)){
					// if the doctor hasn't exited the case
					$caseDoctor->dateEnded = Carbon::now()->toDateTimeString();
					$caseDoctor->notes = $request->input('notes');
					$caseDoctor->save();
				} else {
					return response(array("message" => "You have already been removed from this case"),500);
				}

			} else {
				return response(array("message" => "You are not the doctor assigned to this case"),500);
			}

			return array("message" => 1);
		},2);

		return $response;

	}

	public function handleCardPayment( Request $request ) {

		try {
			$response = DB::transaction( function () {

				global $request;

				$response = json_decode( json_encode( $request->all() ) );

				if ( $response->event == "charge.success" ) {

					$data = $response->data;

					$email     = $data->customer->email;
					$casid     = $data->casid;
					$amount    = $data->amount / 100;
					$reference = $data->reference;

					$patient  = patient::where('email',$email)->first();


					$previousPayment = payment::where( 'patid', $patient->patid )->get()->last();

					if ( isset( $previousPayment ) ) {
						if ( $previousPayment->others != $reference ) { // check if user has been credited for transaction already
							                                            //in the case the user has an existing payment record



							$payment         = new payment();
							$payment->amount = $amount;
							$payment->method = 'card';
							$payment->patid  = $patient->patid;
							$payment->casid  = $casid;
							$payment->others = $reference;
							$payment->save();
//							Mail::to( $email )->send( new cardPaymentSuccessful( $data, $customer ) );


						} // end previous payment check

					} else {


						$payment         = new payment();
						$payment->amount = $amount;
						$payment->method = 'card';
						$payment->cid    = $patient->patid;
						$payment->others = $reference;
						$payment->save();

//						Mail::to( $email )->send( new cardPaymentSuccessful( $data, $customer ) );

					}
				}

				return response();

			}, 3 );

			return $response;

		} catch ( \Exception $exception ) {
//			Bugsnag::notifyException( $exception );
			return response("An error occurred.",500);
		}

	}

	public function consultationFee( Request $request ) {

		try{
			$consultationFee = setting::where( 'name', 'consultationFee' )->get()->last()->value;
			return array("message" => $consultationFee);
		}catch (\Exception $exception){
			return response(array("message" => "An error occurred"),500);
		}

	}

	public function subscriptionFee( Request $request ) {

		try{
			$subscriptionFee          = setting::where( 'name', 'subscriptionFee' )->get()->last()->value;
			return array("message" => $subscriptionFee);
		}catch (\Exception $exception){
			return response(array("message" => "An error occurred"),500);
		}

	}


	public function doctorUpdateBankDetails( Request $request ) {

		try{

			$type = $request->input('type');
			$docid = $request->input('docid');

			if($type == 'paypal'){

				$doctor = doctor::find($docid);
				$doctor->papal = $request->input('paypal');
				$doctor->paymentMethod = $type;
				$doctor->save();

			} else if ($type == "bank") {
				$doctor = doctor::find($docid);
				$doctor->paymentMethod = "bank";
				$doctor->accountName = $request->input('accountName');
				$doctor->accountNumber = $request->input('accountNumber');
				$doctor->bankCode = $request->input('bankCode');
				$doctor->accountBank = $request->input('accountBank');
				$doctor->save();
			}

			return array("message" => 1);


		}catch (\Exception $exception){
			return response(array("message" => "An error occurred."),500);
		}
	}


	public function changeProfilePhoto( Request $request ) {

		try{
			$patid = $request->input('patid');
			$image = $request->file('image');

			$inputFileName = Carbon::now()->timestamp . $image->getClientOriginalName();
			$image->move("uploads",$inputFileName);

			$patient = patient::find($patid);

			$patient->image = url('uploads/' . $inputFileName);
			$patient->save();

			return array("message" => 1);

		}catch (\Exception $exception){

			return response(array("message" => "An error occurred."),500);
		}


	}

	public function patientUploadTestResult( Request $request ) {
		$patid = $request->input('patid');
		$casid = $request->input('casid');

		$ctid = $request->input('ctid');

		$caseTest = caseTest::find($ctid);

		if($caseTest->Case->patid != $patid)
			return response(array("message" => "Un-authorized. You are not the patient for this case"),500);


		$descripton = $request->input('description');


		if ( $request->hasFile( 'file' ) ) {
			$filename = Carbon::now()->timestamp .  $request->file( 'file' )->getClientOriginalName();
			$request->file( 'file' )->move( 'uploads', $filename );
			$url = url( 'uploads/' . $filename );
		} else {
			return response( array( 'message' => "No file uploaded." ) );
		}

		$testResult = new testResult();
		$testResult->ctid = $ctid;
		$testResult->description = $descripton;
		$testResult->url = $url;
		$testResult->save();

		return array("message" => 1);


	}

	public function doctorUploadFile( Request $request ) {

		$docid = $request->input( 'docid' );

		if ( $request->hasFile( 'doc' ) ) {
			$filename = Carbon::now()->timestamp . $request->file( 'doc' )->getClientOriginalName();
			$request->file( 'doc' )->move( 'uploads', $filename );
			$url = url( 'uploads/' . $filename );
		} else {
			return response( array( 'message' => "No file uploaded." ) );
		}

		$doctorDoc              = new doctorsDoc();
		$doctorDoc->docid       = $docid;
		$doctorDoc->title       = $request->input( 'title' );
		$doctorDoc->description = $request->input( 'description' );
		$doctorDoc->url         = $url;
		$doctorDoc->save();

		return array( "message" => 1 );


	}

	public function doctorRemoveFile( Request $request ) {
		$dcid = $request->input('dcid');

		$doctorDoc = doctorsDoc::find($dcid);

		$urlArray = explode("/", $doctorDoc->url);
		$fileName = $urlArray[3]. "/" . $urlArray[4];
		unlink($fileName);

		doctorsDoc::destroy($dcid);

		return array("message" => 1);


	}

	public function addDependant(Request $request) {

		$patientEmailCount = patientDependant::where("email",$request->input('email'))->count();
		if($patientEmailCount > 0) return array('message' => "Email already exists");

		$patientPhoneCount = patientDependant::where("phone",$request->input('phone'))->count();
		if($patientPhoneCount > 0) return array('message' => "Phone number already exists");


		try{
			$response = DB::transaction( function () {
				global $request;

				$signup = new patientDependant();
				$signup->patid = $request->input('patid');
				$signup->fname = $request->input('fname');
				$signup->sname = $request->input('sname');
				$signup->dob = $request->input('dob');
				$signup->weight = $request->input('weight');
				$signup->height = $request->input('height');
				$signup->email = $request->input('email');
				$signup->gender = $request->input('gender');
				$signup->phone = $request->input('phone');
				$signup->image = url('/profileImages/default-image.png');
				$signup->save();

				return array("message" => 1);

			},2);

			return $response;

		}catch (\Exception $exception){
			//Bugsnag::notifyException($exception);
			return response(array("message" => "An error occurred"),500);
		}


	}

	public function updateDependant(Request $request) {

		try{
			$response = DB::transaction( function () {
				global $request;

				$pdid = $request->input('pdid');

				$patientDependant = patientDependant::find($pdid);
				$patientDependant->update($request->all());

				return 1;

			},2);

			return $response;

		}catch (\Exception $exception){
			//Bugsnag::notifyException($exception);
			return response(0,500);
		}

	}

	public function patientEditProfile( Request $request ) {
		$patid = $request->input('patid');
		$patient = patient::find($patid);
		$patient->update($request->all());

		return 1;
	}

	public function doctorEditProfile( Request $request ) {
		$docid = $request->input('docid');
		$doctor = doctor::find($docid);
		$doctor->update($request->all());

		return 1;
	}



	public function newCase( Request $request ) {
		$type = $request->input('type');

		$patid = $request->input('patid');
		$patient = patient::find($patid);

		if($patient->isConfirmed == 0) return response(array("message" => "You must be confirmed to start a case"),500);
		if($patient->isSuspended == 1) return response(array("message" => "You have been suspended from using this service"),500);

		$case = new cases();
		$case->patid = $request->input('patid');

		if(!empty($type)) $case->type  =  $type;

		$case->pdid  = $request->input('pdid');
		$case->description = $request->input('description');
		$case->status = "Available";
		$case->save();

		return array("message" => 1);

	}


    public function suggestDoctors(Request $request)
    {
      $casid = $request->input('casid');
      $case = cases::find($casid);

	}

	public function acceptCase( Request $request ) {
		$casid = $request->input('casid');
		$docid = $request->input('docid');
		$case = cases::find($casid);

		if($case->docid == $docid){
			return response(array("message" => "You have already accepted this case"),500);
		}

		if($case->docid !== null ){
			return response(array("message" => "Another doctor has already accepted this case."),500);
		}


		$case->status = 'Pending';
		$case->docid = $docid;
		$case->save();

		$caseDoctor = new caseDoctor();
		$caseDoctor->docid = $docid;
		$caseDoctor->casid = $casid;
		$caseDoctor->save();

		return array("message" => 1);

	}


	public function sendEmailVerification(Request $request) {

//		try{
		    $patid = $request->input('patid');

		    if(isset($patid)){
			    $email = $request->input('email');
			    $patid = $request->input('patid');

			    $patient = patient::find($patid);

			    $code = Str::random('10');
			    $confirmation = new confirmation();
			    $confirmation->email = $email;
			    $confirmation->code = $code;
			    $saved = $confirmation->save();

			    if($saved) Mail::to($email)->send(new confirmEmail($code,$patient));

			    return 1;

		    }

			$docid = $request->input('docid');

			if(isset($docid)){

				$email = $request->input('email');
				$docid = $request->input('docid');

				$doctor = doctor::find($docid);

				$code = Str::random('10');
				$confirmation = new confirmation();
				$confirmation->email = $email;
				$confirmation->code = $code;
				$saved = $confirmation->save();

				if($saved) Mail::to($email)->send(new confirmEmail($code,$doctor));

				return 1;

			}



//		}catch (\Exception $exception){
//			return 0;
//		}

	}


	public function liveUpdate() {

		$availableCases = cases::where('status','Available')->count();
		$pendingCases = cases::where('status','Pending')->count();
		$completedCases = cases::where('status','Completed')->count();

		$patient = patient::all()->count();
		$patientsToday = patient::where('created_at','>',Carbon::today())->count();

		$hospitals = hospital::all()->count();
		$hospitalsToday = hospital::where('created_at','>',Carbon::today())->count();

		$labs = lab::all()->count();
		$labsToday = lab::where('created_at','>',Carbon::today())->count();

		$pharmacies = pharmacy::all()->count();
		$pharmaciesToday = pharmacy::where('created_at','>',Carbon::today())->count();

		$doctors = doctor::all()->count();
		$doctorsToday = doctor::where('created_at','>=',Carbon::today())->count();

		$payments = payment::all()->count();
		$paymentsToday = payment::where('created_at','>',Carbon::today())->count();


		return response([
			'availableCases' => $availableCases,
			'pendingCases'   => $pendingCases,
			'completedCases'   => $completedCases,
			'patient'        => $patient,
			'patientsToday'  => $patientsToday,
			'hospitals'      => $hospitals,
			'hospitalsToday' => $hospitalsToday,
			'labs'           => $labs,
			'labsToday'      => $labsToday,
			'pharmacies'     => $pharmacies,
			'pharmaciesToday'=> $pharmaciesToday,
			'doctors'        => $doctors,
			'doctorsToday'   => $doctorsToday,
			'payments'       => $payments,
			'paymentsToday'  => $paymentsToday

		]);

	}

	public function dependants( Request $request ) {
		$patid = $request->input('patid');
		$dependants = patientDependant::where('patid', $patid)->get();

		return $dependants;
	}

	public function patientDetails( Request $request ) {
		$patid = $request->input('patid');
		$patient = patient::find($patid);
		$patient->Dependants;

		return $patient;

	}

	public function socialSignUp(Request $request) {

		try{
			DB::transaction( function () {
				global $request;
				$image = $request->input('image');
				$signup = new patient();
				$signup->fname = $request->input('fname');
				$signup->sname = $request->input('sname');
				$signup->password = $request->input('password');
				$signup->email = $request->input('email');


				if(isset($image))
					$signup->image = $image;
				else
					$signup->image = url('/img/default-profile.png');
				$signup->gender = $request->input('gender');
				$signup->phone = $request->input('phone');

				$status = $signup->save();

				if ($status){
					echo 1;

					$code = Str::random('10');
					$confirmation = new confirmation();
					$confirmation->email = $signup->email;
					$confirmation->code = $code;
					$saved = $confirmation->save();

					if($saved) Mail::to($signup->email)->send(new confirmEmail($code,$signup));

				} else {
					echo 0;
				}

			},2);

		}catch (\Exception $exception){
			//Bugsnag::notifyException($exception);
			return 0;
		}



	}


	public function socialSignIn( Request $request ) {
		try {
			$email = $request->input( 'email' );
			$type  = $request->input( 'type' );

			$patient = patient::where( 'email', $email )->first();

			if ( count( $patient ) <= 0 ) {
				return 0;
			}

			if ( $patient->password == $type ) {
				return $patient;
			} else {
				return 0;
			}
		} catch (\Exception $exception){
			//Bugsnag::notifyException($exception);
			return response($exception,500);
		}
	}

	public function signIn( Request $request ) {

		try {
			$email = $request->input('email');
			$password = $request->input('password');

			$patient = patient::where('email',$email)->first();
			$patient->Dependants;

			if(count($patient) > 0) {
				if ( password_verify( $password, $patient->password ) ) {

					// suspension check
					if($patient->isSuspended == 1)
						return response(array("message" => "You have been suspended"),500);


					return array('message' => 1,
					             'patient'  => $patient);
				} else {
					return array('message' => "Wrong credentials");
				}

			} else {

				return array('message' => "Email has not been registered as a patient with us.");
			}


		} catch (\Exception $exception){
			return response($exception,500);
		}


	}

	public function doctorSignIn( Request $request ) {

		try {
			$email = $request->input('email');
			$password = $request->input('password');

			$doctor = doctor::where('email',$email)->first();

			if(count($doctor) > 0) {
				if ( password_verify( $password, $doctor->password ) ) {

					// suspension check
					if($doctor->isSuspended == 1)
					return response(array("message" => "You have been suspended"),500);

					return array('message' => 1,
								 'doctor'  => $doctor);

				} else {
					return array('message' => "Wrong credentials");
				}

			} else {

				return array('message' => "Email has not been registered as a doctor with us.");
			}

		} catch (\Exception $exception){
			return response($exception,500);
		}


	}


	public function confirmVerification( Request $request ) {
		try{
			$email = $request->input('email');
			$confirmCode = $request->input('code');
			$newPassword = $request->input('newPassword');

			$resetData = passwordReset::where('email',$email)->get()->last();

			if($confirmCode == $resetData->token) {

				$patient = patient::where('email',$email)->first();
				$patient->password = bcrypt( $newPassword );
				$status             = $patient->save();

				passwordReset::destroy($resetData->prid);

				if ( $status ) {
					return 1;
				} else {
					return 0;
				}

			} else return 0;

		}catch (\Exception $exception){
			//Bugsnag::notifyException($exception);
			return response("An error occurred.",500);
		}
	}


	public function changeUserPassword( Request $request ) {

		try{
			$email = $request->input('email');
			$password = $request->input('password');
			$newPassword = $request->input('newPassword');

			$patient = patient::where('email',$email)->first();

			if(count($patient) > 0) {
				if ( password_verify( $password, $patient->password ) ) {

					$patient->password = bcrypt( $newPassword );
					$status             = $patient->save();

					if ( $status ) {
						return 1;
					} else {
						return 0;
					}

				} else {
					return 0;
				}

			}

		}catch (\Exception $exception){
			//Bugsnag::notifyException($exception);
			return response("An error occurred.",500);
		}
	}


	public function updateUserProfile( Request $request ) {
		try{
			$patid = $request->input('patid');
			$patient = patient::find($patid);
			$status = $patient->update($request->all());

			if($status) return 1; else return 0;
		}catch (\Exception $exception){
			////Bugsnag::notifyException($exception);

		}

	}

	public function specializations() {
		$specializations = specialization::all();
		return $specializations;
	}

	function sendSms($phone,$Message){

		/* Variables with the values to be sent. */
		$owneremail="tobennaa@gmail.com";
		$subacct="dropster";
		$subacctpwd="dropster";
		$sendto= $phone; /* destination number */
		$sender="DROPSTER"; /* sender id */

		$message= $Message;  /* message to be sent */

		/* create the required URL */
		$url = "http://www.smslive247.com/http/index.aspx?"  . "cmd=sendquickmsg"  . "&owneremail=" . UrlEncode($owneremail)
		       . "&subacct=" . UrlEncode($subacct)
		       . "&subacctpwd=" . UrlEncode($subacctpwd)
		       . "&message=" . UrlEncode($message)
		       . "&sender=" . UrlEncode($sender)
		       ."&sendto=" . UrlEncode($sendto)
		       ."&msgtype=0";


		/* call the URL */
		if ($f = @fopen($url, "r"))  {

			$answer = fgets($f, 255);

			if (substr($answer, 0, 1) == "+") {

				return 1;
			}
			else  {
				return 0;
			}
		}

		else  {  return 0;  }
	}



	public function sendSMSVerification( Request $request ) {

		try{
			$phone = $request->input('phone');

			$code = self::random_str(6);
			$confirmation = new confirmation();
			$confirmation->email = $phone;
			$confirmation->code = $code;
			$confirmation->role = $request->input('role');
			$confirmation->save();

			$this->sendSms($phone,"Your phone verification code is " .$code );

			return 1;
		}catch(\Exception $exception){
			//Bugsnag::notifyException($exception);
			return 0;
		}

	}

	public function confirmSMSVerification( Request $request ) {
		try{
			$phone = $request->input('phone');
			$code = $request->input('code');

			$confirmation = confirmation::where('email',$phone)->get()->last();

			if($confirmation->code == $code) {
				$patient = patient::where('phone',$phone)->first();
				$patient->isConfirmed = 1;
				$patient->save();

				return 1;
			} else return 0;

		}catch(\Exception $exception){
			//Bugsnag::notifyException($exception);
			return response(0,500);
		}

	}

	public function confirmEmailVerification( Request $request ) {
		try{
			$phone = $request->input('email');
			$code = $request->input('code');

			$confirmation = confirmation::where('email',$phone)->get()->last();

			if($confirmation->code == $code) {
				$patient = patient::where('email',$phone)->first();
				$patient->isConfirmed = 1;
				$patient->save();

				return 1;
			} else return 0;

		}catch(\Exception $exception){
			//Bugsnag::notifyException($exception);
			return response(0,500);
		}

	}

	public function loungeArticles( Request $request ) {
		$loungeArticles = loungeItem::all()->toArray();

		return $loungeArticles;
	}

	public function doctorRequestTest( Request $request ) {
		$docid = $request->input('docid');
		$casid = $request->input('casid');
		$testid = $request->input('testid');
		$notes  = $request->input('notes');

		$case = cases::find($casid);
		if($case->docid != $docid){
			return response(array("message" => "Un-authorized. You are not the doctor handling this case"),500);
		}

		$caseTest = new caseTest();
		$caseTest->docid = $docid;
		$caseTest->casid = $casid;
		$caseTest->testid = $testid;
		$caseTest->notes = $notes;
		$caseTest->save();

		return array("message" => 1);

	}


	public function doctorRemoveTestRequest( Request $request ) {
		$docid = $request->input('docid');
		$ctid = $request->input('ctid');

		$caseTest = caseTest::find($ctid);
		if(isset($caseTest->labid)) return response(array("message" => "Test has already been processed by a lab. Can't be removed."),500);
		if($caseTest->docid != $docid){
			return response(array("message" => "Un-authorized. You are not the doctor handling this case"),500);
		}

		caseTest::destroy($ctid);

		return array("message" => 1);

	}

	public function doctorAddPrescription( Request $request ) {
		$docid = $request->input('docid');
		$casid = $request->input('casid');
		$case = cases::find($casid);

		if($case->docid != $docid)return response(array("message" => "Un-authorized. You are not the doctor handling this case"),500);

		$prescription = new prescription();
		$prescription->docid = $docid;
		$prescription->casid = $casid;
		$prescription->prescription = $request->input('prescription');
		$prescription->code = self::random_str_alphanumeric(6);
		$prescription->save();

		return array("message" => 1);

	}

	public function addMedicalHistory( Request $request ) {
		$patid = $request->input('patid');
		$pdid = $request->input('pdid');

		$medicalHistory = new medicalHistory();
		$medicalHistory->patid = $patid;
		$medicalHistory->history = $request->input('history');

		if(isset($pdid)){
			$medicalHistory->pdid = $pdid;
			$medicalHistory->type = 'dependant';
		}

		$medicalHistory->save();

		return array("message" => 1);

	}

	public function selectLab( Request $request ) {
		$ctid = $request->input('ctid');
		$labid = $request->input('labid');

		$caseTest = caseTest::find($ctid);

//		if(!empty($caseTest->labid)) return response(array("message" => "You have already selected a lab"),500);

		$caseTest->labid = $labid;
		$caseTest->save();

		return array("message" => 1);
	}

	public function doctorAddDiagnosis( Request $request ) {
		$docid = $request->input('docid');
		$casid = $request->input('casid');
		$case = cases::find($casid);

		if($case->docid != $docid)return response(array("message" => "Un-authorized. You are not the doctor handling this case"),500);

		$case->diagnosis = $request->input('diagnosis');
		$case->save();

		return array("message" => 1);
	}

	public function addFeedback( Request $request ) {
		try{
			$casid = $request->input('casid');
			$feeling = $request->input('feeling');

			$case = cases::find($casid);
			if($case->status !== "Completed") return response(array("message" => "You cannot rate a doctor for a case that isn't completed"),500);

			//dont allow a case be rated twice
			$check = feedback::where('casid',$casid)->count();
			if($check > 0) return response(array("message" => "The doctor has already been rated for this case"),500);

			$feedback = new feedback();
			$feedback->patid = $request->input('patid');
			$feedback->feeling = $request->input('feeling');
			if($feeling == "3" || $feeling == "4" || $feeling == "5"){
				$feedback->isViewed = 1;
				$feedback->uid = User::where('role','Admin')->first()->uid;
			}

			$feedback->casid = $casid;
			$feedback->content  = $request->input('content');
			$feedback->save();



			$doctorCases = cases::where('docid',$case->docid)->get();
			$doctorCasesArray = array();

			foreach($doctorCases as $item){
				array_push($doctorCasesArray,$item->casid);
			}

			$totalRating = 0;

			$doctorFeedback = feedback::whereIn("casid",$doctorCasesArray)->get();

			$caseCount = count($doctorFeedback);

			foreach($doctorFeedback as $item){

				$totalRating += $item->feeling;
			}


			$rating = $totalRating / $caseCount;

			$doctor = doctor::find($case->docid);
			$doctor->rating = $rating;
			$doctor->save();



			return array("message" => 1);
		} catch (\Exception $exception){
			return response("Sorry an error occurred.", 500);
		}
	}

	public function completeCase( Request $request ) {
		$casid = $request->input('casid');
		$docid = $request->input('docid');
		$patid = $request->input('patid');
		$case = cases::find($casid);

		if(empty($docid) && empty($patid) ) return response(array("message" => "Bad request."),500);
		if(!empty($docid) && $case->docid != $docid) return response(array("message" => "Un-authorized. You are not the doctor handling this case."),500);
		if(!empty($patid) && $case->patid != $patid) return response(array("message" => "You are not authorized to modify this case."),500);

		if(isset($case->diagnosis)){

			if($case->status != "Completed"){
				$case->ended_at = Carbon::now()->toDateTimeString();
				$case->status = "Completed";
				$case->save();
			} else return response(array("message" => "Case has already been completed"),500);
		}

		return array("message" => 1);

	}

	public function labs() {
		$labs = lab::all();
		return $labs;
	}

	public function hospitals() {

		$hospitals = hospital::all();

		foreach ($hospitals as $hospital){
			foreach($hospital->Specialization as $specialization){
				$specialization->Specialization;
			}
		}

		return $hospitals;
	}

	public function pharmacies() {
		$pharmacies = pharmacy::all();

		return $pharmacies;
	}

	public function cancelCase( Request $request ) {
		$casid = $request->input('casid');
		$docid = $request->input('docid');
		$patid = $request->input('patid');
		$case = cases::find($casid);

		if(empty($docid) && empty($patid) ) return response(array("message" => "Bad request."),500);
		if(!empty($docid) && $case->docid != $docid) return response(array("message" => "Un-authorized. You are not the doctor handling this case."),500);
		if(!empty($patid) && $case->patid != $patid) return response(array("message" => "You are not authorized to modify this case."),500);

		if($case->status != "Cancelled"){
			$case->status = "Cancelled";
			$case->save();
		} else return response(array("message" => "Case has already been cancelled."),500);


		return array("message" => 1);

	}


	static  function random_str($length, $keyspace = '0123456789')
	{
		$str = '';
		$max = mb_strlen($keyspace, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			$str .= $keyspace[random_int(0, $max)];
		}
		return $str;
	}

	static  function random_str_alphanumeric($length, $keyspace = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')
	{
		$str = '';
		$max = mb_strlen($keyspace, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			$str .= $keyspace[random_int(0, $max)];
		}
		return $str;
	}


}
