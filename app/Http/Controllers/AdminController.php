<?php

namespace App\Http\Controllers;

use App\cases;
use App\doctor;
use App\doctorPayment;
use App\doctorsSpec;
use App\faq;
use App\feedback;
use App\hospital;
use App\lab;
use App\loungeItem;
use App\patient;
use App\payment;
use App\pharmacy;
use App\privacy;
use App\setting;
use App\specialization;
use App\terms;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use PHPExcel;
use PHPExcel_IOFactory;
use PhpParser\Node\Stmt\If_;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index() {

		$availableCases = cases::where('status','Available')->get();
		$pendingCases = cases::where('status','Pending')->get();
		$completedCases = cases::where('status','Complete')->get();

		$hospitals = hospital::all();
		$hospitalsToday = hospital::where('created_at','>',Carbon::today())->get();

		$labs = lab::all();
		$labsToday = lab::where('created_at','>',Carbon::today())->get();

		$pharmacies = pharmacy::all();
		$pharmaciesToday = pharmacy::where('created_at','>',Carbon::today())->get();

		$doctors = doctor::all();
		$doctorsToday = doctor::where('created_at','>=',Carbon::today())->get();

		$payments = payment::all();
		$paymentsToday = payment::where('created_at','>',Carbon::today())->get();

		$patients = patient::all();
		$patientsToday = patient::where('created_at','>',Carbon::today())->get();




		return view('admin.dash',[
			'availableCases'  => $availableCases,
			'pendingCases'    => $pendingCases,
			'completedCases'  => $completedCases,

			'hospitals'       => $hospitals,
			'hospitalsToday'  => $hospitalsToday,

			'labs'            => $labs,
			'labsToday'       => $labsToday,

			'pharmacies'      => $pharmacies,
			'pharmaciesToday' => $pharmaciesToday,

			'doctors'         => $doctors,
			'doctorsToday'    => $doctorsToday,

			'patients'        => $patients,
			'patientsToday'   => $patientsToday,

			'payments'        => $payments,
			'paymentsToday'   => $paymentsToday,

		]);

	}

	public function suspendPatient( Request $request, $patid ) {
		$patient = patient::find($patid);
		$patient->isSuspended = 1;
		$patient->save();
		$request->session()->flash('success','Patient Suspended.');

		return redirect()->back();

	}

	public function unsuspendPatient( Request $request, $patid ) {
		$patient = patient::find($patid);
		$patient->isSuspended = 0;
		$patient->save();
		$request->session()->flash('success','Patient Readmitted.');

		return redirect()->back();
	}

	public function managePayments() {
    	$payments = payment::all()->sortByDesc('created_at');

    	//get settings
		$accessFeePercent = setting::where('name','accessFee')->get()->last()->value;
		$paymentSchedule = setting::where('name','paymentSchedule')->get()->last()->value;

		$modifiedPayments = collect();

		foreach ( $payments as $payment ) {
			try {$payment->Patient;} catch ( \Exception $exception ) {}
			try {$payment->Case;} catch ( \Exception $exception ) {}
			try {$payment->Case->Doctor;} catch ( \Exception $exception ) {}

			$maturityDate = Carbon::createFromFormat("Y-m-d H:i:s",$payment->Case->ended_at)
			                      ->addDays($paymentSchedule)->toDateTimeString();
			$consultationFee = $payment->amount;
			$accessFee       = $consultationFee * ( $accessFeePercent / 100 );
			$doctorsFee      = $consultationFee - $accessFee;

//    		 add new fields to our collection
			$payment = collect( $payment )->put( 'doctorsFee', $doctorsFee )
			                              ->put('maturityDate',$maturityDate)
			                              ->put( 'accessFee', $accessFee );

			$modifiedPayments->push( $payment );
		}

		return view('payments.manage',[
			'payments' => $modifiedPayments,
		]);
	}



	public function payDoctors(Request $request) {

		$paymentSchedule = setting::where('name','paymentSchedule')->get()->last()->value;
		$accessFeePercent = setting::where('name','accessFee')->get()->last()->value;
		$mnpt           = setting::where( 'name', 'minimumNairaPayoutThreshold' )->get()->last()->value;


		$duePayments = array();



		$doctors = doctor::all();

		foreach($doctors as $doctor){

			$previousTotalEarning = $doctor->totalEarnings;

			//reset doctors earnings

			$doctor->earnings = 0;
			$doctor->totalEarnings = 0;
			$doctor->save();

			$toPay = false;
			foreach($doctor->Cases as $case) {


				$maturityDate = Carbon::createFromFormat( "Y-m-d H:i:s", $case->ended_at )->addDays( $paymentSchedule );


				// check if case is completed, mature
				if ( $case->status == "Completed" &&
				     Carbon::now()->greaterThanOrEqualTo( $maturityDate ) &&
				     $case->paymentStatus == "paid" ) {

					$consultationFee = $case->Payment->amount;
					$accessFee       = $consultationFee * ( $accessFeePercent / 100 );
					$doctorsFee      = $consultationFee - $accessFee;

					//update doctors earnings

					if ( $case->Payment->status == "Unpaid" ) {
						$doctor                = doctor::find( $case->docid );
						$doctor->earnings      += $doctorsFee;
						$doctor->totalEarnings += $doctorsFee;
						$doctor->save();

						array_push( $duePayments, $case->Payment );
					}


					if ( $case->Doctor->earnings >= $mnpt ) {
						$toPay = true;
					}

				}


				if ( $toPay && $doctor->isPaymentRequested ) { // if we are pay, pay

					$paymentRecord = new doctorPayment();
					$paymentRecord->docid  = $doctor->docid;
					$paymentRecord->uid    = Auth::user()->uid;
					$paymentRecord->amount = $doctor->earnings;
					$paymentRecord->save();

					$doctor->earnings = 0;
					$doctor->save();
					foreach ( $doctor->Cases as $thiscase ) {

						if($thiscase->Payment->status == "Unpaid"){
							// change status of the doctor to paid
							// if payment is requested
							$thispayment = payment::find($thiscase->Payment->payid);
							$thispayment->status = "Paid";
							$thispayment->save();

						}

					}
				}

			}

			$doctor->totalEarnings = $doctor->totalEarnings + $previousTotalEarning;
			$doctor->save();


		}

		$request->session()->flash('success','Payments Made.');


    	return redirect()->back();
	}


	public function ExportPayments() {

		$payments = payment::all()->sortByDesc('created_at');

		//get settings
		$accessFeePercent = setting::where('name','accessFee')->get()->last()->value;
		$paymentSchedule = setting::where('name','paymentSchedule')->get()->last()->value;

		$modifiedPayments = collect();

		foreach ( $payments as $payment ) {
			try {$payment->Patient;} catch ( \Exception $exception ) {}
			try {$payment->Case;} catch ( \Exception $exception ) {}
			try {$payment->Case->Doctor;} catch ( \Exception $exception ) {}

			$maturityDate = Carbon::createFromFormat("Y-m-d H:i:s",$payment->Case->ended_at)
			                      ->addDays($paymentSchedule)->toDateTimeString();
			$consultationFee = $payment->amount;
			$accessFee       = $consultationFee * ( $accessFeePercent / 100 );
			$doctorsFee      = $consultationFee - $accessFee;

//    		 add new fields to our collection
			$payment = collect( $payment )->put( 'doctorsFee', $doctorsFee )
			                              ->put('maturityDate',$maturityDate)
			                              ->put( 'accessFee', $accessFee );

			$modifiedPayments->push( $payment );
		}



		$data =  $modifiedPayments;
		$serialNumber = 1;

		define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setCreator(Auth::user()->fname ." ". Auth::user()->sname)
		            ->setTitle("Doctap Payment Export - " . Carbon::now()->toDayDateTimeString() )
		            ->setSubject("Doctap Payments");

		$cell = 2;

		foreach ($data as $payment){

//			return $payment;
			try {

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'A1', "S/N" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'A' . $cell, $serialNumber );


				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'B1', "DATE AND TIME" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'B' . $cell, Carbon::createFromFormat("Y-m-d H:i:s",$payment['case']['ended_at'])->toDayDateTimeString() );

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'C1', "CASE ID" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'C' . $cell, $payment['casid'] );

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'D1', "PATIENT" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'D' . $cell, $payment['patient']['fname'] . " ". $payment['patient']['sname']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'E1', "DOCTOR" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'E' . $cell, $payment['case']['doctor']['fname'] . " ". $payment['case']['doctor']['sname'] );

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'F1', "ACCOUNT NAME" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'F' . $cell, $payment['case']['doctor']['accountName']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'G1', "ACCOUNT NUMBER" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'G' . $cell, $payment['case']['doctor']['accountNumber']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'H1', "ACCOUNT BANK" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'H' . $cell, $payment['case']['doctor']['accountBank']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'I1', "CONSULTATION FEE" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'I' . $cell, $payment['amount']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'J1', "DOCTOR'S FEE" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'J' . $cell, $payment['doctorsFee']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'K1', "ACCESS FEE" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'K' . $cell, $payment['accessFee']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'L1', "TRANSACTION ID" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'L' . $cell, $payment['others']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'M1', "MATURITY DATE" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'M' . $cell, $payment['maturityDate']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'N1', "CASE STATUS" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'N' . $cell, $payment['case']['status']);


			} catch (\Exception $exception){}
			$cell++;
			$serialNumber++;
		}

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save("Doctap Payments Export_" .  Carbon::now()->toDateString() .".xlsx");

		return  response()->download("Doctap Payments Export_" .  Carbon::now()->toDateString() .".xlsx");

	}


	public function manageSpecializations() {

    	if (session()->has('pages')){
    		$pages = session()->get('pages');
	    }else{
    		$pages = session()->put('pages', 25);
	    }


    	if (Input::has('pages')){
    		$pages = Input::get('pages');
    		session()->put('pages',$pages);
		    $specializations = specialization::paginate($pages);
	    }else{
		    $specializations = specialization::paginate($pages);
	    }

		return view('specializations.manage',[
			'specializations' => $specializations
		]);

	}

	public function manageCases( Request $request) {

		if (session()->has('pages')){
			$pages = session()->get('pages');
		}else{
			$pages = session()->put('pages', 25);
		}

		if (Input::has('by') and Input::has('term')){
    		$by = Input::get('by');
    		$term = Input::get('term');

    		$cases = cases::where($by, 'like', "%$term%")->get();

		}elseif(Input::has('pages')){
			$pages = Input::get('pages');

			session()->put('pages',$pages);
			$cases = cases::orderBy('created_at', 'desc')->paginate($pages);

//			return $cases;

		} elseif(Input::has("filter")){
			$filter = Input::get('filter');
			$filter = ucfirst($filter);
			$cases = cases::where('status',$filter)->paginate($pages);
		} else{
			$cases = cases::orderBy('created_at', 'desc')->paginate($pages);

		}


		return view('cases.manage',[
			'cases' => $cases
		]);
	}

	public function feedback() {
    	if (session()->has('pages')){
    		$pages = session()->get('pages');
	    }else{
    		session()->put('pages',25);
	    }

    	if (Input::has('pages')){
    		$pages = Input::get('pages');
    		session()->put('pages',$pages);
		    $feedback = feedback::orderBy('created_at', 'desc')->paginate($pages);
	    }else{
		    $feedback = feedback::orderBy('created_at', 'desc')->paginate($pages);
	    }



		return view('feedback.manage',[
			'feedbacks' => $feedback
		]);
	}

	public function feedbackDetails( $fdid ) {
		$feedback = feedback::find($fdid);
		return view('feedback.details',[
			'feedback' => $feedback
		]);
	}

	public function feedbackViewed( Request $request, $fdid ) {
		$feedback = feedback::find($fdid);
		$feedback->uid = Auth::user()->uid;
		$feedback->isViewed = 1;
		$feedback->save();

		$request->session()->flash('success','Feedback Viewed.');

		return redirect()->back();
	}

	public function manageLounge() {

    	if (session()->has('pages')){
    		$pages = session()->get('pages');
	    }else{
    		$pages = session()->put('pages',25);
	    }

		if (Input::has('pages')){
			$pages = Input::get('pages');
			session()->put('pages',$pages);
			$loungeItems = loungeItem::paginate($pages);
		}else{
//			$pages = session()->put('pages', 25);
			$loungeItems = loungeItem::paginate($pages);
		}


    	return view('lounge.manage',[
    		'loungeItems' => $loungeItems
	    ]);
	}

	public function addLoungeItem() {
		return view('lounge.add');
	}

	public function loungeDetails($lid) {
		$loungeItem = loungeItem::find($lid);

		return view('lounge.details',[
			'loungeItem' => $loungeItem
		]);
	}

	public function terms() {

    	if (session()->has('pages')){
    		$pages = session()->get('pages');
	    }else{
    		 session()->put('pages',25);
	    }


//    	$allterms = terms::all()->sortByDesc('created_at')->paginate(25);

		if(Input::has('pages')){
			$pages = Input::get('pages');
			session()->put('pages',$pages);
			$allterms = terms::paginate($pages);
			$terms = terms::all()->last();
		}else{
			$allterms = terms::paginate($pages);
			$terms = terms::all()->last();
		}


		return view('terms.manage',[
			'terms' => $terms,
			'allterms' => $allterms
		]);

	}

	public function addFAQ() {
		return view('admin.addFAQ');
	}

	public function faqs() {

    	if (session()->has('pages')){
    		$pages = session()->get('pages');
	    }else{
    		session()->put('pages',25);
	    }



    	if (Input::has('pages')){
    		$pages =Input::get('pages');
    		session()->put('pages',$pages);
		    $faqs = faq::orderBy('created_at', 'desc')->paginate($pages);
	    }else{
		    $faqs = faq::orderBy('created_at', 'desc')->paginate($pages);
//		    $faqs = faq::paginate($pages);
	    }


		return view('faqs.manage',[
			'faqs' => $faqs
		]);
	}

	public function privacyPolicy() {
    	if (session()->has('pages')){
    		$pages = session()->get('pages');
	    }else{
    		session()->put('pages', 25);
	    }



    	if (Input::has('pages')){
    		$pages = Input::get('pages');
    		session()->put('pages', $pages);

		    $allprivacy = privacy::orderBy('created_at', 'desc')->paginate($pages);
		    $privacy = privacy::all()->last();
	    }else{
		    $allprivacy = privacy::orderBy('created_at', 'desc')->paginate($pages);
		    $privacy = privacy::all()->last();
	    }

		return view('privacy.manage',[
			'privacy' => $privacy,
			'allprivacy' => $allprivacy
		]);
	}

	public function postTerms( Request $request ) {
		try {

			$terms = new terms();
			$terms->content = $request->input('content');
			$terms->save();

			$request->session()->flash('success','Terms Updated');
			return redirect()->back();
		} catch (\Exception $exception){
//			Bugsnag::notifyException($exception);
			$request->session()->flash('error','Sorry something went wrong. Please try again.');
		}

		return redirect()->back();
	}

	public function postFaq( Request $request ) {
		try{
			$faq = new faq();
			$faq->question = $request->input('question');
			$faq->answer = $request->input('answer');
			$faq->save();

			$request->session()->flash('success','Faq Added');


		}catch (\Exception $exception){
//			Bugsnag::notifyException($exception);
			$request->session()->flash('error','Sorry something went wrong. Please try again.');
		}

		return redirect()->back();

	}

	public function editFaq( $fid ) {
		$faq = faq::find($fid);
		return view('faqs.edit',[
			'faq' => $faq
		]);
	}

	public function postEditFaq( Request $request, $fid ) {
		try{
			$faq = faq::find($fid);
			$faq->question = $request->input('question');
			$faq->answer = $request->input('answer');
			$faq->save();

			$request->session()->flash('success','Faq Updated');

		}catch (\Exception $exception){
//			Bugsnag::notifyException($exception);
			$request->session()->flash('error','Sorry something went wrong. Please try again.');
		}

		return redirect()->back();

	}


	public function deleteFaq( Request $request, $fid ) {
		try{
			faq::destroy($fid);
			$request->session()->flash('success','FAQ Deleted');
		} catch (\Exception $exception){
//			Bugsnag::notifyException($exception);
			$request->session()->flash('error','Sorry something went wrong. Please try again.');
		}

		return redirect()->back();
	}

	public function postPrivacyPolicy( Request $request ) {
		try{
			$privacy = new privacy();
			$privacy->content = $request->input('content');
			$privacy->save();

			$request->session()->flash('success','Policy Updated');
		}catch (\Exception $exception){
//			Bugsnag::notifyException($exception);
			$request->session()->flash('error','Sorry something went wrong. Please try again.');
		}

		return redirect()->back();

	}

	public function postAddLoungeItem( Request $request ) {

    	if($request->hasFile('image')){
    		$filename = $request->file('image')->getClientOriginalName();
    		$request->file('image')->move('uploads',Carbon::now()->timestamp . $filename);
    		$imageUrl = url('uploads/' . Carbon::now()->timestamp. $filename);
	    } else {
    		$imageUrl = url('default-image.png');
	    }

    	$loungeItem = new loungeItem();
		$loungeItem->image = $imageUrl;
		$loungeItem->title = $request->input('title');
		$loungeItem->content = $request->input('content');
		$loungeItem->link = $request->input('link');
		$loungeItem->uid = Auth::user()->uid;
		$loungeItem->save();

		$request->session()->flash('success','Lounge Article Added.');

		return redirect()->back();
	}

	public function removeDoctorSpecialization( Request $request, $dsid ) {
		doctorsSpec::destroy($dsid);
		$request->session()->flash('success','Specialization Removed');
		return redirect()->back();
	}

	public function postAddSpecialization( Request $request ) {
		$specialization = new specialization();
		$specialization->name = $request->input('name');
		$specialization->description = $request->input('description');
		$specialization->save();

		$request->session()->flash('success','Specialization Added.');

		return redirect('manage-specializations');
	}

	public function postAssignSpecialization( Request $request ) {
		$casid = $request->input('casid');

		$case = cases::find($casid);
		$case->spid = $request->input('spid');
		$case->save();

		$request->session()->flash('success','Specialization Assigned.');

		return redirect('case/' . $casid);

	}
}
