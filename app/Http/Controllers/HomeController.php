<?php

namespace App\Http\Controllers;

use App\cases;
use App\caseTest;
use App\customer;
use App\doctor;
use App\feedback;
use App\hospital;
use App\hospitalSpec;
use App\lab;
use App\labtest;
use App\patient;
use App\patientDependant;
use App\payment;
use App\pharmacy;
use App\setting;
use App\specialization;
use App\test;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\In;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

	    if (session()->has('pages')){
		    $pages = session()->get('pages');
	    }else{
		    $pages = session()->put('pages', 25);
	    }

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	if(Auth::user()->role == 'Admin')
    		return redirect('admin');

	    if(Auth::user()->role == 'Lab')
		    return redirect('dashboard/lab');


    }


	public function redirect(  ) {
		return redirect('/');
    }


	public function getData(  ) {
		$cases = cases::paginate(25);

//		return $cases;
    	return view('table',[
    		'cases' => $cases
	    ]);
    }




	public function patientDetails($patid) {
		$patient = patient::find($patid);

		return view('patients.details',[
			'patient' => $patient
		]);
	}



	public function addHospital() {
		return view("hospitals.add");
    }

	public function hospitalDetails( $hid ) {
		$hospital = hospital::find($hid);

		$supportedSpecializations = hospitalSpec::where('hid',$hid)->get();
		$specializationsArray = array();

		foreach($supportedSpecializations as $specialization){
			array_push($specializationsArray,$specialization->spid);
		}

		$specializations = specialization::whereNotIn( 'spid',$specializationsArray)->get();

		return view('hospitals.details',[
			'hospital' => $hospital,
			'specializations' => $specializations,
			'hospitalSpecs' => $specializationsArray,
		]);
    }

	public function publishHospital( Request $request, $hid ) {
    	$hospital = hospital::find($hid);
    	$hospital->isPublished = 1;
    	$hospital->save();

    	$request->session()->flash('success','Hospital Published.');
    	return redirect()->back();
    }

	public function unpublishHospital( Request $request, $hid ) {
    	$hospital = hospital::find($hid);
    	$hospital->isPublished = 0;
    	$hospital->save();

    	$request->session()->flash('success','Hospital Un-Published.');
    	return redirect()->back();
    }

	public function postAddHospital( Request $request) {

		$email = $request->input('useremail');
		$user = User::where('email',$email)->count();
		if($user > 0) {
			$request->session()->flash('error',"User's email already exists");
			return redirect()->back()->withInput($request->all());
		}


		$phone = $request->input('userphone');
		$user = User::where('phone',$phone)->count();
		if($user > 0) {
			$request->session()->flash('error',"User's phone already exists");
			return redirect()->back()->withInput($request->all());
		}


		$phone = $request->input('phone');
		$user = hospital::where('phone',$phone)->count();
		if($user > 0) {
			$request->session()->flash('error',"Hospital's phone already exists");
			return redirect()->back()->withInput($request->all());
		}


		$email = $request->input('email');
		$user = hospital::where('email',$email)->count();
		if($user > 0) {
			$request->session()->flash('error',"Hospital's email already exists");
			return redirect()->back()->withInput($request->all());
		}


		if($request->hasFile('image')){
			$filename = $request->file('image')->getClientOriginalName();
			$request->file('image')->move('profileImages', $filename);

			$profileUrl = url('/profileImages/' . $filename);

		} else {
			$profileUrl = url('/profileImages/default-image.png');

		}

		$data = $request->all();


		$user = User::create([
			'fname' => $data['fname'],
			'sname' => $data['sname'],
			'role' => $data['role'],
			'email' => $data['useremail'],
			'phone' => $data['userphone'],
			'address' => $data['useraddress'],
			'image' => $profileUrl,
			'password' => bcrypt($data['password']),
		]);


		$hospital = new hospital();
		$hospital->name = $request->input('name');
		$hospital->city = $request->input('city');
		$hospital->phone = $request->input('phone');
		$hospital->email = $request->input('email');
		$hospital->address = $request->input('address');
		$hospital->uid = $user->uid;
		$hospital->createdBy = Auth::user()->uid;
		$hospital->save();

		$request->session()->flash('success','Hospital Added.');

		return redirect('manage-hospitals');
	}

	public function manageHospitals(  ) {

		if (Input::has('pages')){
			$pages = Input::get('pages');

			$hospitals = hospital::paginate($pages);
		}else{
			$hospitals = hospital::paginate(25);
		}

		return view('hospitals.manage',[
			'hospitals' =>$hospitals
		]);
	}


	public function getLabs(  ) {

		if (Input::has('by') and Input::has('term')){
			$by = Input::get('by');
			$term = Input::get('term');

			$labs = lab::where($by,'like',"%$term%")->get();
		}elseif (Input::has('pages')){
			$pages = Input::get('pages');

			$labs = lab::paginate($pages);
		}else{
			$labs = lab::paginate(25);
		}




		return view('admin.labs',[
    		'labs' => $labs
	    ]);
    }


	public function getPharmacies(  ) {



		if (Input::has('by') and Input::has('term')){
			$by = Input::get('by');
			$term = Input::get('term');

			$pharmacies = pharmacy::where($by,'like',"%$term%")->get();
		}else if (Input::has('pages')){
			$pages = Input::get('pages');

			$pharmacies = pharmacy::paginate($pages);
		}else{
			$pharmacies = pharmacy::paginate(25);
		}




    	return view('admin.pharmacy',[
    		'pharmacies' => $pharmacies
	    ]);
    }

	public function manageDoctors(  ) {


			if (Input::has('by') and Input::has('term')){
				$by = Input::get('by');
				$term = Input::get('term');

				$doctors = doctor::where($by,'like',"%$term%")->get();
			} elseif (Input::has('pages')){

			$pages = Input::get('pages');
			session()->put('pages',$pages);
			$doctors = doctor::paginate($pages);

		}else{
			$doctors = doctor::orderBy('created_at', 'desc')->paginate();

		}

    	return view('doctors.manage',[
    		'doctors' => $doctors
	    ]);
    }

	public function doctor( $docid ) {
		$doctor = doctor::find($docid);
		$feedbackCount = feedback::whereIn('casid',$doctor->Cases)->count();

		$totalPendingMaturity = 0;

		foreach($doctor->Cases as $case){
			$paymentSchedule = setting::where('name','paymentSchedule')->get()->last()->value;
			$accessFeePercent = setting::where('name','accessFee')->get()->last()->value;
			$maturityDate = Carbon::createFromFormat("Y-m-d H:i:s",$case->ended_at)->addDays($paymentSchedule);

			if(isset($case->Payment)){
				$consultationFee = $case->Payment->amount;
				$accessFee       = $consultationFee * ( $accessFeePercent / 100 );
				$doctorsFee      = $consultationFee - $accessFee;

				// check if case is completed, mature
				if($case->status == "Completed" &&
				   Carbon::now()->lessThan($maturityDate) &&
				   $case->paymentStatus == "paid" &&
				   $case->Payment->status == "Unpaid"
				) {
					$totalPendingMaturity += $doctorsFee;
				}

			}

		}


		return view('doctors.details',[
			'doctor' => $doctor,
			'feedbackCount' => $feedbackCount,
			'totalPendingMaturity' => $totalPendingMaturity
		]);
    }

	public function confirmDoctor( Request $request, $docid ) {
		$doctor = doctor::find($docid);
		$doctor->isConfirmed = 1;
		$doctor->save();
		$request->session()->flash('success','Doctor Confirmed');
		return redirect('doctor/' . $docid);
    }

	public function suspendDoctor(Request $request, $docid) {
    	$doctor = doctor::find($docid);
    	$doctor->isSuspended = 1;
    	$doctor->save();
    	$request->session()->flash('success','Doctor Suspended');
    	return redirect()->back();
    }

	public function unsuspendDoctor( Request $request, $docid ) {
		$doctor = doctor::find($docid);
		$doctor->isSuspended = 0;
		$doctor->save();
		$request->session()->flash('success','Doctor Re-admitted');
		return redirect()->back();
    }

	public function getCases( ) {




	    if (Input::has('by') and Input::has('term')){
    		$by = Input::get('by');
    		$term = Input::get('term');

    		$cases = cases::where("%$by%",'like',"%$term%")->get();
	    }else if (Input::has('pages')){
			    $pages = Input::get('pages');
			    $cases = cases::paginate($pages);

			    return $cases;
		    }else{
			    $cases = cases::paginate(25);
		    }



		return view('admin.cases',[
    		'cases' => $cases
	    ]);

    }

	public function caseDetails($casid) {
		$case = cases::find($casid);
		$specializations = specialization::all();

		return view('cases.details',[
			'case' => $case,
			'specializations' => $specializations
		]);
    }

	public function getPatients(  ) {


	    if (Input::has('by') and Input::has('term')){
    		$by = Input::get('by');
    		$term = Input::get('term');

    		$patients = patient::where($by,'like', "%$term%")->get();
	    }elseif (Input::has('pages')){
		    $pages = Input::get('pages');
		    session()->put('pages',$pages);
		    $patients = patient::paginate($pages);
	    }else{

		    $patients = patient::paginate(25);
	    }


		return view('patients.manage',[
			'patients' => $patients
		]);
    }


	public function editPatient($patid) {

    	$patient = patient::find($patid);
    	return view('patients.edit',[
    		'patient' => $patient
	    ]);

    }


	public function updatePatient(Request $request, $patid  ) {

    	$patient = patient::find($patid);
    	$s = $patient->update($request->all());
    	if ($s){
    		session()->flash('success','Patient Updated.');
	    }else{
    		session()->flash('error','something went wrong, please try again');
	    }


    	return redirect()->back();

    }


	public function editDependant($pdid) {
		$dependant = patientDependant::find($pdid);
		return view('dependants.edit',[
			'dependant' => $dependant
		]);

    }

	public function updateDependant( Request $request, $pdid) {
		$dependant =  patientDependant::find($pdid);
		$s = $dependant->update($request->all());


		if ($s){
			session()->flash('success','Dependant Updated.');
		}else{
			session()->flash('error','something went wrong, please try again');
		}
		return redirect()->back();
    }


	public function addLab() {
		return view('labs.add');
    }

	public function postAddLab( Request $request) {

    	$email = $request->input('useremail');
    	$user = User::where('email',$email)->count();
    	if($user > 0) {
    		$request->session()->flash('error',"User's email already exists");
    		return redirect()->back()->withInput($request->all());
	    }


		$phone = $request->input('userphone');
		$user = User::where('phone',$phone)->count();
		if($user > 0) {
			$request->session()->flash('error',"User's phone already exists");
			return redirect()->back()->withInput($request->all());
		}


		$phone = $request->input('phone');
		$user = lab::where('phone',$phone)->count();
		if($user > 0) {
			$request->session()->flash('error',"Lab's phone already exists");
			return redirect()->back()->withInput($request->all());
		}


		$email = $request->input('email');
		$user = lab::where('email',$email)->count();
		if($user > 0) {
			$request->session()->flash('error',"Lab's email already exists");
			return redirect()->back()->withInput($request->all());
		}


		if($request->hasFile('image')){
			$filename = $request->file('image')->getClientOriginalName();
			$request->file('image')->move('profileImages', $filename);

			$profileUrl = url('/profileImages/' . $filename);

		} else {
			$profileUrl = url('/profileImages/default-image.png');

		}

		$data = $request->all();


		$user = User::create([
			'fname' => $data['fname'],
			'sname' => $data['sname'],
			'role' => $data['role'],
			'email' => $data['useremail'],
			'phone' => $data['userphone'],
			'address' => $data['useraddress'],
			'image' => $profileUrl,
			'password' => bcrypt($data['password']),
		]);


		$lab = new lab();
    	$lab->name = $request->input('name');
    	$lab->city = $request->input('city');
    	$lab->phone = $request->input('phone');
    	$lab->email = $request->input('email');
    	$lab->address = $request->input('address');
    	$lab->uid = $user->uid;
    	$lab->createdBy = Auth::user()->uid;
    	$lab->save();

    	$request->session()->flash('success','Lab Added.');

    	return redirect('manage-labs');
    }

	public function postAddLabTest(Request $request) {
    	$labid = $request->input('labid');
    	$testids = $request->input('testid');

    	foreach($testids as $testid){

		    $labTest = new labtest();
		    $labTest->labid = $labid;
		    $labTest->testid = $testid;
		    $labTest->save();
	    }

	    $numberOfTests = count($testids);
    	$request->session()->flash('success',"$numberOfTests Tests Added to Lab.");
    	return redirect('lab/' . $labid);

    }

	public function addPharmacy() {
		return view('pharmacies.add');
    }

	public function postAddHospitalSpecialization(Request $request) {
    	$hid = $request->input('hid');
    	$spids = $request->input('spid');

    	foreach($spids as $spid){

		    $hospitalSpec = new hospitalSpec();
		    $hospitalSpec->hid = $hid;
		    $hospitalSpec->spid = $spid;
		    $hospitalSpec->save();
	    }

	    $numberOfTests = count($spids);
    	$request->session()->flash('success',"$numberOfTests specializations added.");
    	return redirect()->back();

    }

	public function removeHospitalSpecialization( Request $request, $hid, $spid ) {
    	hospitalSpec::where('hid',$hid)->where('spid',$spid)->first()->delete();

		$request->session()->flash('success','Specialization Removed.');

		return redirect()->back();

	}


	public function removeTest( Request $request, $labid, $testid ) {
		labtest::where('labid',$labid)->where('testid',$testid)->first()->delete();

		$request->session()->flash('success','Test Removed.');

		return redirect('lab/' . $labid);

    }

	public function labDetails( $labid ) {
		$lab = lab::find($labid);
    	$supportedTests = labtest::where('labid',$labid)->get();
    	$testArray = array();

    	foreach($supportedTests as $test){
    		array_push($testArray,$test->testid);
	    }


    	$tests = test::whereNotIn( 'testid',$testArray)->get();


		return view('labs.details',[
			'lab' => $lab,
			'tests' => $tests,
			'supportedTests' => $supportedTests
		]);
    }

	public function pharmacyDetails( $pharmid ) {
		$pharmacy = pharmacy::find($pharmid);

		return view('pharmacies.details',[
			'pharmacy' => $pharmacy
		]);
    }

	public function postAddPharmacy( Request $request) {

		if($request->hasFile('image')){
			$filename = $request->file('image')->getClientOriginalName();
			$request->file('image')->move('profileImages', $filename);

			$profileUrl = url('/profileImages/' . $filename);

		} else {
			$profileUrl = url('/profileImages/default-image.png');

		}

		$data = $request->all();

		$user = User::create([
			'fname' => $data['fname'],
			'sname' => $data['sname'],
			'role' => $data['role'],
			'email' => $data['useremail'],
			'phone' => $data['userphone'],
			'address' => $data['useraddress'],
			'image' => $profileUrl,
			'password' => bcrypt($data['password']),
		]);

		$pharmacy = new pharmacy();
		$pharmacy->name = $data['name'];
		$pharmacy->address = $data['address'];
		$pharmacy->city = $data['city'];
		$pharmacy->uid = $user->uid;
		$pharmacy->createdBy = Auth::user()->uid;
		$pharmacy->save();


		$request->session()->flash('success','Pharmacy Added.');

		return redirect('manage-pharmacies');
	}


	public function manageLabs() {



		if (Input::has('by') and Input::has('term')){
			$by = Input::get('by');
			$term = Input::get('term');

			$labs = lab::where($by,'like',"%$term%")->get();
		}elseif (Input::has('pages')){
			$pages = Input::get('pages');
			session()->put('pages',$pages);

			$labs = lab::paginate($pages);
		}else{

			$labs = lab::paginate(25);
		}





		return view('labs.manage',[
			'labs' => $labs
		]);
    }

	public function managePharmacies() {



	    if (Input::has('by') and Input::has('term')){
    		$by = Input::get('by');
    		$term = Input::get('term');

    		$pharmacies = pharmacy::where($by,'like',"%$term%")->get();
	    }elseif (Input::has('pages')){
		    $pages = Input::get('pages');
		    session()->put('pages', $pages);

		    $pharmacies = pharmacy::paginate($pages);
	    }else{

		    $pharmacies = pharmacy::paginate(25);
	    }


		return view('pharmacies.manage',[
			'pharmacies' => $pharmacies
		]);
    }

	public function manageTests() {


	    if (Input::has('by') and Input::has('term')){

    		$by = Input::get('by');
    		$term = Input::get('term');
    		$tests = test::where($by,'like',"%$term%")->get();

	    }elseif (Input::has('pages')){
		    $pages = Input::get('pages');
		    session()->put('pages', $pages);

		    $tests = test::paginate($pages);
	    }else{
		    $tests = test::paginate(25);
	    }


    	return view('tests.manage',[
    		'tests' => $tests
	    ]);
    }

	public function editTest($testid) {
    	$test = test::find($testid);

		return view('tests.edit',[
			'test' => $test
		]);
    }


	public function dependantDetails(Request $request, $pdid ) {

    	$dependant = patientDependant::find($pdid);
    	return view('dependants.details',[
    		'dependant' => $dependant
	    ]);
    }


	public function manageSettings() {
		if ( count( setting::all() ) <= 0 ) {

			$setting        = new setting();
			$setting->name  = 'minimumNairaPayoutThreshold';
			$setting->value = 10000;
			$setting->save();

			$setting        = new setting();
			$setting->name  = 'minimumDollarPayoutThreshold';
			$setting->value = 30;

			$setting->save();
			$setting        = new setting();
			$setting->name  = 'subscriptionFee';
			$setting->value = 1000;
			$setting->save();

			$setting        = new setting();
			$setting->name  = 'consultationFee';
			$setting->value = 5000;
			$setting->save();

			$setting        = new setting();
			$setting->name  = 'accessFee';
			$setting->value = 25;
			$setting->save();

			$setting        = new setting();
			$setting->name  = 'paymentSchedule';
			$setting->value = 7;
			$setting->save();

		}

		$settings           = setting::all();
		$mnpt           = setting::where( 'name', 'minimumNairaPayoutThreshold' )->get()->last();
		$mdpt           = setting::where( 'name', 'minimumDollarPayoutThreshold' )->get()->last();
		$consultationFee           = setting::where( 'name', 'consultationFee' )->get()->last();
		$subscriptionFee          = setting::where( 'name', 'subscriptionFee' )->get()->last();
		$accessFee          = setting::where( 'name', 'accessFee' )->get()->last();
		$paymentSchedule          = setting::where( 'name', 'paymentSchedule' )->get()->last();

		return view('settings.manage',[
			'settings' => $settings,
			'mnpt' => $mnpt,
			'mdpt' => $mdpt,
			'consultationFee' => $consultationFee,
			'subscriptionFee' => $subscriptionFee,
			'accessFee'       => $accessFee,
			'paymentSchedule' => $paymentSchedule
		]);
    }

	public function editSetting(Request $request) {
		$setting = new setting();
		$setting->name = $request->input('name');
		$setting->value = $request->input('value');
		$setting->save();
		$request->session()->flash('success','Setting Modified.');
		return redirect()->back();
    }

	public function postEditTest( Request $request ) {

    	$testid = $request->input('testid');

		$test = test::find($testid);
		$test->title = $request->input('title');
		$test->description = $request->input('description');
		$test->save();

		$request->session()->flash('success','Test Edited.');

		return redirect('manage-tests');
    }


	public function postAddTest( Request $request ) {
		$test = new test();
		$test->title = $request->input('title');
		$test->description = $request->input('description');
		$test->save();

		$request->session()->flash('success','Test Added.');
		return redirect('manage-tests');
    }

	public function profile() {
		return view('profile');
    }

	public function logout() {
		Auth::logout();
		return redirect('/');
    }


}
