<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $primaryKey = 'uid';


    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $guarded = [ ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	public function Lab() {
		return $this->hasOne(lab::class,'uid','uid');
    }

	public function Hospital() {
		return $this->hasOne(hospital::class,'uid','uid');
    }

	public function Pharmacy() {
		return $this->hasOne(pharmacy::class,'uid','uid');
    }
}
