<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class doctorsSpec extends Model
{
    protected $primaryKey = 'dsid';
    protected $table = 'doctor_specializations';

	public function Specialization() {
		return $this->belongsTo(specialization::class,'spid' ,'spid');
    }
}
