<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class medicalHistory extends Model
{
    protected $primaryKey = 'mhid';
    protected $table = 'medicalHistory';

	public function Patient() {
		return $this->belongsTo(patient::class,'patid','patid');
    }

	public function Depandant() {
		return $this->belongsTo(patientDependant::class,'pdid','pdid');
    }
}
