<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class lab extends Model
{
    protected $primaryKey = 'labid';
    protected $table = 'labs';

	public function Cases() {
		return $this->hasManyThrough(cases::class,caseTest::class,'labid','casid');
    }

	public function LabTests() {
		return $this->hasMany(labtest::class,'labid','labid');
    }

	public function Results() {
		return $this->hasMany(testResult::class,'labid','labid');
    }

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
    }

	public function CreatedBy() {
		return $this->belongsTo(User::class,'createdBy','uid');
    }

}
