<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class specialization extends Model
{
    protected $primaryKey = 'spid';
    protected $table = 'specializations';
}
