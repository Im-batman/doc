<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class payment extends Model
{
	protected $primaryKey = 'payid';

	public function Patient() {
		return $this->belongsTo(patient::class,'patid','patid');
	}

	public function Case() {
		return $this->belongsTo(cases::class,'casid','casid');
	}

}
